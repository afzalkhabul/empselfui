// var uploadFileURL='http://139.162.27.78:8869/api/Uploads/dhanbadDb/download/';
var uploadFileURL='http://localhost:3010/api/Uploads/dhanbadDb/download/';
//
angular.module('ui.tinymce', ['ui.calendar'])
    .value('uiTinymceConfig', {})
    .directive('uiTinymce', ['uiTinymceConfig', function(uiTinymceConfig) {
        uiTinymceConfig = uiTinymceConfig || {};
        var generatedIds = 0;
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ngModel) {
                var expression, options, tinyInstance;
                // generate an ID if not present
                if (!attrs.id) {
                    attrs.$set('id', 'uiTinymce' + generatedIds++);
                }
                options = {
                    // Update model when calling setContent (such as from the source editor popup)
                    setup: function(ed) {
                        ed.on('init', function(args) {
                            ngModel.$render();
                        });
                        // Update model on button click
                        ed.on('ExecCommand', function(e) {
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                        // Update model on keypress
                        ed.on('KeyUp', function(e) {
                            console.log(ed.isDirty());
                            ed.save();
                            ngModel.$setViewValue(elm.val());
                            if (!scope.$$phase) {
                                scope.$apply();
                            }
                        });
                    },
                    mode: 'exact',
                    elements: attrs.id,
                };
                if (attrs.uiTinymce) {
                    expression = scope.$eval(attrs.uiTinymce);
                } else {
                    expression = {};
                }
                angular.extend(options, uiTinymceConfig, expression);
                setTimeout(function() {
                    tinymce.init(options);
                });
                ngModel.$render = function() {
                    if (!tinyInstance) {
                        tinyInstance = tinymce.get(attrs.id);
                    }
                    if (tinyInstance) {
                        tinyInstance.setContent(ngModel.$viewValue || '');
                    }
                };
            }
        };
    }]);

var app = angular.module('landAndAsset', ['ngRoute', 'ui.calendar', 'ngCalendar','ngSanitize', 'ui.select', 'ngFileUpload', 'datatables','angularjs-dropdown-multiselect', 'ui.tinymce', 'datatables.bootstrap']);

app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('inputMaxLengthNumber', function() {
  return {
    require: 'ngModel',
    restrict: 'A',
    link: function (scope, element, attrs, ngModelCtrl) {
      function fromUser(text) {
        var maxlength = Number(attrs.maxlength);
        if (String(text).length > maxlength) {
          ngModelCtrl.$setViewValue(ngModelCtrl.$modelValue);
          ngModelCtrl.$render();
          return ngModelCtrl.$modelValue;
        }
        return text;
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  };
})

app.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    }
});

app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [],
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});

app.config(function($routeProvider) {
    $routeProvider.when('/assetType', {
        templateUrl: './assetType.html',
        controller: 'assetTypeController'
    }).when('/assetStatus', {
        templateUrl: './assetStatus.html',
        controller: 'assetStatusController'
    }).when('/assetKPI', {
        templateUrl: './assetKPI.html',
        controller: 'assetKPIController'
    }).when('/assetGroup', {
        templateUrl: './assetGroup.html',
        controller: 'assetGroupController'
    }).when('/accountDetails', {
        templateUrl: './accountDetails.html',
        controller: 'accountDetailsController'
    }).when('/assetProfile', {
        templateUrl: './assetProfile.html',
        controller: 'assetProfileController'
    }).when('/assetCriticality', {
        templateUrl: './assetCriticality.html',
        controller: 'assetCriticalityController'
    }).when('/assetServiceLevel', {
        templateUrl: './assetServiceLevel.html',
        controller: 'assetServiceLevelController'
    }).when('/disposalMethods', {
        templateUrl: './disposalMethods.html',
        controller: 'disposalMethodController'
    }).when('/assetRisks', {
        templateUrl: './assetRisks.html',
        controller: 'assetRisksController'
    }).when('/costCenters', {
        templateUrl: './costCenter.html',
        controller: 'costCenterController'
    }).when('/assetLifeCyclePlan', {
        templateUrl: './assetLifeCyclePlan.html',
        controller: 'assetLifeCyclePlanController'
    }).when('/assetPolicies', {
        templateUrl: './assetPolicies.html',
        controller: 'assetPoliciesController'
    }).when('/maintenanceObjectives', {
        templateUrl: './maintenanceObjectives.html',
        controller: 'maintenanceObjectivesController'
    }).when('/typeOfDuty', {
        templateUrl: './typeOfDuty.html',
        controller: 'typeOfDutyController'
    }).when('/failureClasses', {
        templateUrl: './failureClasses.html',
        controller: 'failureClassesController'
    }).when('/assetSpares', {
        templateUrl: './assetSpares.html',
        controller: 'assetSparesController'
    }).when('/documentListMaster', {
        templateUrl: './documentListMaster.html',
        controller: 'documentListMasterController'
    }).when('/createAssetLand', {
        templateUrl: './createAssetLand.html',
        controller: 'createAssetLandController'
    }).when('/viewLandDetails/:packId', {
        templateUrl: './viewLand.html',
        controller: 'viewLandAssetController'
    }).when('/viewAssetDetails/:packId', {
        templateUrl: './viewOtherAssets.html',
        controller: 'viewAssetOthersController'
    }).when('/createAssetOthers', {
        templateUrl: './createAssetOthers.html',
        controller: 'createAssetOthersController'
    }).when('/costTransferFunds', {
        templateUrl: './costTransferFunds.html',
        controller: 'costTransferFundsController'
    }).when('/createAssetObjectives', {
        templateUrl: './createAssetObjectives.html',
        controller: 'createAssetObjectivesController'
    }).when('/createAssetSpares', {
        templateUrl: './createAssetSpares.html',
        controller: 'createAssetSparesController'
    }).when('/createAssetFailure', {
        templateUrl: './createAssetFailure.html',
        controller: 'createAssetFailureController'
    }).when('/createAssetRisks', {
        templateUrl: './createAssetRisk.html',
        controller: 'createAssetRisksController'
    }).when('/assetDemandProjection', {
        templateUrl: './demandProjection.html',
        controller: 'demandProjectionController'
    }).when('/createAssetCauses', {
        templateUrl: './assetCauses.html',
        controller: 'createAssetCausesController'
    }).when('/purchageAgenda', {
        templateUrl: './purchageAgenda.html',
        controller: 'purchageAgendaController'
    }).when('/assignDriver', {
        templateUrl: './assignDriver.html',
        controller: 'assignDriverController'
    }).when('/logBook', {
        templateUrl: './logBook.html',
        controller: 'logBookController'
    }).when('/submitBills', {
        templateUrl: './submitBills.html',
        controller: 'submitBillsController'
    }).when('/purchageAgendaRequest', {
        templateUrl: './purchageAgendaRequest.html',
        controller: 'purchageAgendaRequestController'
    }).when('/purchageAgendaRequestReport', {
        templateUrl: './purchageAgendaRequestReport.html',
        controller: 'purchageAgendaRequestReportController'
    }).when('/assetEmailTemplate', {
        templateUrl: './assetEmailTemplete.html',
        controller: 'assetEmailTemplateController'
    }).when('/assetDepartment', {
        templateUrl: './assetDepartment.html',
        controller: 'assetDepartmentController'
    }).when('/createAssetServiceLevelForecast', {
        templateUrl: './createAssetServiceLevelForecast.html',
        controller: 'createAssetServiceLevelForecastController'
    }).when('/employeebenfits', {
            templateUrl: './employeebenfits.html',
            controller: 'employeebenfitsController'
    }).when('/employeereports', {
            templateUrl: './employeereports.html',
            controller: 'employeereportsController'
    }).when('/leaveRequest', {
            templateUrl: './leaveRequest.html',
            controller: 'leaveRequestController'
    }).when('/raiseRequest', {
            templateUrl: './raiseRequest.html',
            controller: 'raiseRequestController'
    }).when('/employeeGrievanseStatus', {
        templateUrl: './employeeGrievanseStatus.html',
        controller: 'employeeGrievanseStatusController'
    }).when('/payslip', {
        templateUrl: './paySlips.html',
        controller: 'payslipController'
    }).when('/empDetails', {
       templateUrl: './empDetails.html',
       controller: 'empDetailsController'
    }).when('/MISReports', {
        templateUrl: './MISReports.html',
        controller: 'MISReportsController'
    }).otherwise({redirectTo: '/'});

});


// export to excel configuration

app.factory('Excel',function($window, Excel){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
})

app.service('anchorSmoothScroll', function(){

      this.scrollTo = function(eID) {

          // This scrolling function
          // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

          var startY = currentYPosition();
          var stopY = elmYPosition(eID);
          var distance = stopY > startY ? stopY - startY : startY - stopY;
          if (distance < 100) {
              scrollTo(0, stopY); return;
          }
          var speed = Math.round(distance / 100);
          if (speed >= 20) speed = 20;
          var step = Math.round(distance / 25);
          var leapY = stopY > startY ? startY + step : startY - step;
          var timer = 0;
          if (stopY > startY) {
              for ( var i=startY; i<stopY; i+=step ) {
                  setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                  leapY += step; if (leapY > stopY) leapY = stopY; timer++;
              } return;
          }
          for ( var i=startY; i>stopY; i-=step ) {
              setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
              leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
          }

          function currentYPosition() {
              // Firefox, Chrome, Opera, Safari
              if (self.pageYOffset) return self.pageYOffset;
              // Internet Explorer 6 - standards mode
              if (document.documentElement && document.documentElement.scrollTop)
                  return document.documentElement.scrollTop;
              // Internet Explorer 6, 7 and 8
              if (document.body.scrollTop) return document.body.scrollTop;
              return 0;
          }

          function elmYPosition(eID) {
              var elm = document.getElementById(eID);
              var y = elm.offsetTop;
              var node = elm;
              while (node.offsetParent && node.offsetParent != document.body) {
                  node = node.offsetParent;
                  y += node.offsetTop;
              } return y;
          }

      };

  });
//Master Controllers Start

 app.controller('assetTypeController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".navbar-nav li.landAndAsset").addClass("active").siblings('.active').removeClass('active');
         $(".navbar-nav li.landAndAsset li.assetType").addClass("active1").siblings('.active1').removeClass('active1');
         $(".navbar-nav li.schemes .collapse").removeClass("in");
         $(".navbar-nav li.landAndAsset .collapse").addClass("in");
         $(".navbar-nav li.configuration .collapse").removeClass("in");
         $(".navbar-nav li.administration .collapse").removeClass("in");

         $(".navbar-nav > li.projectWorks").addClass("active").siblings('.active').removeClass('active');
         $(".navbar-nav li.projectWorks li.masterData li.departmentList").addClass("active1").siblings('.active1').removeClass('active1');
         $(".navbar-nav li.projectWorks li.masterData .nav-list").addClass("active2").siblings('.active2').removeClass('active2');
         $(".navbar-nav li.projectWorks > .collapse").addClass("in");
         $(".navbar-nav li.masterData .collapse").addClass("in");
     });

      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Asset Type</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var asset=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+asset.name+"</td>" +
                      "<td>"+asset.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetType');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

     $scope.editAssetType = function(assetType) {
         $("#editAssetTypeModel").modal("show");
         $scope.errorMessage=false;
         /*$scope.editCharterArea = true;
          $scope.createCharterArea = false;*/
         $scope.editAssetTypeData=angular.copy(assetType);
     }
     $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
     $scope.loadingImage=true;
     $scope.removeMessaageInAdd= function () {
         $scope.errorMessage=false;
         $scope.assetType={};
     }
     $scope.assetType={};

     $scope.showButton=true;
     $scope.getAssetTypeData=function () {

         $http({
             method: 'GET',
             url: 'api/AssetTypes',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('Users Response :' + JSON.stringify(response));
             $scope.assetTypeList = response;

             for(var i = 0; i < $scope.assetTypeList.length; i++) {
             //console.log("++++++++++++" +JSON.stringify($scope.assetTypeList));
               if($scope.assetTypeList[i].name=='land' || $scope.assetTypeList[i].name=='vehicle')
               {
               $scope.assetTypeList[i]['editStatus']=false;
               }else{
               $scope.assetTypeList[i]['editStatus']=true;
               }

             }

             $scope.loadingImage=false;
         }).error(function (response) {
             if(response.error.details.messages.name) {
                 $scope.errorMessageData=response.error.details.messages.name[0];
                 $scope.errorMessage=true;
             }

         });
     }

      $scope.getAssetTypeData();

     $scope.errorMessage=false;

     var addAssetTypeSubmit = true;
     $scope.addAssetTypeData=function () {

         var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
         var assetType=$scope.assetType;
         assetType['createdPerson']=loginPersonDetails.name;
         assetType['employeeId']=loginPersonDetails.employeeId;

         if(addAssetTypeSubmit) {
             addAssetTypeSubmit = false;

             $http({
                 method: 'POST',
                 url: 'api/AssetTypes',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                 "data":assetType
             }).success(function (response) {
                 //console.log('Users Response :' + JSON.stringify(response));
                // $scope.loadingImage=true;
                 //$scope.loadingImage=true;
                 $scope.assetTypeList = response;
                 $("#addAssetType").modal("hide");
                 $("#addAssetSuccess").modal("show");
                 setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                 $scope.getAssetTypeData();
             }).error(function (response) {
                 if(response.error.details.messages.name) {
                     $scope.errorMessageData=response.error.details.messages.name[0];
                     $scope.errorMessage=true;
                 }

             });
         }

         addAssetTypeSubmit = true;

     }

     var editAssetTypeSubmit = true;

     $scope.editAssetTypeButton=function () {

         var editAssetTypeData= $scope.editAssetTypeData;
         var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
         editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
         editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;

         if(editAssetTypeSubmit) {

             editAssetTypeSubmit=false;
             $http({
                 "method": "PUT",
                 "url": 'api/AssetTypes/' + $scope.editAssetTypeData.id ,
                 "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                 "data": editAssetTypeData
             }).success(function (response, data) {
                 //$scope.loadingImage=true;
                 //console.log("filter Schemes "+ JSON.stringify(response));
                 $("#editAssetTypeModel").modal("hide");
                 $("#editAssetSuccess").modal("show");
                 setTimeout(function () {
                     $('#editAssetSuccess').modal('hide')
                 }, 3000);
                 $scope.getAssetTypeData();
             }).error(function (response, data) {
                 if (response.error.details.messages.name) {
                     $scope.errorMessageData = response.error.details.messages.name[0];
                     $scope.errorMessage = true;
                 }
             })
         }
         editAssetTypeSubmit=true;
     }

     //$scope.getAssetTypeData();

 });

 /*Employee Self Service*/

 app.controller('employeebenfitsController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

console.log("emp benefit controller called")
   });

 app.controller('employeereportsController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

console.log("emp reports controller called")
  });

 app.controller('leaveRequestController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

console.log("leave controller called");

$scope.checkedData = true;
$scope.uncheckedData = true;
           $scope.rejectedArea = true;
           $scope.approvedArea= true;

        $scope.approvedAssetType = function(assetType) {

            $scope.editAssetTypeData=angular.copy(assetType);
            var editAssetTypeData =$scope.editAssetTypeData ;
                       editAssetTypeData['status']="Approved";
                       console.log(JSON.stringify(editAssetTypeData));
                        $http({
                           "method": "PUT",
                           "url": 'http://54.189.195.233:3000/api/employeeLeaves/'+$scope.editAssetTypeData.id+'',
                           "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                           "data": editAssetTypeData
                       }).success(function (response, data) {
                      // $scope.checkedData = false;
                               //   $scope.rejectedArea = false;
                        //$scope.checkedData = false;
                           setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                           $scope.getAssetRiskGroupData();
                           //$window.location.reload();
                       }).error(function (response, data) {
                           if(response.error.details.messages.name) {
                               $scope.errorMessageData=response.error.details.messages.name[0];
                               $scope.errorMessage=true;
                           }
                       });
        }

        $scope.rejectedAssetType = function(assetType) {


            $scope.editAssetTypeData=angular.copy(assetType);
             var editAssetTypeData =$scope.editAssetTypeData ;
                                  editAssetTypeData['status']="Rejected";
                                   $http({
                                      "method": "PUT",
                                      "url": 'http://54.189.195.233:3000/api/employeeLeaves/'+$scope.editAssetTypeData.id+'',
                                      "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                                      "data": editAssetTypeData
                                  }).success(function (response, data) {

                                         //$scope.uncheckedData = false;
                                           //  $scope.approvedArea= false;
                                  //$scope.checkedData = false;
                                    //         $scope.rejectedArea = false;
                                   //$scope.checkedData = false;
                                      setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                                      $scope.getAssetRiskGroupData();
                                      //$window.location.reload();
                                  }).error(function (response, data) {
                                      if(response.error.details.messages.name) {
                                          $scope.errorMessageData=response.error.details.messages.name[0];
                                          $scope.errorMessage=true;
                                      }
                                  });


                }


        $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
        $scope.loadingImage=true;
        $scope.removeMessaageInAdd= function () {
            $scope.errorMessage=false;
            $scope.assetType={};
        }
        $scope.assetType={};
        $scope.getAssetRiskGroupData=function () {

            $http({
                method: 'GET',
                url: 'http://54.189.195.233:3000/api/employeeLeaves',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                console.log('getAssetRiskGroupData :' + JSON.stringify(response));
                $scope.loadingImage=false;
                $scope.assetTypeList = response;
                for(var i = 0; i < $scope.assetTypeList.length; i++) {
                             //console.log("++++++++++++" +JSON.stringify($scope.assetTypeList));
                               if($scope.assetTypeList[i].status=='Approved' || $scope.assetTypeList[i].status=='Rejected')
                               {
                               $scope.assetTypeList[i]['editStatus']=false;
                               }else{
                               $scope.assetTypeList[i]['editStatus']=true;
                               }

                             }
            }).error(function (response) {
                if(response.error.details.messages.name) {
                    $scope.errorMessageData=response.error.details.messages.name[0];
                    $scope.errorMessage=true;
                }

            });
        }

        $scope.getAssetRiskGroupData();

        $scope.editAssetRiskGroupButton=function () {
            var editAssetTypeData= $scope.editAssetTypeData;
            var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            //editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
            //editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
            if(editRiskGroupFormSubmit){
                editRiskGroupFormSubmit = false;
                $http({
                    "method": "PUT",
                    "url": 'http://54.189.195.233:3000/api/employeeLeaves/'+$scope.editAssetTypeData.id+'',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": editAssetTypeData
                }).success(function (response, data) {
                    //console.log("filter Schemes "+ JSON.stringify(response));
                    //$scope.loadingImage=true;
                    $("#editAssetTypeModel").modal("hide");
                    $("#editAssetSuccess").modal("show");
                    setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                    $scope.getAssetRiskGroupData();
                    //$window.location.reload();
                }).error(function (response, data) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }
                });
            }
            editRiskGroupFormSubmit = true;
        }

        //$scope.getAssetRiskGroupData();






  });

   app.controller('raiseRequestController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

  console.log("raiseRequestController called")




    });

    app.controller('empDetailsController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

    console.log("empDetailsController called")

    });



  app.controller('employeeGrievanseStatusController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

  /*     $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".sidebar-menu li ul > li").removeClass('active1');
           $('[data-toggle="tooltip"]').tooltip();
           $(".sidebar-menu #landAssetLi").addClass("active");
           $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
           $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
           $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetGroup").addClass("active1").siblings('.active1').removeClass('active1');
           $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetGroup .collapse").addClass("in");
           $(".sidebar-menu li .collapse").removeClass("in");
       });*/


       $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

        $scope.exportToExcel=function(tabelId){
            if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
                $("<tr>" +
                    "<th>Asset Group</th>" +
                    "<th>Status</th>" +
                    "</tr>").appendTo("table"+tabelId);
                for(var i=0;i<$scope.assetTypeList.length;i++){
                    var asset=$scope.assetTypeList[i];
                    $("<tr>" +
                        "<td>"+asset.name+"</td>" +
                        "<td>"+asset.status+"</td>" +
                        "</tr>").appendTo("table"+tabelId);
                }
                var exportHref=Excel.tableToExcel(tabelId,'assetGroup');
                $timeout(function(){location.href=exportHref;},100);
            }else{
                $("#emptyDataTable").modal("show");
                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
            }
        }

        $scope.editAssetType = function(assetType) {
            $("#editAssetTypeModel").modal("show");
            $scope.errorMessage=false;
            /*$scope.editCharterArea = true;
             $scope.createCharterArea = false;*/
            $scope.editAssetTypeData=angular.copy(assetType);
        }
        $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
        $scope.loadingImage=true;
        $scope.removeMessaageInAdd= function () {
            $scope.errorMessage=false;
            $scope.assetType={};
        }
        $scope.assetType={};
        $scope.getAssetRiskGroupData=function () {

            $http({
                method: 'GET',
                url: 'http://54.189.195.233:3000/api/citizenComplaints',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                console.log('getAssetRiskGroupData :' + JSON.stringify(response));
                $scope.loadingImage=false;
                $scope.assetTypeList = response;
            }).error(function (response) {
                if(response.error.details.messages.name) {
                    $scope.errorMessageData=response.error.details.messages.name[0];
                    $scope.errorMessage=true;
                }

            });
        }

        $scope.getAssetRiskGroupData();

        $scope.errorMessage=false;
        var addRiskGroupFormSubmit = true;
        $scope.addAssetRiskGroupData=function () {

            var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            var assetType=$scope.assetType;
            assetType['createdPerson']=loginPersonDetails.name;
            assetType['employeeId']=loginPersonDetails.employeeId;

            if(addRiskGroupFormSubmit){
                addRiskGroupFormSubmit = false;

                $http({
                    method: 'POST',
                    url: 'api/AssetRiskGroups',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    "data":assetType
                }).success(function (response) {
                    console.log('addAssetRiskGroupData :' + JSON.stringify(response));
                   $scope.loadingImage=true;
                 //   $scope.assetTypeList = response;
                    $("#addAssetType").modal("hide");
                    $("#addAssetSuccess").modal("show");
                    setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);

                    $scope.getAssetRiskGroupData();
                    //$window.location.reload();
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });

            }
            addRiskGroupFormSubmit = true;
        }

        var editRiskGroupFormSubmit = true;
        $scope.editAssetRiskGroupButton=function () {
            var editAssetTypeData= $scope.editAssetTypeData;
            var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
            editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
            editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
            if(editRiskGroupFormSubmit){
                editRiskGroupFormSubmit = false;
                $http({
                    "method": "PUT",
                    "url": 'http://54.189.195.233:3000/api/citizenComplaints/'+$scope.editAssetTypeData.id+'',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": editAssetTypeData
                }).success(function (response, data) {
                    //console.log("filter Schemes "+ JSON.stringify(response));
                    //$scope.loadingImage=true;
                    $("#editAssetTypeModel").modal("hide");
                    $("#editAssetSuccess").modal("show");
                    setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                    $scope.getAssetRiskGroupData();
                    //$window.location.reload();
                }).error(function (response, data) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }
                });
            }
            editRiskGroupFormSubmit = true;
        }

        //$scope.getAssetRiskGroupData();



    });

 /*Employee Self Service end*/

 
 app.controller('payslipController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    $(document).ready(function() {
        $('#country').on('change.states', function() {
          $("#march").toggle($(this).val() == 'march');
          $("#april").toggle($(this).val() == 'april');
        }).trigger('change.states');
      });
});

 app.controller('assetGroupController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetGroup").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetGroup .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Asset Group</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var asset=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+asset.name+"</td>" +
                      "<td>"+asset.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetGroup');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.editAssetType = function(assetType) {
          $("#editAssetTypeModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetTypeData=angular.copy(assetType);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetType={};
      }
      $scope.assetType={};
      $scope.getAssetRiskGroupData=function () {

          $http({
              method: 'GET',
              url: 'api/AssetRiskGroups',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('getAssetRiskGroupData :' + JSON.stringify(response));
              $scope.loadingImage=false;
              $scope.assetTypeList = response;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }

      $scope.getAssetRiskGroupData();

      $scope.errorMessage=false;
      var addRiskGroupFormSubmit = true;
      $scope.addAssetRiskGroupData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetType=$scope.assetType;
          assetType['createdPerson']=loginPersonDetails.name;
          assetType['employeeId']=loginPersonDetails.employeeId;

          if(addRiskGroupFormSubmit){
              addRiskGroupFormSubmit = false;

              $http({
                  method: 'POST',
                  url: 'api/AssetRiskGroups',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetType
              }).success(function (response) {
                  console.log('addAssetRiskGroupData :' + JSON.stringify(response));
                 $scope.loadingImage=true;
               //   $scope.assetTypeList = response;
                  $("#addAssetType").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);

                  $scope.getAssetRiskGroupData();
                  //$window.location.reload();
              }).error(function (response) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }

              });

          }
          addRiskGroupFormSubmit = true;
      }

      var editRiskGroupFormSubmit = true;
      $scope.editAssetRiskGroupButton=function () {
          var editAssetTypeData= $scope.editAssetTypeData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
          editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
          if(editRiskGroupFormSubmit){
              editRiskGroupFormSubmit = false;
              $http({
                  "method": "PUT",
                  "url": 'api/AssetRiskGroups/'+$scope.editAssetTypeData.id+'',
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetTypeData
              }).success(function (response, data) {
                  //console.log("filter Schemes "+ JSON.stringify(response));
                  //$scope.loadingImage=true;
                  $("#editAssetTypeModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetRiskGroupData();
                  //$window.location.reload();
              }).error(function (response, data) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }
              });
          }
          editRiskGroupFormSubmit = true;
      }

      //$scope.getAssetRiskGroupData();



  });

  app.controller('accountDetailsController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
        console.log("accountDetailsController");

      $(document).ready(function () {
          $('html,body').scrollTop(0);
          $(".sidebar-menu li ul > li").removeClass('active1');
          $('[data-toggle="tooltip"]').tooltip();
          $(".sidebar-menu #landAssetLi").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
          $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.accountDetails").addClass("active1").siblings('.active1').removeClass('active1');
          $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.accountDetails .collapse").addClass("in");
          $(".sidebar-menu li .collapse").removeClass("in");
      });


      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Account Number</th>" +
                  "<th>Details</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var asset=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+asset.accountNumber+"</td>" +
                      "<td>"+asset.details+"</td>" +
                      "<td>"+asset.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetGroup');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.editAssetType = function(assetType) {
          $("#editAssetTypeModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetTypeData=angular.copy(assetType);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetType={};
      }
      $scope.assetType={};
      $scope.getAssetRiskGroupData=function () {

          $http({
              method: 'GET',
              url: 'api/accountDetails',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('getAssetRiskGroupData :' + JSON.stringify(response));
              $scope.loadingImage=false;
              $scope.assetTypeList = response;
          }).error(function (response) {
              /*if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }*/

          });
      }

      $scope.getAssetRiskGroupData();

       $scope.reset = function () {
        $scope.assetType = angular.copy($scope.master);
          };


      $scope.errorMessage=false;
      var addRiskGroupFormSubmit = true;
      $scope.addAccountDetailsData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetType=$scope.assetType;
          assetType['createdPerson']=loginPersonDetails.name;
          assetType['employeeId']=loginPersonDetails.employeeId;

          if(addRiskGroupFormSubmit){
              addRiskGroupFormSubmit = false;

              $http({
                  method: 'POST',
                  url: 'api/accountDetails',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetType
              }).success(function (response) {
                  console.log('addAssetRiskGroupData :' + JSON.stringify(response));
                  $scope.loadingImage=true;
                  //   $scope.assetTypeList = response;
                  $("#addAssetType").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);

                  $scope.getAssetRiskGroupData();
                  //$window.location.reload();
              }).error(function (response) {
                  if(response.error.details.messages.accountNumber) {
                      $scope.errorMessageData=response.error.details.messages.accountNumber[0];
                      $scope.errorMessage=true;
                  }

              });

          }
          addRiskGroupFormSubmit = true;
      }

      var editRiskGroupFormSubmit = true;
      $scope.editAccountDetailsButton=function () {
          var editAssetTypeData= $scope.editAssetTypeData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
          editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
          if(editRiskGroupFormSubmit){
              editRiskGroupFormSubmit = false;
              $http({
                  "method": "PUT",
                  "url": 'api/accountDetails/'+$scope.editAssetTypeData.id,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetTypeData
              }).success(function (response, data) {
                  //console.log("filter Schemes "+ JSON.stringify(response));
                  //$scope.loadingImage=true;
                  $("#editAssetTypeModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetRiskGroupData();
                  //$window.location.reload();
              }).error(function (response, data) {
                  if(response.error.details.messages.accountNumber) {
                      $scope.errorMessageData=response.error.details.messages.accountNumber[0];
                      $scope.errorMessage=true;
                  }
              });
          }
          editRiskGroupFormSubmit = true;
      }

      //$scope.getAssetRiskGroupData();

$scope.reset();

  });

 app.controller('assetProfileController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetProfile").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetProfile .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Asset Profile</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var asset=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+asset.name+"</td>" +
                      "<td>"+asset.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetProfile');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }


      $scope.editAssetType = function(assetType) {
          $("#editAssetTypeModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetTypeData=angular.copy(assetType);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetType={};
      }
      $scope.assetType={};
      $scope.getAssetRiskProfileData=function () {

          $http({
              method: 'GET',
              url: 'api/AssetRiskProfiles',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.assetTypeList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }

      $scope.getAssetRiskProfileData();

       $scope.reset = function () {
              $scope.assetType = angular.copy($scope.master);
                };

      $scope.errorMessage=false;
      var addRiskProfileFormSubmit = true;
      $scope.addAssetRiskProfileData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetType=$scope.assetType;
          assetType['createdPerson']=loginPersonDetails.name;
          assetType['employeeId']=loginPersonDetails.employeeId;

          if(addRiskProfileFormSubmit){
              addRiskProfileFormSubmit = false;
              $http({
                  method: 'POST',
                  url: 'api/AssetRiskProfiles',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetType
              }).success(function (response) {
                  //console.log('Users Response :' + JSON.stringify(response));
                  $scope.loadingImage=true;
             //     $scope.assetTypeList = response;
                  $("#addAssetType").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetRiskProfileData();
                  //$window.location.reload();
              }).error(function (response) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }

              });
          }
          addRiskProfileFormSubmit = true;
      }

      var editRiskProfileFormSubmit = true;
      $scope.editAssetRiskProfileButton=function () {
          var editAssetTypeData= $scope.editAssetTypeData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
          editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;

          if(editRiskProfileFormSubmit){
              editRiskProfileFormSubmit = false;
              $http({
                  "method": "PUT",
                  "url": 'api/AssetRiskProfiles/'+$scope.editAssetTypeData.id,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetTypeData
              }).success(function (response, data) {
                  //console.log("filter Schemes "+ JSON.stringify(response));
                  //$scope.loadingImage=true;
                  $("#editAssetTypeModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetRiskProfileData();
                  //$window.location.reload();
              }).error(function (response, data) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }
              });
          } editRiskProfileFormSubmit = true;
      }


 $scope.reset();

  });

 app.controller('assetCriticalityController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetCriticality").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetCriticality .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Asset Criticality</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var asset=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+asset.name+"</td>" +
                      "<td>"+asset.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetCriticality');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.editAssetType = function(assetType) {
          $("#editAssetTypeModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetTypeData=angular.copy(assetType);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetType={};
      }
      $scope.assetType={};
      $scope.getAssetRiskCriticality=function () {

          $http({
              method: 'GET',
              url: 'api/AssetRiskCriticalities',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.assetTypeList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }

      $scope.getAssetRiskCriticality();

      $scope.errorMessage=false;
      $scope.reset = function () {
                     $scope.assetType = angular.copy($scope.master);
                 };


      var addRiskCriticalityFormSubmit = true;
      $scope.addAssetRiskCriticality=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetType=$scope.assetType;
          assetType['createdPerson']=loginPersonDetails.name;
          assetType['employeeId']=loginPersonDetails.employeeId;

          if(addRiskCriticalityFormSubmit){
              addRiskCriticalityFormSubmit = false;

              $http({
                  method: 'POST',
                  url: 'api/AssetRiskCriticalities',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetType
              }).success(function (response) {
                  //console.log('Users Response :' + JSON.stringify(response));
                  $scope.loadingImage=true;
             //     $scope.assetTypeList = response;
                  $("#addAssetType").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetRiskCriticality();


              }).error(function (response) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }

              });
          }
          addRiskCriticalityFormSubmit = true;

      }

      var editRiskCriticalityFormSubmit = true;
      $scope.editAssetRiskCriticalityButton=function () {
          var editAssetTypeData= $scope.editAssetTypeData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
          editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;

          if(editRiskCriticalityFormSubmit){
              editRiskCriticalityFormSubmit=false;
              $http({
                  "method": "PUT",
                  "url": 'api/AssetRiskCriticalities/'+$scope.editAssetTypeData.id,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetTypeData
              }).success(function (response, data) {
                  //$scope.loadingImage=true;
                  $("#editAssetTypeModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetRiskCriticality();

              }).error(function (response, data) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }
              });
          }
          editRiskCriticalityFormSubmit=true;

      }

      //$scope.getAssetRiskCriticality();
 $scope.reset();

  });

 app.controller('maintenanceObjectivesController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.maintenanceObjectives").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.maintenanceObjectives .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Maintenance Objectives</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var asset=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+asset.name+"</td>" +
                      "<td>"+asset.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'maintenanceObjectives');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }


      $scope.editAssetType = function(assetType) {
          $("#editAssetTypeModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetTypeData=angular.copy(assetType);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetType={};
      }
      $scope.assetType={};
      $scope.getMaintenanceObjectiveData=function () {

          $http({
              method: 'GET',
              url: 'api/MaintenanceObjectives',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.assetTypeList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }

      $scope.getMaintenanceObjectiveData();
$scope.reset = function () {
$scope.assetType = angular.copy($scope.master);
  };
      $scope.errorMessage=false;
      var addMaintenanceObjectiveFormSubmit = true;
      $scope.addMaintenanceObjectiveData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetType=$scope.assetType;
          assetType['createdPerson']=loginPersonDetails.name;
          assetType['employeeId']=loginPersonDetails.employeeId;

          if(addMaintenanceObjectiveFormSubmit){
              addMaintenanceObjectiveFormSubmit = false;

              $http({
                  method: 'POST',
                  url: 'api/MaintenanceObjectives',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetType
              }).success(function (response) {
                  //console.log('Users Response :' + JSON.stringify(response));

                  $scope.loadingImage=true;
                  $scope.assetTypeList = response;
                  $("#addAssetType").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getMaintenanceObjectiveData();

              }).error(function (response) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }

              });
          }
          addMaintenanceObjectiveFormSubmit = true;
      }

      var editMaintenanceObjectiveFormSubmit = true;
      $scope.editMaintenanceObjectiveButton=function () {
          var editAssetTypeData= $scope.editAssetTypeData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
          editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
          if(editMaintenanceObjectiveFormSubmit){
              editMaintenanceObjectiveFormSubmit = false;
              $http({
                  "method": "PUT",
                  "url": 'api/MaintenanceObjectives/'+$scope.editAssetTypeData.id,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetTypeData
              }).success(function (response, data) {
                  //console.log("filter Schemes "+ JSON.stringify(response));

                  //$scope.loadingImage=true;
                  $("#editAssetTypeModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getMaintenanceObjectiveData();

              }).error(function (response, data) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }
              });
          }
          editMaintenanceObjectiveFormSubmit = true;
      }

      //$scope.getMaintenanceObjectiveData();

$scope.reset();
  });

 app.controller('typeOfDutyController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.typeOfDuty").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.typeOfDuty .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Type</th>" +
                  "<th>Duty</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var asset=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+asset.name+"</td>" +
                      "<td>"+asset.duty+"</td>" +
                      "<td>"+asset.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'typeOfDuty');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.editAssetType = function(assetType) {
          $("#editAssetTypeModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetTypeData=angular.copy(assetType);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetType={};
      }
      $scope.assetType={};
      $scope.getTypeOfDutyData=function () {

          $http({
              method: 'GET',
              url: 'api/TypeOfDuties',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.assetTypeList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }
      $scope.errorMessage=false;
      $scope.reset = function () {
      $scope.assetType = angular.copy($scope.master);
        };

      $scope.getTypeOfDutyData();

      var addTypeOfDutyFormSubmit = true;
      $scope.addTypeOfDutyData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetType=$scope.assetType;
          assetType['createdPerson']=loginPersonDetails.name;
          assetType['employeeId']=loginPersonDetails.employeeId;

          if(addTypeOfDutyFormSubmit){
              addTypeOfDutyFormSubmit = false;

              $http({
                  method: 'POST',
                  url: 'api/TypeOfDuties',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetType
              }).success(function (response) {
                  //console.log('Users Response :' + JSON.stringify(response));

                  $scope.loadingImage=true;
                //  $scope.assetTypeList = response;
                  $("#addAssetType").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getTypeOfDutyData();

              }).error(function (response) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }

              });
          }
          addTypeOfDutyFormSubmit = true;
      }

      var editTypeOfDutyFormSubmit = true;
      $scope.editTypeOfDutyButton=function () {
          var editAssetTypeData= $scope.editAssetTypeData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
          editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
          if(editTypeOfDutyFormSubmit){
              editTypeOfDutyFormSubmit = false;
              $http({
                  "method": "PUT",
                  "url": 'api/TypeOfDuties/'+$scope.editAssetTypeData.id,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetTypeData
              }).success(function (response, data) {
                  //console.log("filter Schemes "+ JSON.stringify(response));

                  //$scope.loadingImage=true;
                  $("#editAssetTypeModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getTypeOfDutyData();

              }).error(function (response, data) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }
              });
          }
          editTypeOfDutyFormSubmit = true;
      }

      //$scope.getTypeOfDutyData();
$scope.reset();
  });

 app.controller('failureClassesController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.failureClasses").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.failureClasses .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Failure Classes</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var asset=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+asset.name+"</td>" +
                      "<td>"+asset.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'failureClasses');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.editAssetType = function(assetType) {
          $("#editAssetTypeModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetTypeData=angular.copy(assetType);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetType={};
      }
      $scope.assetType={};
      $scope.getFailureClassData=function () {

          $http({
              method: 'GET',
              url: 'api/FailureClasses',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.assetTypeList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }

      $scope.getFailureClassData();
$scope.reset = function () {
$scope.assetType = angular.copy($scope.master);
  };
      $scope.errorMessage=false;
      var addFailureClassFormSubmit = true;
      $scope.addFailureClassData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetType=$scope.assetType;
          assetType['createdPerson']=loginPersonDetails.name;
          assetType['employeeId']=loginPersonDetails.employeeId;

          if(addFailureClassFormSubmit){
              addFailureClassFormSubmit = false;

              $http({
                  method: 'POST',
                  url: 'api/FailureClasses',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetType
              }).success(function (response) {
                  //console.log('Users Response :' + JSON.stringify(response));

                  $scope.loadingImage=true;
              //    $scope.assetTypeList = response;
                  $("#addAssetType").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getFailureClassData();

              }).error(function (response) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }

              });
          }
          addFailureClassFormSubmit = true;
      }

      var editFailureClassFormSubmit = true;
      $scope.editFailureClassButton=function () {
          var editAssetTypeData= $scope.editAssetTypeData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
          editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
          if(editFailureClassFormSubmit){
              editFailureClassFormSubmit = false;
              $http({
                  "method": "PUT",
                  "url": 'api/FailureClasses/'+$scope.editAssetTypeData.id,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetTypeData
              }).success(function (response, data) {
                  console.log("filter Schemes "+ JSON.stringify(response));

                  //$scope.loadingImage=true;
                  //$scope.assetTypeList = response;
                  $("#editAssetTypeModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getFailureClassData();

              }).error(function (response, data) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }
              });
          }
          editFailureClassFormSubmit = true;
      }

      //$scope.getFailureClassData();
$scope.reset();
  });

 app.controller('assetSparesController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetSparesAndConsumables").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetSparesAndConsumables .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Asset Spares/Consumables</th>" +
                  "<th>Comments</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var asset=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+asset.name+"</td>" +
                      "<td>"+asset.comments+"</td>" +
                      "<td>"+asset.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetSpares');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.editAssetType = function(assetType) {
          $("#editAssetTypeModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetTypeData=angular.copy(assetType);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetType={};
      }
      $scope.assetType={};
      $scope.getAssetSparesData=function () {

          $http({
              method: 'GET',
              url: 'api/AssetSpares',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.assetTypeList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }

      $scope.getAssetSparesData();

$scope.reset = function () {
$scope.assetType = angular.copy($scope.master);
  };
      $scope.errorMessage=false;
      var addAssetSparesFormSubmit = true;
      $scope.addAssetSparesData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetType=$scope.assetType;
          assetType['createdPerson']=loginPersonDetails.name;
          assetType['employeeId']=loginPersonDetails.employeeId;

          if(addAssetSparesFormSubmit){
              addAssetSparesFormSubmit = false;

              $http({
                  method: 'POST',
                  url: 'api/AssetSpares',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetType
              }).success(function (response) {
                  //console.log('Users Response :' + JSON.stringify(response));

                  $scope.loadingImage=true;
             //     $scope.assetTypeList = response;
                  $("#addAssetType").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetSparesData();

              }).error(function (response) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }

              });
          }
          addAssetSparesFormSubmit = true;
      }

      var editAssetSparesFormSubmit = true;
      $scope.editAssetSparesButton=function () {
          var editAssetTypeData= $scope.editAssetTypeData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
          editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
          if(editAssetSparesFormSubmit){
              editAssetSparesFormSubmit = false;
              $http({
                  "method": "PUT",
                  "url": 'api/AssetSpares/'+$scope.editAssetTypeData.id,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetTypeData
              }).success(function (response, data) {
                  //console.log("filter Schemes "+ JSON.stringify(response));

                  //$scope.loadingImage=true;
                  $scope.assetTypeList = response;
                  $("#editAssetTypeModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetSparesData();

              }).error(function (response, data) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }
              });
          }
          editAssetSparesFormSubmit = true;
      }

      //$scope.getAssetSparesData();
$scope.reset();
  });

 app.controller('documentListMasterController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.documentListMaster").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.documentListMaster .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Document List Master</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var asset=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+asset.name+"</td>" +
                      "<td>"+asset.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'documentListMaster');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.editAssetType = function(assetType) {
          $("#editAssetTypeModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetTypeData=angular.copy(assetType);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetType={};
      }
      $scope.assetType={};
      $scope.getDocumentListMasterData=function () {

          $http({
              method: 'GET',
              url: 'api/DocumentListMasters',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.assetTypeList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }

      $scope.getDocumentListMasterData();
$scope.reset = function () {
$scope.assetType = angular.copy($scope.master);
  };
      $scope.errorMessage=false;
      var addDocumentListMasterFormSubmit = true;
      $scope.addDocumentListMasterData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetType=$scope.assetType;
          assetType['createdPerson']=loginPersonDetails.name;
          assetType['employeeId']=loginPersonDetails.employeeId;

          if(addDocumentListMasterFormSubmit){
              addDocumentListMasterFormSubmit = false;

              $http({
                  method: 'POST',
                  url: 'api/DocumentListMasters',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetType
              }).success(function (response) {

                  $scope.loadingImage=true;
            //      $scope.assetTypeList = response;
                  $("#addAssetType").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getDocumentListMasterData();

              }).error(function (response) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }

              });
          }
          addDocumentListMasterFormSubmit = true;
      }

      var editDocumentListMasterFormSubmit = true;
      $scope.editDocumentListMasterButton=function () {
          var editAssetTypeData= $scope.editAssetTypeData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
          editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
          if(editDocumentListMasterFormSubmit){
              editDocumentListMasterFormSubmit = false;
              $http({
                  "method": "PUT",
                  "url": 'api/DocumentListMasters/'+$scope.editAssetTypeData.id,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetTypeData
              }).success(function (response, data) {
                  //console.log("filter Schemes "+ JSON.stringify(response));

                  //$scope.loadingImage=true;
                  $("#editAssetTypeModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getDocumentListMasterData();

              }).error(function (response, data) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }
              });
          }
          editDocumentListMasterFormSubmit = true;
      }

      //$scope.getDocumentListMasterData();
$scope.reset();
  });

 app.controller('assetStatusController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetStatus").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetStatus .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetStatusList!=null &&  $scope.assetStatusList.length>0){
              $("<tr>" +
                  "<th>Asset Status</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetStatusList.length;i++){
                  var assetStatus=$scope.assetStatusList[i];
                  $("<tr>" +
                      "<td>"+assetStatus.name+"</td>" +
                      "<td>"+assetStatus.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetStatus');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }


      $scope.editAssetStatus = function(assetStatus) {
          $("#editAssetStatusModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetStatusData=angular.copy(assetStatus);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetStatus={};
      }
      $scope.assetStatus={};
      $scope.getAssetStatusData=function () {

                $http({
                    method: 'GET',
                    url: 'api/AssetStatuses',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    //console.log('Users Response :' + JSON.stringify(response));
                    $scope.assetStatusList = response;
                    for(var i = 0; i < $scope.assetStatusList.length; i++) {
                                 //console.log("++++++++++++" +JSON.stringify($scope.assetTypeList));
                                   if($scope.assetStatusList[i].name=='disposal' || $scope.assetStatusList[i].name=='decommissioned' || $scope.assetStatusList[i].name=='commissioned')
                                   {
                                   $scope.assetStatusList[i]['editStatus']=false;
                                   }else{
                                   $scope.assetStatusList[i]['editStatus']=true;
                                   }

                                 }
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }

      $scope.getAssetStatusData();
$scope.reset = function () {
$scope.assetStatus = angular.copy($scope.master);
  };
      $scope.errorMessage=false;

      var addAssetStatusSubmit = true;

      $scope.addAssetTypeData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetStatus=$scope.assetStatus;
          assetStatus['createdPerson']=loginPersonDetails.name;
          assetStatus['employeeId']=loginPersonDetails.employeeId;

          if(addAssetStatusSubmit) {
              addAssetStatusSubmit = false;

              $http({
                  method: 'POST',
                  url: 'api/AssetStatuses',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": assetStatus
              }).success(function (response) {
                  //console.log('Users Response :' + JSON.stringify(response));

                  $scope.loadingImage=true;
              //    $scope.assetTypeList = response;
                  $("#addAssetStatus").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetStatusData();

              }).error(function (response) {
                  if (response.error.details.messages.name) {
                      $scope.errorMessageData = response.error.details.messages.name[0];
                      $scope.errorMessage = true;
                  }

              });
          }
          addAssetStatusSubmit = true;
      }

      var editAssetStatusSubmit = true;

      $scope.editAssetStatusButton=function () {

          var editAssetStatusData= $scope.editAssetStatusData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetStatusData['lastEditPerson']=loginPersonDetails.name;
          editAssetStatusData['editemployeeId']=loginPersonDetails.employeeId;
          if(editAssetStatusSubmit) {
              editAssetStatusSubmit = false;

              $http({
                  "method": "PUT",
                  "url": 'api/AssetStatuses/' + $scope.editAssetStatusData.id ,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetStatusData
              }).success(function (response, data) {
                  //console.log("filter Schemes "+ JSON.stringify(response));

                  //$scope.loadingImage=true;
                  $("#editAssetStatusModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetStatusData();

              }).error(function (response, data) {
                  if (response.error.details.messages.name) {
                      $scope.errorMessageData = response.error.details.messages.name[0];
                      $scope.errorMessage = true;
                  }
              })
          }
          editAssetStatusSubmit = true;
      }


      //$scope.getAssetStatusData();
$scope.reset();
  });

 app.controller('assetKPIController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetKPIs").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetKPIs .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetKPIList!=null &&  $scope.assetKPIList.length>0){
              $("<tr>" +
                  "<th>Asset KPI's</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetKPIList.length;i++){
                  var assetKpi=$scope.assetKPIList[i];
                  $("<tr>" +
                      "<td>"+assetKpi.name+"</td>" +
                      "<td>"+assetKpi.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetStatus');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.editAssetKPI = function(assetKPI) {
          $("#editAssetKPIModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetKPIData=angular.copy(assetKPI);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetKPI={};
      }
      $scope.assetKPI={};
      $scope.getAssetKPIData=function () {

          $http({
              method: 'GET',
              url: 'api/AssetKPIs',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.assetKPIList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }

      $scope.getAssetKPIData();

      $scope.errorMessage=false;

      var assetKPISubmit=true;
      $scope.addAssetKPIData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetKPI=$scope.assetKPI;
          assetKPI['createdPerson']=loginPersonDetails.name;
          assetKPI['employeeId']=loginPersonDetails.employeeId;

          if(assetKPISubmit) {
              assetKPISubmit = false;
              $http({
                  method: 'POST',
                  url: 'api/AssetKPIs',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": assetKPI
              }).success(function (response) {
                  console.log('Users Response :' + JSON.stringify(response));
                  //  $scope.assetKPIList = response;
                  /*$scope.assetKPI=response;
                  console.log("name is" +JSON.stringify($scope.assetKPI.name));*/
                  $scope.loadingImage=true;
                //  $scope.assetKPIList = response;
                  $("#addAssetKPI").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetKPIData();
                   /* $scope.assetKPI = null;*/

              }).error(function (response) {
                  if (response.error.details.messages.name) {
                      $scope.errorMessageData = response.error.details.messages.name[0];
                      $scope.errorMessage = true;
                  }
              });
          }
          assetKPISubmit=true;
      }

      var editAssetKPISubmit=true;

      $scope.editAssetKPIButton=function () {

          var editAssetKPIData= $scope.editAssetKPIData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetKPIData['lastEditPerson']=loginPersonDetails.name;
          editAssetKPIData['editemployeeId']=loginPersonDetails.employeeId;

          if(editAssetKPISubmit) {
              editAssetKPISubmit = false;
              $http({
                  "method": "PUT",
                  "url": 'api/AssetKPIs/' + $scope.editAssetKPIData.id ,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetKPIData
              }).success(function (response, data) {
                  //console.log("filter Schemes "+ JSON.stringify(response));
                  //$scope.loadingImage=true;
                  $("#editAssetKPIModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetKPIData();
              }).error(function (response, data) {
                  if (response.error.details.messages.name) {
                      $scope.errorMessageData = response.error.details.messages.name[0];
                      $scope.errorMessage = true;
                  }
              })
          }
          editAssetKPISubmit=true;
      }

      //$scope.getAssetKPIData();

  });

 app.controller('assetServiceLevelController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

      console.log("assetServiceLevelController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetServiceLevel").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetServiceLevel .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetServiceLevelList!=null &&  $scope.assetServiceLevelList.length>0){
              $("<tr>" +
                  "<th>Asset Service Level</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetServiceLevelList.length;i++){
                  var service=$scope.assetServiceLevelList[i];
                  $("<tr>" +
                      "<td>"+service.description+"</td>" +
                      "<td>"+service.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetStatus');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetServiceLevel={};
      }
      $scope.assetServiceLevel={};
      $scope.getAssetServiceLevelData=function () {

          $http({
              method: 'GET',
              url: 'api/AssetServiceLevels',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('Users Response :' + JSON.stringify(response));
              $scope.assetServiceLevelList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.description) {
                  $scope.errorMessageData=response.error.details.messages.description[0];
                  $scope.errorMessage=true;
              }

          });
      }
      $scope.getAssetServiceLevelData();
$scope.reset = function () {
$scope.assetServiceLevel = angular.copy($scope.master);
  };
      $scope.errorMessage=false;
      var addAssetServiceLevelSubmit=true;
      $scope.addAssetServiceLevelData=function () {


          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetServiceLevel=$scope.assetServiceLevel;
          assetServiceLevel['createdPerson']=loginPersonDetails.name;
          assetServiceLevel['employeeId']=loginPersonDetails.employeeId;

          if(addAssetServiceLevelSubmit) {
              addAssetServiceLevelSubmit = false;
              $http({
                  method: 'POST',
                  url: 'api/AssetServiceLevels',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": assetServiceLevel
              }).success(function (response) {
                  console.log('Users Response :' + JSON.stringify(response));
                  $scope.loadingImage=true;
              //    $scope.assetServiceLevelList = response;
                  $("#addAssetServiceLevel").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetServiceLevelData();

              }).error(function (response) {
                  if (response.error.details.messages.description) {
                      $scope.errorMessageData = response.error.details.messages.description[0];
                      $scope.errorMessage = true;
                  }

              });
          }
          addAssetServiceLevelSubmit = true;
      }

      $scope.editAssetServiceLevel = function(assetServiceLevel) {
          $("#editAssetServiceLevelModel").modal("show");
          $scope.errorMessage=false;

          $scope.editAssetServiceLevelData=angular.copy(assetServiceLevel);
      }

      var editAssetServiceLevelSubmit=true;
      $scope.editAssetServiceLevelSubmit=function () {

          var editAssetServiceLevelData= $scope.editAssetServiceLevelData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetServiceLevelData['lastEditPerson']=loginPersonDetails.name;
          editAssetServiceLevelData['editemployeeId']=loginPersonDetails.employeeId;
          if(editAssetServiceLevelSubmit) {
              editAssetServiceLevelSubmit=false;
              $http({
                  "method": "PUT",
                  "url": 'api/AssetServiceLevels/' + $scope.editAssetServiceLevelData.id ,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetServiceLevelData
              }).success(function (response, data) {

                  //$scope.loadingImage=true;
                  $("#editAssetServiceLevelModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetServiceLevelData();

              }).error(function (response, data) {
                  if (response.error.details.messages.description) {
                      $scope.errorMessageData = response.error.details.messages.description[0];
                      $scope.errorMessage = true;
                  }
              });
          }
          editAssetServiceLevelSubmit=true;
      }

      //$scope.getAssetServiceLevelData();
$scope.reset();
  });

 app.controller('disposalMethodController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

      console.log("disposalMethodController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetDisposalMethods").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetDisposalMethods .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetDisposalMethodsList!=null &&  $scope.assetDisposalMethodsList.length>0){
              $("<tr>" +
                  "<th>Asset Disposal Method</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetDisposalMethodsList.length;i++){
                  var disposal=$scope.assetDisposalMethodsList[i];
                  $("<tr>" +
                      "<td>"+disposal.name+"</td>" +
                      "<td>"+disposal.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'disposalMethods');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetDisposalMethod={};
      }
      $scope.assetDisposalMethod={};
      $scope.getAssetDisposalMethodsData=function () {

          $http({
              method: 'GET',
              url: 'api/AssetDisposalMethods',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('Asset Disposal Methods Response :' + JSON.stringify(response));
              $scope.assetDisposalMethodsList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }
      $scope.getAssetDisposalMethodsData();
$scope.reset = function () {
$scope.assetDisposalMethod = angular.copy($scope.master);
  };
      $scope.errorMessage=false;
      var addAssetDisposalMethodSubmit=true;
      $scope.addAssetDisposalMethodData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetDisposalMethod=$scope.assetDisposalMethod;
          assetDisposalMethod['createdPerson']=loginPersonDetails.name;
          assetDisposalMethod['employeeId']=loginPersonDetails.employeeId;

          if(addAssetDisposalMethodSubmit) {
              addAssetDisposalMethodSubmit = false;
              $http({
                  method: 'POST',
                  url: 'api/AssetDisposalMethods',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": assetDisposalMethod
              }).success(function (response) {
                  $scope.loadingImage=true;
              //    $scope.assetDisposalMethodsList = response;
                  $("#addAssetServiceLevel").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetDisposalMethodsData();
              }).error(function (response) {
                  if (response.error.details.messages.name) {
                      $scope.errorMessageData = response.error.details.messages.name[0];
                      $scope.errorMessage = true;
                  }


              });
          }
          addAssetDisposalMethodSubmit=true;


      }

      $scope.editAssetDisposalMethod = function(assetServiceLevel) {
          // alert("edit data:::" +JSON.stringify(assetDisposalMethod));
          $("#editAssetServiceLevelModel").modal("show");
          // alert(JSON.stringify(assetServiceLevel));
          $scope.editAssetDisposalMethodData=angular.copy(assetServiceLevel);
      }


      var editAssetDisposalMethodSubmit=true;
      $scope.editAssetDisposalData=function () {

          var editAssetDisposalMethodData= $scope.editAssetDisposalMethodData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetDisposalMethodData['lastEditPerson']=loginPersonDetails.name;
          editAssetDisposalMethodData['editemployeeId']=loginPersonDetails.employeeId;
          if(editAssetDisposalMethodSubmit) {
              editAssetDisposalMethodSubmit=false;
              $http({
                  "method": "PUT",
                  "url": 'api/AssetDisposalMethods/' + $scope.editAssetDisposalMethodData.id ,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetDisposalMethodData
              }).success(function (response, data) {
                  //$scope.loadingImage=true;
                  $("#editAssetServiceLevelModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetDisposalMethodsData();

              }).error(function (response, data) {
                  if (response.error.details.messages.name) {
                      $scope.errorMessageData = response.error.details.messages.name[0];
                      $scope.errorMessage = true;
                  }
              });
          }
          editAssetDisposalMethodSubmit=true;
      }

      //$scope.getAssetDisposalMethodsData();
$scope.reset();
  });

 app.controller('assetRisksController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

      console.log("assetRisksController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetRisks").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetRisks .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetRiskList!=null &&  $scope.assetRiskList.length>0){
              $("<tr>" +
                  "<th>Asset Risk</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetRiskList.length;i++){
                  var assetRisk=$scope.assetRiskList[i];
                  $("<tr>" +
                      "<td>"+assetRisk.name+"</td>" +
                      "<td>"+assetRisk.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'disposalMethods');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetRisk={};
      }
      $scope.assetRisk={};
      $scope.getAssetRiskDetails=function () {

          $http({
              method: 'GET',
              url: 'api/AssetRisks',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('Asset Disposal Methods Response :' + JSON.stringify(response));
              $scope.assetRiskList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }
      $scope.getAssetRiskDetails();
$scope.reset = function () {
$scope.assetRisk = angular.copy($scope.master);
  };
      $scope.errorMessage=false;
      var addAssetRiskSubmit=true;
      $scope.addAssetRisks=function () {


          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetRisk=$scope.assetRisk;
          assetRisk['createdPerson']=loginPersonDetails.name;
          assetRisk['employeeId']=loginPersonDetails.employeeId;

          if(addAssetRiskSubmit) {
              addAssetRiskSubmit = false;
              $http({
                  method: 'POST',
                  url: 'api/AssetRisks',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": assetRisk
              }).success(function (response) {
                  // addAssetRiskSubmit = true;
                  $scope.loadingImage=true;
              //   $scope.assetRiskPostList = response;
                  $("#addAssetRiskPopup").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetRiskDetails();


              }).error(function (response) {
                  addAssetRiskSubmit = true;
                  if (response.error.details.messages.name) {
                      $scope.errorMessageData = response.error.details.messages.name[0];
                      $scope.errorMessage = true;
                  }


              });
          }

          addAssetRiskSubmit=true;


      }

      $scope.editAssetRiskPopupInfo = function(assetRisks) {
          // alert("edit data:::" +JSON.stringify(assetDisposalMethod));
          $("#editAssetRiskPopup").modal("show");
          //alert(JSON.stringify(assetRisks));
          $scope.editAssetRiskData=angular.copy(assetRisks);
      }


      var editAssetRiskSubmit=true;
      $scope.editAssetRisks=function () {

          var editAssetRiskData= $scope.editAssetRiskData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetRiskData['lastEditPerson']=loginPersonDetails.name;
          editAssetRiskData['editemployeeId']=loginPersonDetails.employeeId;
          if(editAssetRiskSubmit) {
              editAssetRiskSubmit=false;
              $http({
                  "method": "PUT",
                  "url": 'api/AssetRisks/' + $scope.editAssetRiskData.id ,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetRiskData
              }).success(function (response, data) {
                  // editAssetRiskSubmit=true;
                  //$scope.loadingImage=true;
                  $("#editAssetRiskPopup").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetRiskDetails();

              }).error(function (response, data) {
                  editAssetRiskSubmit=true;
                  if (response.error.details.messages.name) {
                      $scope.errorMessageData = response.error.details.messages.name[0];
                      $scope.errorMessage = true;
                  }
              });
          }
          editAssetRiskSubmit=true;
      }


      //$scope.getAssetRiskDetails();
$scope.reset();
  });

 app.controller('costCenterController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {


      console.log("costCenterController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.costCenter").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.costCenter .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetTypeList!=null &&  $scope.assetTypeList.length>0){
              $("<tr>" +
                  "<th>Cost Center</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetTypeList.length;i++){
                  var type=$scope.assetTypeList[i];
                  $("<tr>" +
                      "<td>"+type.name+"</td>" +
                      "<td>"+type.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'costCenters');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.editAssetType = function(assetType) {
          $("#editAssetTypeModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetTypeData=angular.copy(assetType);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetType={};
      }
      $scope.assetType={};
      $scope.getCostCenterData=function () {

          $http({
              method: 'GET',
              url: 'api/CostCenters',
              /*url: 'http://localhost:8866/api/CostCenters',*/
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.assetTypeList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }
      $scope.getCostCenterData();
$scope.reset = function () {
$scope.assetType = angular.copy($scope.master);
  };
      $scope.errorMessage=false;
      var addCostCenterFormSubmit = true;
      $scope.addCostCenterData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetType=$scope.assetType;
          assetType['createdPerson']=loginPersonDetails.name;
          assetType['employeeId']=loginPersonDetails.employeeId;

          if(addCostCenterFormSubmit){
              addCostCenterFormSubmit = false;

              $http({
                  method: 'POST',
                  url: 'api/CostCenters',
                  /*url: 'http://localhost:8866/api/CostCenters',*/
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetType
              }).success(function (response) {
                  $scope.loadingImage=true;
               //   $scope.assetTypeList = response;
                  $("#addAssetType").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getCostCenterData();

              }).error(function (response) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }

              });
          }
          addCostCenterFormSubmit = true;
      }

      var editCostCenterFormSubmit = true;
      $scope.editCostCenterButton=function () {
          var editAssetTypeData= $scope.editAssetTypeData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetTypeData['lastEditPerson']=loginPersonDetails.name;
          editAssetTypeData['editemployeeId']=loginPersonDetails.employeeId;
          if(editCostCenterFormSubmit){
              editCostCenterFormSubmit = false;
              $http({
                  "method": "PUT",
                  "url": 'api/CostCenters/'+$scope.editAssetTypeData.id,
                  /*"url": 'http://localhost:8866/api/CostCenters/'+$scope.editAssetTypeData.id+'?access_token='+$scope.accessTokenDetails,*/
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetTypeData
              }).success(function (response, data) {
                  //$scope.loadingImage=true;
                  $("#editAssetTypeModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getCostCenterData();

              }).error(function (response, data) {
                  if(response.error.details.messages.name) {
                      $scope.errorMessageData=response.error.details.messages.name[0];
                      $scope.errorMessage=true;
                  }
              });
          }
          editCostCenterFormSubmit = true;
      }

      //$scope.getCostCenterData();

$scope.reset()

  });

 app.controller('assetPoliciesController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

      console.log("assetPoliciesController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetPolicies").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetPolicies .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetPolicyList!=null &&  $scope.assetPolicyList.length>0){
              $("<tr>" +
                  "<th>Asset Policy</th>" +
                  "<th>Asset Strategy</th>" +
                  "<th>Asset Objective</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetPolicyList.length;i++){
                  var type=$scope.assetPolicyList[i];
                  $("<tr>" +
                      "<td>"+type.policy+"</td>" +
                      "<td>"+type.strategy+"</td>" +
                      "<td>"+type.objective+"</td>" +
                      "<td>"+type.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetPolicies');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.editAssetPolicy = function(assetPolicy) {
          $("#editAssetPolicyModel").modal("show");
          $scope.errorMessage=false;

          $scope.editAssetPolicyData=angular.copy(assetPolicy);
      }
      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetPolicy={};
      }
      $scope.assetPolicy={};
      $scope.getAssetPolicyData=function () {

          $http({
              method: 'GET',
              url: 'api/AssetPolicies',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('Users Response :' + JSON.stringify(response));
              $scope.assetPolicyList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.policy) {
                  $scope.errorMessageData=response.error.details.messages.policy[0];
                  $scope.errorMessage=true;
              }

          });
      }

      $scope.getAssetPolicyData();
$scope.reset = function () {
$scope.assetPolicy = angular.copy($scope.master);
  };
      $scope.errorMessage=false;
      var addAssetPolicySubmit=true;
      $scope.addAssetPolicy=function () {


          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetPolicy=$scope.assetPolicy;
          assetPolicy['createdPerson']=loginPersonDetails.name;
          assetPolicy['employeeId']=loginPersonDetails.employeeId;

          if(addAssetPolicySubmit) {
              addAssetPolicySubmit = false;
              $http({
                  method: 'POST',
                  url: 'api/AssetPolicies',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": assetPolicy
              }).success(function (response) {
                  console.log('Users Response :' + JSON.stringify(response));
                  $scope.loadingImage=true;
              //   $scope.assetPolicyList = response;
                  $("#addAssetPolicyModel").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetPolicyData();

              }).error(function (response) {
                  if (response.error.details.messages.policy) {
                      $scope.errorMessageData = response.error.details.messages.policy[0];
                      $scope.errorMessage = true;
                  }


              });
          }
          addAssetPolicySubmit=true;
          //$scope.getAssetPolicyData();
      }
      var editAssetPolicySubmit=true;
      $scope.editAssetPolicyButton=function () {

          var editAssetPolicyData= $scope.editAssetPolicyData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          console.log("loginPersonDetails" +JSON.stringify(loginPersonDetails));
          editAssetPolicyData['lastEditPerson']=loginPersonDetails.name;
          editAssetPolicyData['editemployeeId']=loginPersonDetails.employeeId;
          if(editAssetPolicySubmit) {
              editAssetPolicySubmit=false;
              $http({
                  "method": "PUT",
                  "url": 'api/AssetPolicies/' + $scope.editAssetPolicyData.id ,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetPolicyData
              }).success(function (response, data) {
                  console.log("filter Schemes "+ JSON.stringify(response));
                  //$scope.loadingImage=true;
                  $("#editAssetPolicyModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetPolicyData();

              }).error(function (response, data) {
                  if (response.error.details.messages.policy) {
                      $scope.errorMessageData = response.error.details.messages.policy[0];
                      $scope.errorMessage = true;
                  }
              });
          }editAssetPolicySubmit=true;
      }

      //$scope.getAssetPolicyData();
$scope.reset();
  });

 app.controller('assetLifeCyclePlanController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

      console.log("assetLifeCyclePlanController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetLifeCyclePlan").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetLifeCyclePlan .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.assetPhaseList!=null &&  $scope.assetPhaseList.length>0){
              $("<tr>" +
                  "<th>Asset Phase</th>" +
                  "<th>Status</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.assetPhaseList.length;i++){
                  var type=$scope.assetPhaseList[i];
                  $("<tr>" +
                      "<td>"+type.assetPhase+"</td>" +
                      "<td>"+type.status+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'assetLifeCyclePlan');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.accessTokenDetails = $window.localStorage.getItem('accessToken');
      $scope.loadingImage=true;
      $scope.removeMessaageInAdd= function () {
          $scope.errorMessage=false;
          $scope.assetPhase={};
      }
      $scope.assetPhase={};
      $scope.getAssetPhaseData=function () {

          $http({
              method: 'GET',
              url: 'api/AssetLifeCyclePlans',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.assetPhaseList = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.assetPhase) {
                  $scope.errorMessageData=response.error.details.messages.assetPhase[0];
                  $scope.errorMessage=true;
              }

          });
      }
      $scope.getAssetPhaseData();
$scope.reset = function () {
$scope.assetPhase = angular.copy($scope.master);
  };
      $scope.errorMessage=false;
      var addAssetPhaseDataSubmit=true;
      $scope.addAssetPhaseData=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var assetPhase=$scope.assetPhase;
          assetPhase['createdPerson']=loginPersonDetails.name;
          assetPhase['employeeId']=loginPersonDetails.employeeId;

          if(addAssetPhaseDataSubmit)
          {
              addAssetPhaseDataSubmit=false;
              $http({
                  method: 'POST',
                  url: 'api/AssetLifeCyclePlans',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data":assetPhase
              }).success(function (response) {
                  console.log('Users Response :' + JSON.stringify(response));
                  $scope.loadingImage=true;
               //   $scope.assetPhaseList = response;
                  $("#addAssetPhase").modal("hide");
                  $("#addAssetSuccess").modal("show");
                  setTimeout(function(){$('#addAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetPhaseData();

              }).error(function (response) {
                  if(response.error.details.messages.assetPhase) {
                      $scope.errorMessageData=response.error.details.messages.assetPhase[0];
                      $scope.errorMessage=true;
                  }
              });
          }
          addAssetPhaseDataSubmit=true;
      }

      $scope.editAssetPhase = function(assetPhase) {
          $("#editAssetPhaseModel").modal("show");
          $scope.errorMessage=false;
          /*$scope.editCharterArea = true;
           $scope.createCharterArea = false;*/
          $scope.editAssetPhaseData=angular.copy(assetPhase);
      }

      var editAssetPhaseDataSubmit=true;
      $scope.editAssetPhaseButton=function () {

          var editAssetPhaseData= $scope.editAssetPhaseData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editAssetPhaseData['lastEditPerson']=loginPersonDetails.name;
          editAssetPhaseData['editemployeeId']=loginPersonDetails.employeeId;
          if(editAssetPhaseDataSubmit)
          {
              editAssetPhaseDataSubmit=false;
              $http({
                  "method": "PUT",
                  "url": 'api/AssetLifeCyclePlans/'+$scope.editAssetPhaseData.id,
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": editAssetPhaseData
              }).success(function (response, data) {
                  console.log("filter Schemes "+ JSON.stringify(response));
                  //$scope.loadingImage=true;
                  $("#editAssetPhaseModel").modal("hide");
                  $("#editAssetSuccess").modal("show");
                  setTimeout(function(){$('#editAssetSuccess').modal('hide')}, 3000);
                  $scope.getAssetPhaseData();

              }).error(function (response, data) {
                  if(response.error.details.messages.assetPhase) {
                      $scope.errorMessageData=response.error.details.messages.assetPhase[0];
                      $scope.errorMessage=true;
                  }
              })
          }
          editAssetPhaseDataSubmit=true;
      }

      //$scope.getAssetPhaseData();
$scope.reset();
  });


 //Master Controllers End


 app.controller('assetEmailTemplateController', function($http, $scope, $window, $location, $rootScope) {
     console.log("assetEmailTemplateController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landMasterDataPro.active #masterProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetEmailConfiguration").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landMasterDataPro.active #masterProSetUp li.assetEmailConfiguration .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

     $scope.loadingImage=true;


     $scope.getEmail = function () {
         $http({
             method: 'GET',
             url: 'api/EmailTempletes/?filter={"where":{"emailType":"' + "landAndAsset" + '"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             console.log('Users Response :' + JSON.stringify(response));
             $scope.email = response[0];
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }
     $scope.getEmail();

     $scope.getProjectDetailsForNotification = function() {

         var editEmailData= $scope.email;

         //alert("email input" +JSON.stringify($scope.email));
         var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
         editEmailData.lastEditPerson=loginPersonDetails.name;
         $http({
             "method": "PUT",
             "url": 'api/EmailTempletes/'+$scope.email.id,
             "headers": {"Content-Type": "application/json", "Accept": "application/json"},
             "data": editEmailData
         }).success(function (response, data) {
             $scope.loadingImage=false;
             console.log("filter land and asset "+ JSON.stringify(response));
             $scope.emailAlert = response;
             $scope.getEmail();
             //$window.location.reload();
             console.log("filter land and asset alert "+ JSON.stringify($scope.emailAlert));
             $("#registerSuccess").modal("show");
             setTimeout(function(){$('#registerSuccess').modal('hide')}, 2000);
             /*$scope.getEmail();*/
         }).error(function (response, data) {
             console.log("failure");
         })
     }
     $scope.getEmail();


 });


 app.controller('createAssetLandController', function($http, $scope, $window,$location, $rootScope, Excel,Upload,$timeout, DTOptionsBuilder, DTColumnBuilder) {

     $scope.$on("$routeChangeSuccess",function(){
        $scope.showLand=true;

     });

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetLand").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetLand .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

     $scope.uploadFile=uploadFileURL;
     //$scope.uploadFile='api/Uploads/dhanbadDb/download/';
     //$scope.docUploadURL = 'apiUploads/dhanbadDb/download/';
     $scope.dtOptions = {paging: false, searching: false}
     $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
         .withOption("sPaginationType", "bootstrap")
         .withOption("retrieve", true)
         .withOption('stateSave', true)
         .withPaginationType('simple_numbers')
         .withOption('order', [0, 'ASC']);

     $scope.exportToExcel=function(tabelId){
         if( $scope.landData!=null &&  $scope.landData.length>0){
             $("<tr>" +
                 "<th lang=en>Asset Id</th>" +
                 "<th>Asset Type</th>" +
                 "<th>Asset Description</th>" +
                 "<th>Acquisition Comments</th>" +
                 "<th>Acquisition Status</th>" +
                 "<th>Address Details</th>" +
                 "<th>Adjustment Account</th>" +
                 "<th>Amortization Amount</th>" +
                 "<th>Amortization Method</th>" +
                 "<th>Amortization Rate</th>" +
                 "<th>Area In Units</th>" +
                 "<th>Asset Last Review Date</th>" +
                 "<th>Asset Mode</th>" +
                 "<th>Asset Name</th>" +
                 "<th>Asset Renewable Availability</th>" +
                 "<th>Asset Spares Comment</th>" +
                 "<th>Asset Spares Name</th>" +
                 "<th>Asset Upgrade Date</th>" +
                 "<th>Book Value</th>" +
                 "<th>Contact Person</th>" +
                 "<th>Contact Person Department</th>" +
                 "<th>Control Account</th>" +
                 "<th>Cost Center</th>" +
                 "<th>Created Person</th>" +
                 "<th>Criticality Asset</th>" +
                 "<th>Currency Variance</th>" +
                 "<th>Duty</th>" +
                 "<th>Duty Type</th>" +
                 "<th>Employee ID</th>" +
                 "<th>Expected Date Of Disposal</th>" +
                 "<th>Historic Cost Availability</th>" +
                 "<th>Impairment Life</th>" +
                 "<th>Initial Valuation Cost</th>" +
                 "<th>Historic Cost</th>" +
                 "<th>Land Details</th>" +
                 "<th>Leased Date</th>" +
                 "<th>Legal Formality Status</th>" +
                 "<th>Locality</th>" +
                 "<th>Maintenance Department</th>" +
                 "<th>Maintenance Contact Name</th>" +
                 "<th>Muncipality or Mandal </th>" +
                 "<th>Original Life</th>" +
                 "<th>Original Remaining Life</th>" +
                 "<th>Owner Name</th>" +
                 "<th>Ownership Transfer Status</th>" +
                 "<th>Mobile Number</th>" +
                 "<th>Price Variance</th>" +
                 "<th>Purchase Price Variance</th>" +
                 "<th>Receipt Variance</th>" +
                 "<th>Registration Details</th>" +
                 "<th>Replacement Due Date</th>" +
                 "<th>Request Transfer Name</th>" +
                 "<th>Required Date</th>" +
                 "<th>Return Value</th>" +
                 "<th>Revised Remaining Life</th>" +
                 "<th>Scrap Value</th>" +
                 "<th>Shrinkage Account</th>" +
                 "<th>Since Date</th>" +
                 "<th>Status</th>" +
                 "<th>Terms In Months</th>" +
                 "<th>Village</th>" +
                 "<th>Ward</th>" +
                 "</tr>").appendTo("table"+tabelId);
             for(var i=0;i<$scope.landData.length;i++){
                 var land=$scope.landData[i];
                 $("<tr>" +
                     "<td>"+land.assetNumber+"</td>" +
                     "<td>"+land.assetType+"</td>" +
                     "<td>"+land.assetDescription+"</td>" +
                     "<td>"+land.acquisitionComments+"</td>" +
                     "<td>"+land.acquisitionStatus+"</td>" +
                     "<td>"+land.addressDetails+"</td>" +
                     "<td>"+land.adjustmentAccount+"</td>" +
                     "<td>"+land.amortizationAmount+"</td>" +
                     "<td>"+land.amortizationMethod+"</td>" +
                     "<td>"+land.amortizationRate+"</td>" +
                     "<td>"+land.areaInUnits+"</td>" +
                     "<td>"+land.assetLastReviewedDate+"</td>" +
                     "<td>"+land.assetMode+"</td>" +
                     "<td>"+land.assetName+"</td>" +
                     "<td>"+land.assetRenewableAvailability+"</td>" +
                     "<td>"+land.assetSparesComment+"</td>" +
                     "<td>"+land.assetSparesName+"</td>" +
                     "<td>"+land.assetUpgradeDate+"</td>" +
                     "<td>"+land.bookValue+"</td>" +
                     "<td>"+land.contactPerson+"</td>" +
                     "<td>"+land.contactPersonDepartment+"</td>" +
                     "<td>"+land.controlAccount+"</td>" +
                     "<td>"+land.costCenter+"</td>" +
                     "<td>"+land.createdPerson+"</td>" +
                     "<td>"+land.createdTime+"</td>" +
                     "<td>"+land.criticalityAsset+"</td>" +
                     "<td>"+land.currencyVariance+"</td>" +
                     "<td>"+land.duty+"</td>" +
                     "<td>"+land.dutyType+"</td>" +
                     "<td>"+land.employeeId+"</td>" +
                     "<td>"+land.expectedDateOfDisposal+"</td>" +
                     "<td>"+land.historicCostAvailability+"</td>" +
                     "<td>"+land.impairmentLife+"</td>" +
                     "<td>"+land.initialValuationCost+"</td>" +
                     "<td>"+land.landDetails+"</td>" +
                     "<td>"+land.leasedDate+"</td>" +
                     "<td>"+land.legalFormalityStatus+"</td>" +
                     "<td>"+land.locality+"</td>" +
                     "<td>"+land.maintenanceContactDepartment+"</td>" +
                     "<td>"+land.maintenanceContactName+"</td>" +
                     "<td>"+land.muncipalityOrMandal+"</td>" +
                     "<td>"+land.originalLife+"</td>" +
                     "<td>"+land.originalRemainingLife+"</td>" +
                     "<td>"+land.ownerName+"</td>" +
                     "<td>"+land.ownerShipTransferStatus+"</td>" +
                     "<td>"+land.phoneNumber+"</td>" +
                     "<td>"+land.priceVariance+"</td>" +
                     "<td>"+land.purchasePriceVariance+"</td>" +
                     "<td>"+land.receiptVariance+"</td>" +
                     "<td>"+land.registrationDetails+"</td>" +
                     "<td>"+land.replacementDueDate+"</td>" +
                     "<td>"+land.requestTransferName+"</td>" +
                     "<td>"+land.requiredDate+"</td>" +
                     "<td>"+land.returnValue+"</td>" +
                     "<td>"+land.revisedRemainingLife+"</td>" +
                     "<td>"+land.scrapValue+"</td>" +
                     "<td>"+land.shrinkageAccount+"</td>" +
                     "<td>"+land.sinceDate+"</td>" +
                     "<td>"+land.status+"</td>" +
                     "<td>"+land.termsInMonths+"</td>" +
                     "<td>"+land.village+"</td>" +
                     "<td>"+land.ward+"</td>" +
                     "</tr>").appendTo("table"+tabelId);
             }
             var exportHref=Excel.tableToExcel(tabelId,'employeeExcel');
             $timeout(function(){location.href=exportHref;},100);
         }else{
             $("#emptyDataTable").modal("show");
             setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
         }
     }

     //import functionality start

     var importDoubleClick=false;
     $scope.importData=function(){
         if(!importDoubleClick){
             importDoubleClick=true;
             $http({
                 "method": "POST",
                 "url": 'api/AssetForLands/importAssetData',
                 "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                 "data": $scope.uploadFileURL
             }).success(function (response, data) {
                 //alert('succesfully updated');
                 $('#exportData').modal('hide');
                 importDoubleClick=false;
                 $scope.importedAssetData=response;
                 console.log('Imported Data Details>>>>>>'+JSON.stringify($scope.importedAssetData));
                 $scope.importedEmployee=true;

                 $scope.getLandDetails();

                 for(var x=0;x<$scope.importedAssetData.length;x++){
                     if($scope.importedAssetData[x].errorMessage=='success'){
                         console.log('errorMessage.....'+$scope.importedAssetData[x].errorMessage);
                     }else{
                         console.log('error message.....'+$scope.importedAssetData[x].errorMessage);
                         $scope.importError=true;
                         $scope.errorMessage = true;
                         $scope.errorMessageData = $scope.importedAssetData[x].errorMessage;
                         //$timeout(function(){ $scope.errorMessage=false; }, 3000);
                         $("#importError").modal("show");
                         setTimeout(function () {
                             $('#importError').modal('hide')
                         }, 3000);

                         console.log(JSON.stringify($scope.uploadFileURL));

                         $scope.uploadFileURL=[];
                         break;
                     }
                 }
                 
                 

             }).error(function (response, data) {
                 console.log("failure");
                 importDoubleClick=false;

             })
         }
     }



     var filedetails=[];
     var fileIdsArray=[];
     $scope.uploadFileURL = [];
     var fileUploadStatus=true;
     $scope.uploadFiles = function (files) {
         var count=1;
         filedetails=[];
         fileIdsArray=[];
         var fileCount = 0;
         //$scope.uploadFileURL = [];

         angular.forEach(files, function(file) {
             $scope.disable = true;
             $scope.errorMssg1 = true;
             fileCount++;/*
              if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {*/
             fileUploadStatus = false;
             file.upload = Upload.upload({
                 url:  'api/Uploads/dhanbadDb/upload',
                 data: {file: file}
             });

             file.upload.then(function (response) {
                 $timeout(function () {
                     var fileDetails = {
                         'id': response.data._id,
                         'name': response.data.filename
                     }
                     fileIdsArray.push(fileDetails);
                     $scope.uploadFileURL.push(fileDetails);
                     filedetails.push(response.data);
                     fileUploadStatus = true;
                     file.result = response.data;
                     console.log("file response" +JSON.stringify(file.result))
                 });
             }, function (response) {
                 if (response.status > 0)
                     $scope.errorMsg = response.status + ': ' + response.data;
             }, function (evt) {
                 file.progress = Math.min(100, parseInt(100.0 *
                     evt.loaded / evt.total));
             });
             /* }else{
              alert('Please Upload JPEG or PDF files only');
              }*/

             if (fileCount == files.length) {
                 $scope.uploadNitif = true;
                 $scope.disable = false;
                 $scope.errorMssg1 = false;
                 $scope.formNotSelected = '';
             }
         });
     };

     //remove files start

     $scope.deleteFiles = function(index, fileId, type){
         $http({
             method: 'DELETE',
             url: 'api/Uploads/dhanbadDb/files/'+fileId,
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             if(type == 'file') {
                 console.log('File');
                 $scope.uploadFileURL.splice(index, 1);
             }else{
                 console.log('Form');
                 $scope.uploadFormsNotif.splice(index, 1);
             }
         });
     };

     //remove files end

     //import functionality end

     $scope.getEmployees=function () {
         $http({
             method: 'GET',
             url: 'api/Employees?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             console.log('Employees Response :' + JSON.stringify(response));
             $scope.employeeList = response
             //$scope.createEmployee = false;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }

     $scope.getEmployees();

     $scope.getAssetName=function () {
         $http({
             method: 'GET',
             url: 'api/AssetTypes?filter={"where":{"status":"Active"}}',
             /*url: 'api/AssetTypes?filter={"where":{"status":"Immovable"}}',*/
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.loadingImage=false;
             console.log('Asset Name Response :' + JSON.stringify(response));
             $scope.assetNameList = response
             //$scope.createEmployee = false;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }

     $scope.getAssetName();


     $scope.getCostCenterData=function () {
         $http({
             method: 'GET',
             url: 'api/CostCenters?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('Cost Center  Response :' + JSON.stringify(response));
             $scope.costCenterList = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             if(response.error.details.messages.name) {
                 $scope.errorMessageData=response.error.details.messages.name[0];
                 $scope.errorMessage=true;
             }
         });
     }
     $scope.getCostCenterData();

     $scope.getAssetStatusData=function () {

         $http({
             method: 'GET',
             url: 'api/AssetStatuses?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('Users Response :' + JSON.stringify(response));
             $scope.assetStatusList = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             if(response.error.details.messages.name) {
                 $scope.errorMessageData=response.error.details.messages.name[0];
                 $scope.errorMessage=true;
             }

         });
     }
     $scope.getAssetStatusData();

     $scope.getDocumentListMasterData=function () {

         $http({
             method: 'GET',
             url: 'api/DocumentListMasters?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('Doc List Master Response :' + JSON.stringify(response));
             $scope.docTypeList = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             if(response.error.details.messages.name) {
                 $scope.errorMessageData=response.error.details.messages.name[0];
                 $scope.errorMessage=true;
             }

         });
     }

     $scope.getDocumentListMasterData();
     $scope.getMaintenanceObjectiveData=function () {

         $http({
             method: 'GET',
             url: 'api/MaintenanceObjectives?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('Maintanence Response :' + JSON.stringify(response));
             $scope.assetTypeList = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             if(response.error.details.messages.name) {
                 $scope.errorMessageData=response.error.details.messages.name[0];
                 $scope.errorMessage=true;
             }

         });
     }
     $scope.getMaintenanceObjectiveData();

     $scope.getAssetKPIData=function () {

         $http({
             method: 'GET',
             url: 'api/AssetKPIs?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('KPI Response :' + JSON.stringify(response));
             $scope.assetKPIList = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             if(response.error.details.messages.name) {
                 $scope.errorMessageData=response.error.details.messages.name[0];
                 $scope.errorMessage=true;
             }

         });
     }

     $scope.getAssetKPIData();

     $scope.getAssetSparesData=function () {

         $http({
             method: 'GET',
             url: 'api/AssetSpares?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('AssetSpares Response :' + JSON.stringify(response));
             $scope.assetSparesList = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             if(response.error.details.messages.name) {
                 $scope.errorMessageData=response.error.details.messages.name[0];
                 $scope.errorMessage=true;
             }

         });
     }
     $scope.getAssetSparesData();

     $scope.getAssetRiskCriticality=function () {

         $http({
             method: 'GET',
             url: 'api/AssetRiskCriticalities',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('Asset Criticality Response :' + JSON.stringify(response));
             $scope.assetCriticalityList = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             if(response.error.details.messages.name) {
                 $scope.errorMessageData=response.error.details.messages.name[0];
                 $scope.errorMessage=true;
             }

         });
     }
     $scope.getAssetRiskCriticality();


     $scope.getTypeOfDutyData=function () {

         $http({
             method: 'GET',
             url: 'api/TypeOfDuties?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('Type of Duty Response :' + JSON.stringify(response));
             $scope.dutyTypeList = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             if(response.error.details.messages.name) {
                 $scope.errorMessageData=response.error.details.messages.name[0];
                 $scope.errorMessage=true;
             }
         });
     }
     $scope.getTypeOfDutyData();

     $scope.dutyChange=function(dutyType){
        //alert(JSON.stringify(dutyType));

         $scope.dutyValue=JSON.parse(dutyType);
         
         $scope.editLandDetails.dutyType=$scope.dutyValue.name;
         $scope.editLandDetails.duty=$scope.dutyValue.duty;

         /* console.log("type::"+JSON.stringify(dutyType.dutyType));
         if(JSON.stringify(dutyType.dutyType)==undefined)
         {
             $scope.duty="";
         }
         $scope.dutyValue=JSON.parse(dutyType.dutyType);
         //$scope.dutyValue1=typeof(dutyType);
         console.log("duty type is:::" +JSON.stringify($scope.dutyValue.duty));
         $scope.duty=$scope.dutyValue.duty;*/
     }


     $scope.getFailureClassData=function () {
         $http({
             method: 'GET',
             url: 'api/FailureClasses?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('Failure Class Response :' + JSON.stringify(response));
             $scope.assetFailureList = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             if(response.error.details.messages.name) {
                 $scope.errorMessageData=response.error.details.messages.name[0];
                 $scope.errorMessage=true;
             }

         });
     }
     $scope.getFailureClassData();


     $scope.historicCostsAvailability = ["Yes", "No"];

     $scope.historicCostChanges = function (historicCostAvailability)
     {

         if(historicCostAvailability ==null || historicCostAvailability==undefined){
             $scope.historicCost=false;
             $scope.initialValuationCost=false;
         }
         else if(historicCostAvailability =="Yes") {
             delete $scope.land.initialValuationCost;
             $scope.historicCost=true;
             $scope.initialValuationCost=false;
         } else if(historicCostAvailability =="No"){
             delete $scope.land.historicCost;
             $scope.historicCost=false;
             $scope.initialValuationCost=true;
         }
         else {
             $scope.historicCost=false;
             $scope.initialValuationCost=false;
         }
     }

     $scope.assetRenewableAvailability = ["Yes", "No"];

     $scope.assetRenewableAvailabilityChanges = function (assetRenewableAvailability)
     {
         //$scope.assetRenewableDate=false;

         if(assetRenewableAvailability =="Yes")
         {
             $scope.assetRenewableDate=true;

         }
         else {
             $scope.assetRenewableDate=false;
             delete $scope.land.assetUpgradeDate;

         }

     }

     $scope.names = ["owned", "managed"];

     $scope.selectLandAssetType=function (assetMode) {

         if(assetMode=="owned")
         {
             $scope.ownedMode=true;
             $scope.managedMode=false;
             delete $scope.land.ownerName;
             delete $scope.land.phoneNumber;
             delete $scope.land.addressDetails;
             delete $scope.land.leasedDate;
             delete $scope.land.termsInMonths;
         }
         else if(assetMode="managed")
         {
             $scope.ownedMode=false;
             $scope.managedMode=true;
             delete $scope.land.transferDate;
             delete $scope.land.department;
             delete $scope.land.regdNo;

         }

     }


     $scope.statusChange=function (assetStatusList) {

         $scope.commissionedDate=false;
         $scope.disposalFiles=false;
         $scope.disposalStatus=false;
         $scope.disposalMethod=false;
         $scope.disposalAndDecommissionDate=false;


         if (assetStatusList=="disposal"){

             $scope.disposalFiles=true;
             $scope.disposalStatus=true;
             $scope.disposalMethod=true;
             $scope.disposalAndDecommissionDate=true;
         }

         else if (assetStatusList=="decommissioned"){

             $scope.disposalAndDecommissionDate=true;
             delete $scope.land.commissionDate;
             delete $scope.land.disposalStatus;
             delete $scope.land.disposalMethod;

         }

         else if(assetStatusList=="commissioned")
         {
             $scope.commissionedDate=true;
             delete $scope.land.decommissionDate;
             delete $scope.land.disposalStatus;
             delete $scope.land.disposalMethod;
         }

     }

     $scope.getLandDetails = function () {
         console.log('getLandDetails..'+$scope.showLand);
         $http({
             "method": "GET",
             //"url": 'api/AssetForLands?filter=%7B%22where%22%3A%7B%22assetType%22%3A%22Land%22%7D%7D',
             "url": 'api/AssetForLands?filter={"where":{"assetType":"Immovable"}}',
             "headers": {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('create Asset Get Response :' + JSON.stringify(response));
             $rootScope.landData = response;
             $scope.createLand = false;
         }).error(function (response, data) {
             console.log("failure");
         });
     };
     $scope.getLandDetails();

     $scope.showLand = true;
     $scope.createLand = false;
     $scope.editLand=false;
     $scope.showCreateLand = function() {
         $scope.land = {};
         $scope.upload={};
         $scope.loadingImage=false;
         $scope.createLand = true;
         $scope.showLand = false;
         $scope.files = [];
         $scope.fileUploadSuccess='';
         $scope.docList = [];
         formIdsArray = [];
     }

     $scope.clickToView=function () {
         $scope.loadingImage=false;
         $scope.showLand = true;
         $scope.createLand = false;
         $scope.editLand=false;
         $scope.files = [];
         $scope.fileUploadSuccess='';
         $scope.docList = [];
         formIdsArray = [];
     }


     $scope.reset = function () {
         $scope.land = angular.copy($scope.master);
     };

     $scope.land = {};
     $scope.upload={};

     var assetLandCreationForm=true;

     $scope.assetType="Immovable";

     $scope.docTypeSelect=function(documentType)

     {
         console.log("check doc type::" +JSON.stringify(documentType));
         $scope.documentSelectUpload=documentType;
         $scope.undefinedType=false;
         $('#add2').show();
         $('#add1').hide();
     }




     //*********************************File Upload for Create Asset************************************


     //******file Upload***********
     var formIdsArray = [];
     var formUploadStatus = false;
     $scope.disable = true;
     $scope.docList = [];
     $scope.uploadDocuments = function (files) {

         $('#add2').hide();
         $('#add1').show();
         formIdsArray = [];
         $scope.forms = files;
         var fileCount = 0;

         angular.forEach(files, function (file) {
             $scope.disable = true;
             $scope.errorMssg1 = true;
             fileCount++;
             if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                 formUploadStatus = false;
                 file.upload = Upload.upload({
                     url: 'api/Uploads/dhanbadDb/upload',
                     data: {file: file}
                 });

                 $scope.typeOfDocs = {"typeOfDocs": ''};
                 $scope.editUploadStatus=false;

                 file.upload.then(function (response) {
                     //$scope.docTypeSelect($scope.documentSelectUpload);

                     if($scope.documentSelectUpload!=undefined && $scope.documentSelectUpload!="" && $scope.documentSelectUpload!=null){
                         console.log("select doc upload" +$scope.documentSelectUpload)
                         $scope.undefinedType=false;

                         $timeout(function () {
                             $scope.typeOfDocs = response.data;
                             $scope.typeOfDocs.typeOfDocs = $scope.documentSelectUpload;
                             $scope.editUploadStatus=true;
                             console.log("upload doc response::" + JSON.stringify($scope.typeOfDocs));
                             $scope.editFileUploads=$scope.typeOfDocs;

                             formIdsArray.push(response.data);
                             $scope.docList.push(response.data);
                             formUploadStatus = true;
                             //alert('Entered')
                             //file['typeOfDoc']=response.data;
                             checkStatus();
                             file.result = response.data;
                         });
                     }else{
                         $scope.errorMssg1=false;
                         $scope.invalidFormat=false;
                         $scope.undefinedType=true;
                     }

                 }  , function (response) {
                     if (response.status > 0)
                         $scope.errorMsg = response.status + ': ' + response.data;
                 }, function (evt) {
                     file.progress = Math.min(100, parseInt(100.0 *
                         evt.loaded / evt.total));
                 });
             }else{
                 formUploadStatus = false;
                 $scope.successUpload=true;
                 checkStatus();
                 //alert('Please Upload JPEG or PDF files only');
                 $scope.undefinedType=false;
                 console.log("check" +$scope.invalidFormat);
             }

             function checkStatus(){
                 if (fileCount == files.length) {
                     $scope.disable = false;
                     $scope.errorMssg1 = false;
                     $scope.invalidFormat = false;
                     //alert(formUploadStatus);
                     if(formUploadStatus){

                         $timeout(function () {
                             $scope.fileUploadSuccess = false;
                         }, 3000);

                         $scope.fileUploadSuccess = true;
                         $scope.successUpload=true;
                     }else{
                         $scope.invalidFormat = true;
                         $scope.fileUploadSuccess = false;
                         $scope.successUpload=true;
                     }
                 }
             }

         });
     };
     //******file Upload***********

     $scope.deleteFile = function(index, fileId){

         $scope.editUploadStatus=false;

         if($scope.docList.length==1)
         {
             $scope.successUpload=false;
         }

         $scope.fileUploadSuccess=false;

         $http({
             method: 'DELETE',
             url: 'api/Uploads/dhanbadDb/files/'+fileId,
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             $scope.docList.splice(index, 1);
         });

     };

     $http({
         method: 'GET',
         url: 'api/AssetDepartments?filter={"where":{"status":"Active"}}',
         headers: {"Content-Type": "application/json", "Accept": "application/json"}
     }).success(function (response) {
         //console.log('Users Response :' + JSON.stringify(response));
         $scope.departmentList = response;
     }).error(function (response) {

     });

     $scope.getAccountDetails=function () {

         $http({
             method: 'GET',
             url: 'api/accountDetails?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('getAccountDetails :' + JSON.stringify(response));
             $scope.loadingImage=false;
             $scope.accountDetails = response;
         }).error(function (response) {
             /*if(response.error.details.messages.name) {
              $scope.errorMessageData=response.error.details.messages.name[0];
              $scope.errorMessage=true;
              }*/

         });
     }

     $scope.getAccountDetails();



     $scope.getCharterDetails=function () {
         $http({
             method: 'GET',
             url: 'api/ProjectCharters',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('charter Response :' + JSON.stringify(response));
             $scope.charterList=response;
             //    //alert('successfully created charter');
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
         ////alert('get charter list');
     }
     $scope.getCharterDetails();

     $scope.getULB=function () {
         $http({
             method: 'GET',
             url: 'api/ULBs',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             //console.log('Users Response :' + JSON.stringify(response));
             $scope.ulbList = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });
     }
     $scope.getULB();

     //popup details start

     function defaultActiveTab() {
         $('#tab1').addClass('active');
         $('#step1').addClass('active');
         $('#tab2').removeClass('active');
         $('#tab3').removeClass('active');
         $('#step2').removeClass('active');
         $('#complete').removeClass('active');
         document.getElementById('tab2').style.pointerEvents = 'none';
         document.getElementById('tab3').style.pointerEvents = 'none';
     }

     $(document).ready(function () {
         //Initialize tooltips
         $('.nav-tabs > li a[title]').tooltip();

         //Wizard
         $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

             var $target = $(e.target);

             if ($target.parent().hasClass('disabled')) {
                 return false;
             }
         });

         $(".next-step").click(function (e) {


             var $active = $('.wizard .nav-tabs li.active');
             $active.next().removeClass('disabled');
             nextTab($active);

         })

         document.getElementById('test').addEventListener('click', function() {

             var $active = $('.wizard .nav-tabs li.active');
             $active.next().removeClass('disabled');
             nextTab($active);
         }, false);

         document.getElementById('test').addEventListener('click', function() {
             var $active = $('.wizard .nav-tabs li.active');
             $active.next().removeClass('disabled');
             nextTab($active);

         }, false);

         $(".prev-step").click(function (e) {

             var $active = $('.wizard .nav-tabs li.active');
             prevTab($active);

         });
     });

     function nextTab(elem) {
         $(elem).next().find('a[data-toggle="tab"]').click();
     }
     function prevTab(elem) {
         $(elem).prev().find('a[data-toggle="tab"]').click();
     }

     //popup details end



     $scope.withOutAskDetails=function()
     {
         console.log("no button");

         console.log("land creation" +JSON.stringify($scope.land));
         var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
         var land=$scope.land;
         /*land['createdPerson']=loginPersonDetails.name;
          land['employeeId']=loginPersonDetails.employeeId;
          land['assetType']=$scope.assetType;
          land.file = $scope.docList;
          land['duty']=$scope.duty;*/
         if(assetLandCreationForm) {
             assetLandCreationForm = false;
             console.log('Land Response: :' + JSON.stringify(land));
             $http({
                 method: 'POST',
                 url: 'api/AssetForLands',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                 data: land
             }).success(function (response) {
                 console.log('Land Response: :' + JSON.stringify(response));
                 $("#submitClosureMessage").modal("hide");
                 $scope.docList = [];
                 $scope.getEmployees();
                 $scope.createLand = false;
                 $scope.showLand = true;

                 $scope.getLandDetails();
                 console.log("create asset details: " + JSON.stringify($scope.land));
                 $("#addEmployeeSuccess").modal("show");
                 setTimeout(function () {
                     $('#addEmployeeSuccess').modal('hide')
                 }, 3000);

                 $rootScope.landData.push(response);
                 console.log("$rootScope.landData*************" + JSON.stringify($rootScope.landData));

             }).error(function (response) {
                 //alert("progress3")
                 console.log('Error Response1 :' + JSON.stringify(response));

             });
         }

     };

     $scope.charter={};
     function doSomethingElse() {
         $('#tab3').addClass('active');
         $('#complete').addClass('active');
         $('#tab2').removeClass('active');
         $('#tab1').removeClass('active');
         $('#step1').removeClass('active');
         $('#step2').removeClass('active');
         $('.nav.nav-tabs li').removeClass("active");
         document.getElementById('tab2').style.pointerEvents = 'none';
         document.getElementById('tab1').style.pointerEvents = 'none';
     }

     $scope.errorMessage = false;
     $scope.errorMessageData = '';

     $scope.createProjectPlan=function()
     {

         if($scope.charter.projectCharter != '' && $scope.charter.projectCharter != undefined && $scope.charter.projectCharter != null &&  $scope.charter.projectCharter.length>0) {
             if($scope.charter.projectULB != '' && $scope.charter.projectULB != undefined && $scope.charter.projectULB != null) {
                 if($scope.charter.projectPlan != '' && $scope.charter.projectPlan != undefined && $scope.charter.projectPlan != null) {
                     if($scope.charter.departmentInfo != '' && $scope.charter.departmentInfo != undefined && $scope.charter.departmentInfo != null &&  $scope.charter.departmentInfo.length>0) {
                         if($scope.charter.financialYear != '' && $scope.charter.financialYear != undefined && $scope.charter.financialYear != null) {
                             doSomethingElse();
                         } else {
                             $scope.errorMessage = true;
                             $scope.errorMessageData = 'Please Select Financial Year';
                             $timeout(function(){ $scope.errorMessage=false; }, 3000);
                         }
                     } else {
                         $scope.errorMessage = true;
                         $scope.errorMessageData = 'Please Select A Department';
                         $timeout(function(){ $scope.errorMessage=false; }, 3000);
                     }
                 } else {
                     $scope.errorMessage = true;
                     $scope.errorMessageData = 'Please Enter Project Plan';
                     $timeout(function(){ $scope.errorMessage=false; }, 3000);
                 }
             } else {
                 $scope.errorMessage = true;
                 $scope.errorMessageData = 'Please Select Project ULB';
                 $timeout(function(){ $scope.errorMessage=false; }, 3000);
             }

         } else {
             $scope.errorMessage = true;
             $scope.errorMessageData = 'Please Select The Project Charter';
             $timeout(function(){ $scope.errorMessage=false; }, 3000);
         }
     }

     $scope.createProjectForAnAsset =function()
     {
         console.log("land creation");

         var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
         var land=$scope.land;
         land['projectDetails']=$scope.charter;
         if(assetLandCreationForm) {
             assetLandCreationForm = false;

             $http({
                 method: 'POST',
                 url: 'api/AssetForLands',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                 data: land
             }).success(function (response) {

                 $scope.landPostSuccess=response;
                 console.log('Land Response: :' + JSON.stringify(response));
                 //$('#askModel1').modal('hide');
                 $("#submitClosureMessage").modal("hide");
                 $scope.docList = [];
                 $scope.getEmployees();
                 $scope.createLand = false;
                 $scope.showLand = true;
                 $rootScope.landData.push(response);
                 console.log("$rootScope.landData*************" + JSON.stringify($rootScope.landData));
                 $scope.getLandDetails();
                 console.log("create asset details: " + JSON.stringify($scope.land));
                 $("#addEmployeeSuccess").modal("show");
                 setTimeout(function () {
                     $('#addEmployeeSuccess').modal('hide')
                 }, 3000);

                 //$scope.selectLandType=response.landType;
                 $scope.selectlandTypeId = response.id;
                 $window.localStorage.setItem('landCreationCheck', $scope.selectlandTypeId);
             }).error(function (response) {
                 console.log('Error Response1 :' + JSON.stringify(response));

             });
         }
     };

     $scope.createError = false;
     $scope.msgError = '';

     var validateDetails=function()
     {
         console.log("enterrrrrrrr")

         if($scope.land.assetName!=undefined && $scope.land.assetName!="" && $scope.land.assetName!=null) {
             if($scope.land.assetDescription!=undefined && $scope.land.assetDescription!="" && $scope.land.assetDescription!=null){
                 if($scope.land.accountNumber!=undefined && $scope.land.accountNumber!="" && $scope.land.accountNumber!=null){
                     if($scope.land.assetRenewableAvailability!=undefined && $scope.land.assetRenewableAvailability!="" && $scope.land.assetRenewableAvailability!=null){
                         if($scope.land.assetRenewableAvailability=="Yes"){
                             if($scope.land.assetUpgradeDate!=undefined && $scope.land.assetUpgradeDate!="" && $scope.land.assetUpgradeDate!=null){

                             } else{
                                 $scope.createError = true;
                                 $scope.msgError = 'Please Select Upgradable Date';
                                 return false;
                             }
                         }
                         //console.log("$scope.assetMode"+$scope.assetMode)
                         if($scope.land.assetMode!=undefined && $scope.land.assetMode!="" && $scope.land.assetMode!=null) {

                             if($scope.land.assetMode=="Owned"){
                                 if($scope.land.transferDate!=undefined && $scope.land.transferDate!="" && $scope.land.transferDate!=null){

                                     if($scope.land.department!=undefined && $scope.land.department!="" && $scope.land.department!=null){

                                         if($scope.land.regdNo!=undefined && $scope.land.regdNo!="" && $scope.land.regdNo!=null){

                                         }
                                         else{
                                             $scope.createError = true;
                                             $scope.msgError = 'Please Select regd no';
                                             return false;
                                         }
                                     }
                                     else{
                                         $scope.createError = true;
                                         $scope.msgError = 'Please Select Dept';
                                         return false;
                                     }
                                 }
                                 else{
                                     $scope.createError = true;
                                     $scope.msgError = 'Please Select Transfer Date';
                                     return false;
                                 }
                             }
                             if($scope.land.assetMode=="Managed"){
                                 if($scope.land.ownerName!=undefined && $scope.land.ownerName!="" && $scope.land.ownerName!=null){

                                     if($scope.land.phoneNumber!=undefined && $scope.land.phoneNumber!="" && $scope.land.phoneNumber!=null){

                                         if($scope.land.addressDetails!=undefined && $scope.land.addressDetails!="" && $scope.land.addressDetails!=null){

                                             if($scope.land.leasedDate!=undefined && $scope.land.leasedDate!="" && $scope.land.leasedDate!=null){

                                                 if($scope.land.termsInMonths!=undefined && $scope.land.termsInMonths!="" && $scope.land.termsInMonths!=null){

                                                 }
                                                 else{
                                                     $scope.createError = true;
                                                     $scope.msgError = 'Please Select Terms in Months';
                                                     return false;
                                                 }
                                             }
                                             else{
                                                 $scope.createError = true;
                                                 $scope.msgError = 'Please Select Leased Date';
                                                 return false;
                                             }
                                         }
                                         else{
                                             $scope.createError = true;
                                             $scope.msgError = 'Please Enter Address Details';
                                             return false;
                                         }
                                     }
                                     else{
                                         $scope.createError = true;
                                         $scope.msgError = 'Please Enter Mobile Number';
                                         return false;
                                     }
                                 }
                                 else{
                                     $scope.createError = true;
                                     $scope.msgError = 'Please Select Owner Name';
                                     return false;
                                 }
                             }
                             //                                                        return false;
                             if($scope.land.contactPersonDepartment!=undefined && $scope.land.contactPersonDepartment!="" && $scope.land.contactPersonDepartment!=null ) {
                                 if($scope.land.contactPerson!=undefined && $scope.land.contactPerson!="" && $scope.land.contactPerson!=null ) {
                                     if($scope.land.landDetails!=undefined && $scope.land.landDetails!="" && $scope.land.landDetails!=null ) {
                                         if($scope.land.areaInUnits!=undefined && $scope.land.areaInUnits!="" && $scope.land.areaInUnits!=null ) {
                                             if($scope.land.locality!=undefined && $scope.land.locality!="" && $scope.land.locality!=null ) {
                                                 if($scope.land.village!=undefined && $scope.land.village!="" && $scope.land.village!=null ) {
                                                     if($scope.land.longitude!=undefined && $scope.land.longitude!="" && $scope.land.longitude!=null ) {
                                                         if($scope.land.latitude!=undefined && $scope.land.latitude!="" && $scope.land.latitude!=null ) {
                                                             if($scope.land.ward!=undefined && $scope.land.ward!="" && $scope.land.ward!=null ) {
                                                                 if($scope.land.muncipalityOrMandal!=undefined && $scope.land.muncipalityOrMandal!="" && $scope.land.muncipalityOrMandal!=null ) {
                                                                     if($scope.land.registrationDetails!=undefined && $scope.land.registrationDetails!="" && $scope.land.registrationDetails!=null ) {
                                                                         if($scope.land.status!=undefined && $scope.land.status!="" && $scope.land.status!=null ) {

                                                                             if($scope.land.status=="commissioned"){
                                                                                 if($scope.land.commissionDate!=undefined && $scope.land.commissionDate!="" && $scope.land.commissionDate!=null){
                                                                                 }
                                                                                 else{
                                                                                     $scope.createError = true;
                                                                                     $scope.msgError = 'Please Select Commissioned Date';
                                                                                     return false;
                                                                                 }
                                                                             }
                                                                             if($scope.land.status=="decommissioned"){
                                                                                 if($scope.land.decommissionDate!=undefined && $scope.land.decommissionDate!="" && $scope.land.decommissionDate!=null){
                                                                                 }
                                                                                 else{
                                                                                     $scope.createError = true;
                                                                                     $scope.msgError = 'Please Select Decommissioned Date';
                                                                                     return false;
                                                                                 }
                                                                             }
                                                                             if($scope.land.status=="disposal"){
                                                                                 if($scope.land.disposalMethod!=undefined && $scope.land.disposalMethod!="" && $scope.land.disposalMethod!=null){
                                                                                     if($scope.land.deCommissionStatus!=undefined && $scope.land.deCommissionStatus!="" && $scope.land.deCommissionStatus!=null){
                                                                                         if($scope.land.decommissionDate!=undefined && $scope.land.decommissionDate!="" && $scope.land.decommissionDate!=null){
                                                                                         }
                                                                                         else{
                                                                                             $scope.createError = true;
                                                                                             $scope.msgError = 'Please Select Decommission Date';
                                                                                             return false;
                                                                                         }
                                                                                     }
                                                                                     else{
                                                                                         $scope.createError = true;
                                                                                         $scope.msgError = 'Please Select Decommission Status';
                                                                                         return false;
                                                                                     }
                                                                                 }
                                                                                 else{
                                                                                     $scope.createError = true;
                                                                                     $scope.msgError = 'Please Enter Disposal Method';
                                                                                     return false;
                                                                                 }
                                                                             }
                                                                             if($scope.land.dutyType!=undefined && $scope.land.dutyType!="" && $scope.land.dutyType!=null ) {
                                                                                 if($scope.land.maintenanceContactDepartment!=undefined && $scope.land.maintenanceContactDepartment!="" && $scope.land.maintenanceContactDepartment!=null ) {
                                                                                     if($scope.land.maintenanceContactName!=undefined && $scope.land.maintenanceContactName!="" && $scope.land.maintenanceContactName!=null ) {
                                                                                         if($scope.land.originalLife!=undefined && $scope.land.originalLife!="" && $scope.land.originalLife!=null ) {
                                                                                             if($scope.land.expectedDateOfDisposal!=undefined && $scope.land.expectedDateOfDisposal!="" && $scope.land.expectedDateOfDisposal!=null ) {
                                                                                                 if($scope.land.originalRemainingLife!=undefined && $scope.land.originalRemainingLife!="" && $scope.land.originalRemainingLife!=null ) {
                                                                                                     if($scope.land.revisedRemainingLife!=undefined && $scope.land.revisedRemainingLife!="" && $scope.land.revisedRemainingLife!=null ) {
                                                                                                         if($scope.land.replacementDueDate!=undefined && $scope.land.replacementDueDate!="" && $scope.land.replacementDueDate!=null ) {
                                                                                                             if($scope.land.assetLastReviewedDate!=undefined && $scope.land.assetLastReviewedDate!="" && $scope.land.assetLastReviewedDate!=null ) {
                                                                                                                 if($scope.land.impairmentLife!=undefined && $scope.land.impairmentLife!="" && $scope.land.impairmentLife!=null ) {
                                                                                                                     if($scope.land.costCenter!=undefined && $scope.land.costCenter!="" && $scope.land.costCenter!=null ) {
                                                                                                                         if($scope.land.historicCostAvailability!=undefined && $scope.land.historicCostAvailability!="" && $scope.land.historicCostAvailability!=null ) {

                                                                                                                             if($scope.land.historicCostAvailability=="Yes"){
                                                                                                                                 if($scope.land.historicCost!=undefined && $scope.land.historicCost!="" && $scope.land.historicCost!=null){
                                                                                                                                 }
                                                                                                                                 else{
                                                                                                                                     $scope.createError = true;
                                                                                                                                     $scope.msgError = 'Please Enter Historic Cost';
                                                                                                                                     return false;
                                                                                                                                 }
                                                                                                                             }
                                                                                                                             if($scope.land.historicCostAvailability=="No"){
                                                                                                                                 if($scope.land.initialValuationCost!=undefined && $scope.land.initialValuationCost!="" && $scope.land.initialValuationCost!=null){
                                                                                                                                 }
                                                                                                                                 else{
                                                                                                                                     $scope.createError = true;
                                                                                                                                     $scope.msgError = 'Please Enter Initial Valuation Cost';
                                                                                                                                     return false;
                                                                                                                                 }
                                                                                                                             }

                                                                                                                             if($scope.land.amortizationMethod!=undefined && $scope.land.amortizationMethod!="" && $scope.land.amortizationMethod!=null ) {
                                                                                                                                 if($scope.land.amortizationRate!=undefined && $scope.land.amortizationRate!="" && $scope.land.amortizationRate!=null ) {
                                                                                                                                     if($scope.land.amortizationAmount!=undefined && $scope.land.amortizationAmount!="" && $scope.land.amortizationAmount!=null ) {
                                                                                                                                         if($scope.land.bookValue!=undefined && $scope.land.bookValue!="" && $scope.land.bookValue!=null ) {
                                                                                                                                             if($scope.land.returnValue!=undefined && $scope.land.returnValue!="" && $scope.land.returnValue!=null ) {
                                                                                                                                                 if($scope.land.scrapValue!=undefined && $scope.land.scrapValue!="" && $scope.land.scrapValue!=null ) {
                                                                                                                                                     if($scope.land.controlAccount!=undefined && $scope.land.controlAccount!="" && $scope.land.controlAccount!=null ) {
                                                                                                                                                         if($scope.land.adjustmentAccount!=undefined && $scope.land.adjustmentAccount!="" && $scope.land.adjustmentAccount!=null ) {
                                                                                                                                                             if($scope.land.shrinkageAccount!=undefined && $scope.land.shrinkageAccount!="" && $scope.land.shrinkageAccount!=null ) {
                                                                                                                                                                 if($scope.land.priceVariance!=undefined && $scope.land.priceVariance!="" && $scope.land.priceVariance!=null ) {
                                                                                                                                                                     if($scope.land.currencyVariance!=undefined && $scope.land.currencyVariance!="" && $scope.land.currencyVariance!=null ) {
                                                                                                                                                                         if($scope.land.purchasePriceVariance!=undefined && $scope.land.purchasePriceVariance!="" && $scope.land.purchasePriceVariance!=null ) {
                                                                                                                                                                             if($scope.land.receiptVariance!=undefined && $scope.land.receiptVariance!="" && $scope.land.receiptVariance!=null ) {
                                                                                                                                                                                 if($scope.land.criticalityAsset!=undefined && $scope.land.criticalityAsset!="" && $scope.land.criticalityAsset!=null ) {
                                                                                                                                                                                     if($scope.land.sinceDate!=undefined && $scope.land.sinceDate!="" && $scope.land.sinceDate!=null ) {
                                                                                                                                                                                         if($scope.land.acquisitionStatus!=undefined && $scope.land.acquisitionStatus!="" && $scope.land.acquisitionStatus!=null ) {
                                                                                                                                                                                             if($scope.land.ownerShipTransferStatus!=undefined && $scope.land.ownerShipTransferStatus!="" && $scope.land.ownerShipTransferStatus!=null ) {
                                                                                                                                                                                                 if($scope.land.requestTransferName!=undefined && $scope.land.requestTransferName!="" && $scope.land.requestTransferName!=null ) {
                                                                                                                                                                                                     if($scope.land.requiredDate!=undefined && $scope.land.requiredDate!="" && $scope.land.requiredDate!=null ) {
                                                                                                                                                                                                         if($scope.land.legalFormalityStatus!=undefined && $scope.land.legalFormalityStatus!="" && $scope.land.legalFormalityStatus!=null ) {
                                                                                                                                                                                                             if($scope.land.assetSparesName!=undefined && $scope.land.assetSparesName!="" && $scope.land.assetSparesName!=null ) {
                                                                                                                                                                                                                 if($scope.land.assetSparesComment!=undefined && $scope.land.assetSparesComment!="" && $scope.land.assetSparesComment!=null ) {
                                                                                                                                                                                                                     if($scope.land.acquisitionComments!=undefined && $scope.land.acquisitionComments!="" && $scope.land.acquisitionComments!=null ) {
                                                                                                                                                                                                                         if($scope.documentSelectUpload!=undefined && $scope.documentSelectUpload!="" && $scope.documentSelectUpload!=null ) {
                                                                                                                                                                                                                             if($scope.docList!=undefined && $scope.docList!="" && $scope.docList!=null ){
                                                                                                                                                                                                                                 console.log("doc list is:" +JSON.stringify($scope.docList))
                                                                                                                                                                                                                                 return true;
                                                                                                                                                                                                                             }
                                                                                                                                                                                                                             else{
                                                                                                                                                                                                                                 $scope.createError = true;
                                                                                                                                                                                                                                 $scope.msgError = 'Please Attach The Files...';
                                                                                                                                                                                                                                 return false;
                                                                                                                                                                                                                             }
                                                                                                                                                                                                                         }
                                                                                                                                                                                                                         else{
                                                                                                                                                                                                                             $scope.createError = true;
                                                                                                                                                                                                                             $scope.msgError = 'Please Select Document Type...';
                                                                                                                                                                                                                             return false;
                                                                                                                                                                                                                         }

                                                                                                                                                                                                                     }
                                                                                                                                                                                                                     else{
                                                                                                                                                                                                                         $scope.createError = true;
                                                                                                                                                                                                                         $scope.msgError = 'Status/issues During Acquisition Comment...';
                                                                                                                                                                                                                         return false;
                                                                                                                                                                                                                     }
                                                                                                                                                                                                                 }
                                                                                                                                                                                                                 else{
                                                                                                                                                                                                                     $scope.createError = true;
                                                                                                                                                                                                                     $scope.msgError = 'Please Give A Comment For Asset Spares/Consumables ...';
                                                                                                                                                                                                                     return false;
                                                                                                                                                                                                                 }
                                                                                                                                                                                                             }
                                                                                                                                                                                                             else{
                                                                                                                                                                                                                 $scope.createError = true;
                                                                                                                                                                                                                 $scope.msgError = 'Please Select Asset Spares or Consumables ...';
                                                                                                                                                                                                                 return false;
                                                                                                                                                                                                             }
                                                                                                                                                                                                         }
                                                                                                                                                                                                         else{
                                                                                                                                                                                                             $scope.createError = true;
                                                                                                                                                                                                             $scope.msgError = 'Please Select Legal Formality Status ...';
                                                                                                                                                                                                             return false;
                                                                                                                                                                                                         }
                                                                                                                                                                                                     }
                                                                                                                                                                                                     else{
                                                                                                                                                                                                         $scope.createError = true;
                                                                                                                                                                                                         $scope.msgError = 'Please Select Required Date ...';
                                                                                                                                                                                                         return false;
                                                                                                                                                                                                     }
                                                                                                                                                                                                 }
                                                                                                                                                                                                 else{
                                                                                                                                                                                                     $scope.createError = true;
                                                                                                                                                                                                     $scope.msgError = 'Please Enter Request Transfer Name ...';
                                                                                                                                                                                                     return false;
                                                                                                                                                                                                 }
                                                                                                                                                                                             }
                                                                                                                                                                                             else{
                                                                                                                                                                                                 $scope.createError = true;
                                                                                                                                                                                                 $scope.msgError = 'Please Select Ownership Transfer Status ...';
                                                                                                                                                                                                 return false;
                                                                                                                                                                                             }
                                                                                                                                                                                         }
                                                                                                                                                                                         else{
                                                                                                                                                                                             $scope.createError = true;
                                                                                                                                                                                             $scope.msgError = 'Please Select Acquisition Notice Submission ...';
                                                                                                                                                                                             return false;
                                                                                                                                                                                         }
                                                                                                                                                                                     }
                                                                                                                                                                                     else{
                                                                                                                                                                                         $scope.createError = true;
                                                                                                                                                                                         $scope.msgError = 'Please Select Since Date ...';
                                                                                                                                                                                         return false;
                                                                                                                                                                                     }
                                                                                                                                                                                 }
                                                                                                                                                                                 else{
                                                                                                                                                                                     $scope.createError = true;
                                                                                                                                                                                     $scope.msgError = 'Please Select Criticality Of An Asset ...';
                                                                                                                                                                                     return false;
                                                                                                                                                                                 }
                                                                                                                                                                             }
                                                                                                                                                                             else{
                                                                                                                                                                                 $scope.createError = true;
                                                                                                                                                                                 $scope.msgError = 'Please Enter Receipt Variance ...';
                                                                                                                                                                                 return false;
                                                                                                                                                                             }
                                                                                                                                                                         }
                                                                                                                                                                         else{
                                                                                                                                                                             $scope.createError = true;
                                                                                                                                                                             $scope.msgError = 'Please Enter Purchase Price Variance ...';
                                                                                                                                                                             return false;
                                                                                                                                                                         }
                                                                                                                                                                     }
                                                                                                                                                                     else{
                                                                                                                                                                         $scope.createError = true;
                                                                                                                                                                         $scope.msgError = 'Please Enter Currency Variance ...';
                                                                                                                                                                         return false;
                                                                                                                                                                     }
                                                                                                                                                                 }
                                                                                                                                                                 else{
                                                                                                                                                                     $scope.createError = true;
                                                                                                                                                                     $scope.msgError = 'Please Enter Invoice Price Variance ...';
                                                                                                                                                                     return false;
                                                                                                                                                                 }
                                                                                                                                                             }
                                                                                                                                                             else{
                                                                                                                                                                 $scope.createError = true;
                                                                                                                                                                 $scope.msgError = 'Please Enter Shrinkage Account ...';
                                                                                                                                                                 return false;
                                                                                                                                                             }
                                                                                                                                                         }
                                                                                                                                                         else{
                                                                                                                                                             $scope.createError = true;
                                                                                                                                                             $scope.msgError = 'Please Enter Adjustment Account ...';
                                                                                                                                                             return false;
                                                                                                                                                         }
                                                                                                                                                     }
                                                                                                                                                     else{
                                                                                                                                                         $scope.createError = true;
                                                                                                                                                         $scope.msgError = 'Please Enter Control Account ...';
                                                                                                                                                         return false;
                                                                                                                                                     }
                                                                                                                                                 }
                                                                                                                                                 else{
                                                                                                                                                     $scope.createError = true;
                                                                                                                                                     $scope.msgError = 'Please Enter Scrap Value ...';
                                                                                                                                                     return false;
                                                                                                                                                 }
                                                                                                                                             }
                                                                                                                                             else{
                                                                                                                                                 $scope.createError = true;
                                                                                                                                                 $scope.msgError = 'Please Enter Return Value ...';
                                                                                                                                                 return false;
                                                                                                                                             }
                                                                                                                                         }
                                                                                                                                         else{
                                                                                                                                             $scope.createError = true;
                                                                                                                                             $scope.msgError = 'Please Enter Book Value Of An Asset ...';
                                                                                                                                             return false;
                                                                                                                                         }
                                                                                                                                     }
                                                                                                                                     else{
                                                                                                                                         $scope.createError = true;
                                                                                                                                         $scope.msgError = 'Please Enter Amortization Amount ...';
                                                                                                                                         return false;
                                                                                                                                     }
                                                                                                                                 }
                                                                                                                                 else{
                                                                                                                                     $scope.createError = true;
                                                                                                                                     $scope.msgError = 'Please Enter Amortization Rate ...';
                                                                                                                                     return false;
                                                                                                                                 }
                                                                                                                             }
                                                                                                                             else{
                                                                                                                                 $scope.createError = true;
                                                                                                                                 $scope.msgError = 'Please Enter Amortization Method ...';
                                                                                                                                 return false;
                                                                                                                             }
                                                                                                                         }
                                                                                                                         else{
                                                                                                                             $scope.createError = true;
                                                                                                                             $scope.msgError = 'Please Select Historic Cost Availability ...';
                                                                                                                             return false;
                                                                                                                         }
                                                                                                                     }
                                                                                                                     else{
                                                                                                                         $scope.createError = true;
                                                                                                                         $scope.msgError = 'Please Select The Cost Center ...';
                                                                                                                         return false;
                                                                                                                     }
                                                                                                                 }
                                                                                                                 else{
                                                                                                                     $scope.createError = true;
                                                                                                                     $scope.msgError = 'Please Fill The Evidence Of Impairment ...';
                                                                                                                     return false;
                                                                                                                 }
                                                                                                             }
                                                                                                             else{
                                                                                                                 $scope.createError = true;
                                                                                                                 $scope.msgError = 'Please Select Asset Last Review Date...';
                                                                                                                 return false;
                                                                                                             }
                                                                                                         }
                                                                                                         else{
                                                                                                             $scope.createError = true;
                                                                                                             $scope.msgError = 'Please Select Replacement Due Date...';
                                                                                                             return false;
                                                                                                         }
                                                                                                     }
                                                                                                     else{
                                                                                                         $scope.createError = true;
                                                                                                         $scope.msgError = 'Please Enter Revised Remaining Life...';
                                                                                                         return false;
                                                                                                     }
                                                                                                 }
                                                                                                 else{
                                                                                                     $scope.createError = true;
                                                                                                     $scope.msgError = 'Please Enter Original Remaining Life...';
                                                                                                     return false;
                                                                                                 }
                                                                                             }
                                                                                             else{
                                                                                                 $scope.createError = true;
                                                                                                 $scope.msgError = 'Please Select Expected Date Of Disposal...';
                                                                                                 return false;
                                                                                             }
                                                                                         }
                                                                                         else{
                                                                                             $scope.createError = true;
                                                                                             $scope.msgError = 'Please Enter Original Life...';
                                                                                             return false;
                                                                                         }
                                                                                     }
                                                                                     else{
                                                                                         $scope.createError = true;
                                                                                         $scope.msgError = 'Please Select Maintenance Contact Name...';
                                                                                         return false;
                                                                                     }
                                                                                 }
                                                                                 else{
                                                                                     $scope.createError = true;
                                                                                     $scope.msgError = 'Please Select Maintenance Contact Department...';
                                                                                     return false;
                                                                                 }
                                                                             }
                                                                             else{
                                                                                 $scope.createError = true;
                                                                                 $scope.msgError = 'Please Select Duty Type...';
                                                                                 return false;
                                                                             }
                                                                         }
                                                                         else{
                                                                             $scope.createError = true;
                                                                             $scope.msgError = 'Please Select Present Status/Usage...';
                                                                             return false;
                                                                         }
                                                                     }
                                                                     else{
                                                                         $scope.createError = true;
                                                                         $scope.msgError = 'Please Fill The Registration Details...';
                                                                         return false;
                                                                     }
                                                                 }
                                                                 else{
                                                                     $scope.createError = true;
                                                                     $scope.msgError = 'Please Enter Municipality Or Mandal...';
                                                                     return false;
                                                                 }
                                                             }
                                                             else{
                                                                 $scope.createError = true;
                                                                 $scope.msgError = 'Please Enter Ward Number...';
                                                                 return false;
                                                             }
                                                         }
                                                         else{
                                                             $scope.createError = true;
                                                             $scope.msgError = 'Please Enter Latitude...';
                                                             return false;
                                                         }
                                                     }
                                                     else{
                                                         $scope.createError = true;
                                                         $scope.msgError = 'Please Enter Longitude...';
                                                         return false;
                                                     }
                                                 }
                                                 else{
                                                     $scope.createError = true;
                                                     $scope.msgError = 'Please Enter Village...';
                                                     return false;
                                                 }
                                             }
                                             else{
                                                 $scope.createError = true;
                                                 $scope.msgError = 'Please Enter Location...';
                                                 return false;
                                             }
                                         }
                                         else{
                                             $scope.createError = true;
                                             $scope.msgError = 'Please Select Area In Units...';
                                             return false;
                                         }
                                     }
                                     else{
                                         $scope.createError = true;
                                         $scope.msgError = 'Please Select Land Details in Units...';
                                         return false;
                                     }
                                 }
                                 else{
                                     $scope.createError = true;
                                     $scope.msgError = 'Please Select Contact Person Name';
                                     return false;
                                 }
                             }
                             else{
                                 $scope.createError = true;
                                 $scope.msgError = 'Please Select Contact Person Dept';
                                 return false;
                             }
                         }

                         else {
                             $scope.createError = true;
                             $scope.msgError = 'Please Select Land is Owned/Managed';
                             return false;
                         }
                     }
                     else{
                         $scope.createError = true;
                         $scope.msgError = 'Please Select Asset Renewable Availability';
                         return false;
                     }
                 }
                 else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Account Number';
                     return false;
                 }
             }
             else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Asset Description';
                 return false;
             }
         }
         else{
             //alert("Please Select Asset Name")
             $scope.createError = true;
             $scope.msgError = 'Please Select Asset Name';
             return false;
         }
     }

     $scope.createLandButton = function () {

         var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
         var land=$scope.land;
         land['createdPerson']=loginPersonDetails.name;
         land['employeeId']=loginPersonDetails.employeeId;
         land['assetType']=$scope.assetType;
         land.file = $scope.docList;
         land['duty']=$scope.duty;

         $scope.createError = false;
         $scope.msgError = '';

         console.log("land creation");
         var validate=validateDetails();
         console.log("validate"+validate);
         if(validate){
             console.log("@@@@@@@@@@@@@@@@@@users response@@@@@@@@@@@@@@"+JSON.stringify($scope.land));
             var dataIs=JSON.parse($scope.land.dutyType);
             $scope.land['dutyType']=dataIs.name;
             if($scope.ownedMode==true)
             {
                 $scope.deptData = [];
                 $scope.deptData.push(JSON.parse($scope.land.department));
                 $scope.land.department=null;
                 $scope.land.department = $scope.deptData[0].id;
             }

             $("#submitClosureMessage").modal("show");

         }
         return true;

         console.log("@@@@@@@@@@@@@@@@@@users response@@@@@@@@@@@@@@"+JSON.stringify($scope.land));


     };

     //***********select*************


     $(function () {
         $("#scroll2").hide();
         $("#scroll").click(function () {
             $("#scroll2").show();
             $("#scroll").hide();
             $('html,body').animate({
                     scrollTop: $("#scroll2").offset().top
                 },
                 'slow');
         });
         $("#cancel").click(function () {
             $("#scroll2").hide()
             $("#scroll").show();
             ;
         });
     });




     //******************* File Upload For Create Asset *****************************************


     $scope.editLandDetails={};
     $scope.editLandData=function (landDetails) {

         $scope.loadingImage=false;
         $scope.editLandDetails=landDetails;
         $scope.editLand=true;
         $scope.showLand = false;
         //console.log(" editEmployeeDetails entered  " +JSON.stringify($scope.editLandDetails.file));

         $scope.editFileUpload=$scope.editLandDetails.file;
         console.log(" editEmployeeDetails entered  " +JSON.stringify( $scope.editFileUpload));
     }




     var addAssetTypeSubmit = true;

     $scope.editLandButton=function () {
         $scope.loadingImage = false;

         $scope.empUpdationError = false;
         $scope.updateEmpError = '';
         $scope.editFileUpload = [];
         if ($scope.editLandDetails['file'].length == 0) {
            $scope.editLandDetails['file'] = [];
            }
         var editLandDetails= $scope.editLandDetails;
         var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
         editLandDetails['lastEditPerson']=loginPersonDetails.name;
         editLandDetails['file'].push($scope.editFileUploads);
         //editLandDetails.noOfFiles=$scope.editFileUpload.push($scope.editFileUploads);

         if(addAssetTypeSubmit)
         {
             addAssetTypeSubmit = false;
             $http({
                 "method": "PUT",
                 "url": 'api/AssetForLands/'+$scope.editLandDetails.id,
                 "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                 "data": editLandDetails
             }).success(function (response, data) {
                 $scope.createLand = false;
                 $scope.editLand=false;
                 $scope.showLand = true;
                 $scope.editUploadStatus=false;
                 $scope.docList = [];
                 $scope.getLandDetails();
                 console.log("update land details: "+JSON.stringify($scope.editLandDetails) );
                 $("#editEmployeeSuccess").modal("show");
                 setTimeout(function(){$('#editEmployeeSuccess').modal('hide')}, 3000);
             }).error(function (response, data) {
                 console.log("failure");

                 if(response.error.details.messages.email) {
                     $scope.updateEmpError=response.error.details.messages.email[0];
                     $scope.updateEmpError = 'Email already Exist';
                     $scope.empUpdationError = true;
                 }
                 if(response.error.details.messages.employeeId) {
                     $scope.updateEmpError=response.error.details.messages.employeeId[0];
                     $scope.updateEmpError = 'EmployeeId already Exist';
                     $scope.empUpdationError = true;
                 }
                 $timeout(function(){
                     $scope.empUpdationError=false;
                 }, 3000);
             })
         }
         addAssetTypeSubmit = true;

     }

     $scope.resetField=function()
     {

         $scope.land={};
         $scope.fileUploadSuccess='';
         $scope.files = [];
         $scope.docList = [];
         $scope.invalidFormat=false;
         $scope.successUpload=false;
     }








 });

 app.controller('viewLandAssetController', function($http, $scope, $window, $location, $rootScope, $routeParams) {
      console.log("viewLandAssetController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetLand").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetLand .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

      $scope.loadingImage=true;
      $scope.pack=$routeParams.packId;
      //$scope.uploadURL='api/Uploads/dhanbadDb/download/';
      $scope.uploadURL=uploadFileURL;
      $http({
          "method": "GET",
          "url": 'api/AssetForLands/'+$scope.pack,
          //"url": 'api/AssetForLands?filter=%7B%22where%22%3A%7B%20%22assetType%22%3A%20%7B%22neq%22%3A%22Land%22%7D%7D%7D'+$scope.pack,
          "headers": {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {
          $scope.loadingImage=false;
          console.log('Users Response :' + JSON.stringify(response));
          $scope.scheme = response;
      }).error(function (response) {
          console.log('Error Response :' + JSON.stringify(response));
      });
      $scope.clickToBack = function(){
          $location.url("/createAssetLand");
          $scope.showLand = true;
      }


         /*$scope.generatePDF = function() {
         var element = document.getElementById("elementId");
         element.parentNode.removeChild(element);
         //alert(element)
         var printContents = document.getElementById('formConfirmation').innerHTML;
         var originalContents = document.body.innerHTML;

         document.body.innerHTML = printContents;

         window.print();

         document.body.innerHTML = originalContents;


        }*/

     $scope.generatePDF = function(formConfirmation) {
        /* var element = document.getElementById("elementId");
         element.parentNode.removeChild(element);*/
         /*var element = document.getElementById("elementId");
         element.parentNode.removeChild(element);*/
         var innerContents = document.getElementById(formConfirmation).innerHTML;
         var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
         popupWinindow.document.open();
         popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
         popupWinindow.document.close();

     }

  });

 app.controller('createAssetOthersController', function($scope, $rootScope, $window, $parse, $location, $http, Excel, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder) {

      console.log("createAssetOthersController");
       $scope.show=true;
       $scope.show1=true;
       $scope.show2=true;
       $scope.show3=true;
       $scope.show4=true;
       $scope.show5=true;
       $scope.show6=true;

      $(document).ready(function () {
          $('html,body').scrollTop(0);
          $(".sidebar-menu li ul > li").removeClass('active1');
          $('[data-toggle="tooltip"]').tooltip();
          $(".sidebar-menu #landAssetLi").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
          $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetOthers").addClass("active1").siblings('.active1').removeClass('active1');
          $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetOthers .collapse").addClass("in");
          $(".sidebar-menu li .collapse").removeClass("in");
      });

       //$scope.uploadFile='api/Uploads/dhanbadDb/download/';
       $scope.uploadFile=uploadFileURL;

       $(document).ready(function () {
           $('html,body').scrollTop(0);
           $(".navbar-nav li.landAndAsset").addClass("active").siblings('.active').removeClass('active');
           $(".navbar-nav li.landAndAsset li.assetType").addClass("active1").siblings('.active1').removeClass('active1');
           $(".navbar-nav li.schemes .collapse").removeClass("in");
           $(".navbar-nav li.landAndAsset .collapse").addClass("in");
           $(".navbar-nav li.configuration .collapse").removeClass("in");
           $(".navbar-nav li.administration .collapse").removeClass("in");

           $(".navbar-nav > li.projectWorks").addClass("active").siblings('.active').removeClass('active');
           $(".navbar-nav li.projectWorks li.masterData li.departmentList").addClass("active1").siblings('.active1').removeClass('active1');
           $(".navbar-nav li.projectWorks li.masterData .nav-list").addClass("active2").siblings('.active2').removeClass('active2');
           $(".navbar-nav li.projectWorks > .collapse").addClass("in");
           $(".navbar-nav li.masterData .collapse").addClass("in");
       });

       $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
           .withOption("sPaginationType", "bootstrap")
           .withOption("retrieve", true)
           .withOption('stateSave', true)
           .withPaginationType('simple_numbers')
           .withOption('order', [0, 'ASC']);

       $scope.exportToExcel=function(tabelId){
           if( $scope.landData!=null &&  $scope.landData.length>0){
               $("<tr>" +
                   "<th>Asset Id</th>" +
                   "<th>Asset Type</th>" +
                   "<th>Asset Description</th>" +
                   "<th>Asset Name</th>" +
                   "<th>Asset Renewable Availability</th>" +
                   "<th>Asset Mode</th>" +
                   "<th>Contact Person Department</th>" +
                   "<th>Contact Person</th>" +
                   "<th>Phone Number</th>" +
                   "<th>Leased Date</th>" +
                   "<th>Terms In Months</th>" +
                   "<th>Village</th>" +
                   "<th>Status</th>" +
                   "<th>Duty Type</th>" +
                   "<th>Duty</th>" +
                   "<th>Manufacturing Serial Number</th>" +
                   "<th>Manufacturing Code</th>" +
                   "<th>Manufacturing Name</th>" +
                   "<th>Supplier Number</th>" +
                   "<th>Supplier Code</th>" +
                   "<th>Supplier Name</th>" +
                   "<th>Purchase Date</th>" +
                   "<th>Warrenty Period</th>" +
                   "<th>Expiry Date</th>" +
                   "<th>Maintenance Contact Name</th>" +
                   "<th>Original Life</th>" +
                   "<th>Expected Date Of Disposal</th>" +
                   "<th>Original Remaining Life</th>" +
                   "<th>Revised Remaining Life</th>" +
                   "<th>Replacement Due Date</th>" +
                   "<th>Asset Last Reviewed Date</th>" +
                   "<th>Impairment Life</th>" +
                   "<th>Cost Center</th>" +
                   "<th>Historic Cost Availability</th>" +
                   "<th>Historic Cost</th>" +
                   "<th>Amortization Method</th>" +
                   "<th>Amortization Rate</th>" +
                   "<th>Amortization Amount</th>" +
                   "<th>Book Value</th>" +
                   "<th>Return Value</th>" +
                   "<th>Scrap Value</th>" +
                   "<th>Control Account</th>" +
                   "<th>Adjustment Account</th>" +
                   "<th>Shrinkage Account</th>" +
                   "<th>Price Variance</th>" +
                   "<th>Currency Variance</th>" +
                   "<th>Purchase Price Variance</th>" +
                   "<th>Receipt Variance</th>" +
                   "<th>Criticality Asset</th>" +
                   "<th>Since Date</th>" +
                   "<th>Acquisition Status</th>" +
                   "<th>Ownership Transfer Status</th>" +
                   "<th>Request Transfer Name</th>" +
                   "<th>Required Date</th>" +
                   "<th>Legal Formality Status</th>" +
                   "<th>Asset Spares Name</th>" +
                   "<th>Asset Spares Comment</th>" +
                   "<th>Acquisition Comments</th>" +
                   "<th>Created Person</th>" +
                   "<th>Employee ID</th>" +
                   "</tr>").appendTo("table"+tabelId);
               for(var i=0;i<$scope.landData.length;i++){
                   var land=$scope.landData[i];
                   $("<tr>" +
                       "<td>"+land.assetNumber+"</td>" +
                       "<td>"+land.assetType+"</td>" +
                       "<td>"+land.assetDescription+"</td>" +
                       "<td>"+land.assetName+"</td>" +
                       "<td>"+land.assetRenewableAvailability+"</td>" +
                       "<td>"+land.assetMode+"</td>" +
                       "<td>"+land.contactPersonDepartment+"</td>" +
                       "<td>"+land.contactPerson+"</td>" +
                       "<td>"+land.phoneNumber+"</td>" +
                       "<td>"+land.leasedDate+"</td>" +
                       "<td>"+land.termsInMonths+"</td>" +
                       "<td>"+land.village+"</td>" +
                       "<td>"+land.status+"</td>" +
                       "<td>"+land.dutyType+"</td>" +
                       "<td>"+land.duty+"</td>" +
                       "<td>"+land.manufacturingSNo+"</td>" +
                       "<td>"+land.manufacturingCode+"</td>" +
                       "<td>"+land.manufacturingName+"</td>" +
                       "<td>"+land.supplierNo+"</td>" +
                       "<td>"+land.supplierCode+"</td>" +
                       "<td>"+land.supplierName+"</td>" +
                       "<td>"+land.purchaseDate+"</td>" +
                       "<td>"+land.warrentyPeriod+"</td>" +
                       "<td>"+land.expiryDate+"</td>" +
                       "<td>"+land.maintenanceContactDepartment+"</td>" +
                       "<td>"+land.maintenanceContactName+"</td>" +
                       "<td>"+land.originalLife+"</td>" +
                       "<td>"+land.expectedDateOfDisposal+"</td>" +
                       "<td>"+land.originalRemainingLife+"</td>" +
                       "<td>"+land.revisedRemainingLife+"</td>" +
                       "<td>"+land.replacementDueDate+"</td>" +
                       "<td>"+land.assetLastReviewedDate+"</td>" +
                       "<td>"+land.impairmentLife+"</td>" +
                       "<td>"+land.costCenter+"</td>" +
                       "<td>"+land.historicCostAvailability+"</td>" +
                       "<td>"+land.historicCost+"</td>" +
                       "<td>"+land.amortizationMethod+"</td>" +
                       "<td>"+land.amortizationRate+"</td>" +
                       "<td>"+land.amortizationAmount+"</td>" +
                       "<td>"+land.bookValue+"</td>" +
                       "<td>"+land.returnValue+"</td>" +
                       "<td>"+land.scrapValue+"</td>" +
                       "<td>"+land.controlAccount+"</td>" +
                       "<td>"+land.adjustmentAccount+"</td>" +
                       "<td>"+land.shrinkageAccount+"</td>" +
                       "<td>"+land.priceVariance+"</td>" +
                       "<td>"+land.currencyVariance+"</td>" +
                       "<td>"+land.purchasePriceVariance+"</td>" +
                       "<td>"+land.receiptVariance+"</td>" +
                       "<td>"+land.criticalityAsset+"</td>" +
                       "<td>"+land.sinceDate+"</td>" +
                       "<td>"+land.acquisitionStatus+"</td>" +
                       "<td>"+land.ownerShipTransferStatus+"</td>" +
                       "<td>"+land.requestTransferName+"</td>" +
                       "<td>"+land.requiredDate+"</td>" +
                       "<td>"+land.legalFormalityStatus+"</td>" +
                       "<td>"+land.assetSparesName+"</td>" +
                       "<td>"+land.assetSparesComment+"</td>" +
                       "<td>"+land.acquisitionComments+"</td>" +
                       "<td>"+land.createdPerson+"</td>" +
                       "<td>"+land.employeeId+"</td>" +
                       "</tr>").appendTo("table"+tabelId);
               }
               var exportHref=Excel.tableToExcel(tabelId,'createAssetOthers');
               $timeout(function(){location.href=exportHref;},100);
           }else{
               $("#emptyDataTable").modal("show");
               setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
           }
       }

       $scope.getAssetServiceLevelData=function () {

             $http({
                 method: 'GET',
                 url: 'api/AssetServiceLevels?filter={"where":{"status":"Active"}}',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 console.log('Users Response :' + JSON.stringify(response));
                 $scope.assetServiceLevelList = response;
                 $scope.loadingImage=false;
             }).error(function (response) {
                 if(response.error.details.messages.description) {
                     $scope.errorMessageData=response.error.details.messages.description[0];
                     $scope.errorMessage=true;
                 }

             });
       }
       $scope.getAssetServiceLevelData();

       $scope.getEmployees=function () {
                $http({
                    method: 'GET',
                    url: 'api/Employees?filter={"where":{"status":"Active"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.loadingImage=false;
                    console.log('Employees Response :' + JSON.stringify(response));

                    $scope.employeeList = response
                    //$scope.createEmployee = false;
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                });
            }

            $scope.getEmployees();

             $scope.getAssetName=function () {
                  $http({
                      method: 'GET',
                      url: 'api/AssetTypes?filter={"where":{"status":"Movable"}}',
                      headers: {"Content-Type": "application/json", "Accept": "application/json"}
                  }).success(function (response) {
                      $scope.loadingImage=false;
                      console.log('Asset Name Response :' + JSON.stringify(response));
                      $scope.assetNameList = response
                      //$scope.createEmployee = false;
                  }).error(function (response) {
                      console.log('Error Response :' + JSON.stringify(response));
                  });
              }

            $scope.getAssetName();

            $scope.getCostCenterData=function () {

                $http({
                    method: 'GET',
                    url: 'api/CostCenters?filter={"where":{"status":"Active"}}',
                    /*url: 'http://localhost:8866/api/CostCenters',*/
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.costCenterList = response;
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }
            $scope.getCostCenterData();

            $scope.getAssetStatusData=function () {

                $http({
                    method: 'GET',
                    url: 'api/AssetStatuses?filter={"where":{"status":"Active"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('Users Response :' + JSON.stringify(response));
                    $scope.assetStatusList = response;
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }
            $scope.getAssetStatusData();

            $scope.getDocumentListMasterData=function () {

                $http({
                    method: 'GET',
                    url: 'api/DocumentListMasters?filter={"where":{"status":"Active"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('Doc List Master Response :' + JSON.stringify(response));
                    $scope.docTypeList = response;
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }

            $scope.getDocumentListMasterData();


            $scope.getMaintenanceObjectiveData=function () {

                $http({
                    method: 'GET',
                    url: 'api/MaintenanceObjectives?filter={"where":{"status":"Active"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('Maintanence Response :' + JSON.stringify(response));
                    $scope.assetTypeList = response;
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }
            $scope.getMaintenanceObjectiveData();

            $scope.getAssetKPIData=function () {

                $http({
                    method: 'GET',
                    url: 'api/AssetKPIs?filter={"where":{"status":"Active"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('KPI Response :' + JSON.stringify(response));
                    $scope.assetKPIList = response;
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }

            $scope.getAssetKPIData();

            $scope.getAssetSparesData=function () {

                $http({
                    method: 'GET',
                    url: 'api/AssetSpares?filter={"where":{"status":"Active"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('AssetSpares Response :' + JSON.stringify(response));
                    $scope.assetSparesList = response;
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }
            $scope.getAssetSparesData();

            $scope.getAssetRiskCriticality=function () {

                $http({
                    method: 'GET',
                    url: 'api/AssetRiskCriticalities?filter={"where":{"status":"Active"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('Asset Criticality Response :' + JSON.stringify(response));
                    $scope.assetCriticalityList = response;
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }
            $scope.getAssetRiskCriticality();


            $scope.getTypeOfDutyData=function () {

                $http({
                    method: 'GET',
                    url: 'api/TypeOfDuties?filter={"where":{"status":"Active"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('Type of Duty Response :' + JSON.stringify(response));
                    $scope.dutyTypeList = response;

                    //console.log("duty list:::" +JSON.stringify(response.duty));
                    //$window.localStorage.setItem('dutyValue', $scope.selectlandTypeId);
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }
            $scope.getTypeOfDutyData();

            //$scope.dutyValue={};

          $scope.dutyChange=function(dutyType)
              {
              console.log("type::"+JSON.stringify(dutyType.dutyType));
              if(JSON.stringify(dutyType.dutyType)==undefined)
               {
                $scope.duty="";
               }
                 $scope.dutyValue=JSON.parse(dutyType.dutyType);
                 //$scope.dutyValue1=typeof(dutyType);
                 console.log("duty type is:::" +JSON.stringify($scope.dutyValue.duty));
                 $scope.duty=$scope.dutyValue.duty;
                 //console.log("duty type is:::" +$scope.dutyValue1);
              }

            $scope.getFailureClassData=function () {

                $http({
                    method: 'GET',
                    url: 'api/FailureClasses?filter={"where":{"status":"Active"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('Failure Class Response :' + JSON.stringify(response));
                    $scope.assetFailureList = response;
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }
            $scope.getFailureClassData();


            $scope.getAssetStatusData=function () {

                $http({
                    method: 'GET',
                    url: 'api/AssetStatuses?filter={"where":{"status":"Active"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('Users Response :' + JSON.stringify(response));
                    $scope.assetStatusList = response;
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }
            $scope.getAssetStatusData();

               $scope.historicCostsAvailability = ["Yes", "No"];

               $scope.historicCostChanges = function (historicCostAvailability)
               {

                   if(historicCostAvailability ==null || historicCostAvailability==undefined){
                   $scope.historicCost=false;
                   $scope.initialValuationCost=false;
                   }
                   else if(historicCostAvailability =="Yes") {
                   delete $scope.land.initialValuationCost;
                   $scope.historicCost=true;
                   $scope.initialValuationCost=false;
                   } else if(historicCostAvailability =="No"){
                   delete $scope.land.historicCost;
                   $scope.historicCost=false;
                   $scope.initialValuationCost=true;
                   }
                   else {
                   $scope.historicCost=false;
                   $scope.initialValuationCost=false;
                   }
               }

           $scope.assetRenewableAvailability = ["Yes", "No"];

           $scope.assetRenewableAvailabilityChanges = function (assetRenewableAvailability)
            {
               //$scope.assetRenewableDate=false;

               if(assetRenewableAvailability =="Yes")
               {
                   $scope.assetRenewableDate=true;
               }
               else {
                   $scope.assetRenewableDate=false;
                   delete $scope.land.assetUpgradeDate;
               }
            }

            $scope.names = ["Owned", "Managed"];

            $scope.selectLandAssetType=function (assetMode) {

                           if(assetMode=="Owned")
                           {
                               $scope.ownedMode=true;
                               $scope.managedMode=false;
                               delete $scope.land.ownerName;
                               delete $scope.land.phoneNumber;
                               delete $scope.land.addressDetails;
                               delete $scope.land.leasedDate;
                               delete $scope.land.termsInMonths;
                           }
                           else if(assetMode="Managed")
                           {
                               $scope.ownedMode=false;
                               $scope.managedMode=true;
                               delete $scope.land.transferDate;
                               delete $scope.land.department;
                               delete $scope.land.regdNo;
                           }
                       }


            $scope.statusChange=function (assetStatusList) {

                $scope.commissionedDate=false;
                $scope.disposalFiles=false;
                $scope.disposalStatus=false;
                $scope.disposalMethod=false;
                $scope.disposalAndDecommissionDate=false;


                if (assetStatusList=="disposal"){

                    $scope.disposalFiles=true;
                    $scope.disposalStatus=true;
                    $scope.disposalMethod=true;
                    $scope.disposalAndDecommissionDate=true;
                }

                else if (assetStatusList=="decommissioned"){

                    $scope.disposalAndDecommissionDate=true;
                    delete $scope.land.commissionDate;
                    delete $scope.land.disposalStatus;
                    delete $scope.land.disposalMethod;

                }

                else if(assetStatusList=="commissioned")
                {
                    $scope.commissionedDate=true;
                    delete $scope.land.decommissionDate;
                    delete $scope.land.disposalStatus;
                    delete $scope.land.disposalMethod;
                }

            }

            /*$scope.getAssetTypeData=function () {

                $http({
                    method: 'GET',
                    //url: 'api/AssetTypes?filter=%7B%22where%22%3A%7B%22name%22%3A%20%7B%22neq%22%3A%22Land%22%7D%7D%7D',
                    url: 'api/AssetTypes?filter={"where":{"and":[{"name": {"neq":"land"}},{"status": "Active"}]}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('Users Response :' + JSON.stringify(response));
                    $scope.assetTypeList = response;
                    $scope.loadingImage=false;
                }).error(function (response) {
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }

                });
            }

            $scope.getAssetTypeData();*/

            $scope.getLandDetails = function () {
                $http({
                    "method": "GET",
                   // "url": 'api/AssetForLands?filter=%7B%22where%22%3A%7B%20%22assetType%22%3A%20%7B%22neq%22%3A%22land%22%7D%7D%7D',
                    //"url": 'api/AssetForLands?filter={"where":{"and":[{"assetType": {"neq":"Land"}},{"status": "Active"}]}}',
                     "url": 'api/AssetForLands?filter={"where":{"assetType":"Movable"}}',
                     "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('create Asset Get Response :' + JSON.stringify(response));
                    $rootScope.landData = response;
                    $scope.createLand = false;


                }).error(function (response, data) {
                    console.log("failure");
                });
            };
            $scope.getLandDetails();

            $scope.showLand = true;
            $scope.createLand = false;
            $scope.editLand=false;
            $scope.showCreateLand = function() {
                $scope.land = {};
                $scope.upload={};
                $scope.loadingImage=false;
                $scope.createLand = true;
                $scope.showLand = false;
                $scope.fileUploadSuccess='';
                $scope.files = [];
                $scope.docList = [];
                formIdsArray = [];
            }

            $scope.clickToView=function () {
                $scope.loadingImage=false;
                $scope.showLand = true;
                $scope.createLand = false;
                $scope.editLand=false;
                $scope.fileUploadSuccess='';
                $scope.files = [];
                $scope.docList = [];
                $scope.invalidFormat=false;
                $scope.successUpload=false;
                formIdsArray = [];
            }


            $scope.reset = function () {
                $scope.land = angular.copy($scope.master);
            };

            $scope.land = {};
            $scope.upload={};

            var assetLandCreationForm=true;

           // $scope.assetType="land";
             $scope.assetType="Movable";

            $scope.docTypeSelect=function(documentType)

            {
                console.log("check doc type::" +JSON.stringify(documentType));
                $scope.documentSelectUpload=documentType;
                $scope.undefinedType=false;
                $('#add2').show();
                $('#add1').hide();
            }

            //*********************************File Upload for Create Asset************************************


            //******file Upload***********
            var formIdsArray = [];
            var formUploadStatus = false;
            $scope.disable = true;
            $scope.docList = [];
            $scope.uploadDocuments = function (files) {

                           $('#add2').hide();
                           $('#add1').show();
                           formIdsArray = [];
                           $scope.forms = files;
                           var fileCount = 0;

                           angular.forEach(files, function (file) {
                               $scope.disable = true;
                               $scope.errorMssg1 = true;
                               fileCount++;
                               if(file.type == "application/pdf" || file.type == "image/jpeg" || file.type == "image/png") {
                                   formUploadStatus = false;
                                   file.upload = Upload.upload({
                                       url: 'api/Uploads/dhanbadDb/upload',
                                       data: {file: file}
                                   });

                                   $scope.typeOfDocs = {"typeOfDocs": ''};
                                   $scope.editUploadStatus=false;

                                   file.upload.then(function (response) {
                                       $timeout(function () {
                                           $scope.typeOfDocs = response.data;
                                           $scope.typeOfDocs.typeOfDocs = $scope.documentSelectUpload;

                                           $scope.editUploadStatus=true;
                                           console.log("upload doc response::" + JSON.stringify($scope.typeOfDocs));
                                           $scope.editFileUploads=$scope.typeOfDocs;
                                           formIdsArray.push(response.data);
                                           $scope.docList.push(response.data);
                                           formUploadStatus = true;
                                           //alert('Entered')
                                           //file['typeOfDoc']=response.data;
                                            checkStatus();
                                           file.result = response.data;
                                       });
                                   }, function (response) {
                                       if (response.status > 0)
                                           $scope.errorMsg = response.status + ': ' + response.data;
                                   }, function (evt) {
                                       file.progress = Math.min(100, parseInt(100.0 *
                                           evt.loaded / evt.total));
                                   });
                               }else{
                                    formUploadStatus = false;
                                     $scope.successUpload=true;
                                    checkStatus();
                                   //alert('Please Upload JPEG or PDF files only');
                                   console.log("check" +$scope.invalidFormat);
                               }

                                function checkStatus(){
                                   if (fileCount == files.length) {
                                       $scope.disable = false;
                                       $scope.errorMssg1 = false;
                                        $scope.invalidFormat = false;
                                       // $scope.successUpload=false;
                                       //alert(formUploadStatus);
                                       if(formUploadStatus){
                                       $timeout(function () {
                                       $scope.fileUploadSuccess = false;
                                       }, 3000);
                                          $scope.fileUploadSuccess = true;
                                            $scope.successUpload=true;
                                       }else{
                                           $scope.invalidFormat = true;
                                           $scope.fileUploadSuccess = false;
                                             $scope.successUpload=true;
                                       }
                                   }
                               }

                           });
                       };
       //******file Upload***********

            $scope.deleteFile = function(index, fileId){

                $scope.editUploadStatus=false;

                if($scope.docList.length==1)
                {
                    $scope.successUpload=false;
                }

                $scope.fileUploadSuccess=false;

                $http({
                    method: 'DELETE',
                    url: 'api/Uploads/dhanbadDb/files/'+fileId,
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.docList.splice(index, 1);
                });

            };

             $http({
             method: 'GET',
             url: 'api/AssetDepartments?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 //console.log('Users Response :' + JSON.stringify(response));
                 $scope.departmentList = response;
             }).error(function (response) {

             });

             $scope.getAccountDetails=function () {

                      $http({
                          method: 'GET',
                          url: 'api/accountDetails?filter={"where":{"status":"Active"}}',
                          headers: {"Content-Type": "application/json", "Accept": "application/json"}
                      }).success(function (response) {
                          console.log('getAccountDetails :' + JSON.stringify(response));
                          $scope.loadingImage=false;
                          $scope.accountDetails = response;
                      }).error(function (response) {
                          /*if(response.error.details.messages.name) {
                              $scope.errorMessageData=response.error.details.messages.name[0];
                              $scope.errorMessage=true;
                          }*/

                      });
                  }

                  $scope.getAccountDetails();



            $scope.getCharterDetails=function () {
                    $http({
                        method: 'GET',
                        url: 'api/ProjectCharters',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"}
                    }).success(function (response) {
                       console.log('charter Response :' + JSON.stringify(response));
                        $scope.charterList=response;
                        //    //alert('successfully created charter');
                    }).error(function (response) {
                        console.log('Error Response :' + JSON.stringify(response));
                    });
                    ////alert('get charter list');
                }
            $scope.getCharterDetails();

            $scope.getULB=function () {
                    $http({
                        method: 'GET',
                        url: 'api/ULBs',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"}
                    }).success(function (response) {
                        //console.log('Users Response :' + JSON.stringify(response));
                        $scope.ulbList = response;
                        $scope.loadingImage=false;
                    }).error(function (response) {
                        console.log('Error Response :' + JSON.stringify(response));
                    });
                }
            $scope.getULB();

        //popup details start

        function defaultActiveTab() {
                $('#tab1').addClass('active');
                $('#step1').addClass('active');
                $('#tab2').removeClass('active');
                $('#tab3').removeClass('active');
                $('#step2').removeClass('active');
                $('#complete').removeClass('active');
                document.getElementById('tab2').style.pointerEvents = 'none';
                document.getElementById('tab3').style.pointerEvents = 'none';
        }

        $(document).ready(function () {
                //Initialize tooltips
                $('.nav-tabs > li a[title]').tooltip();

                //Wizard
                $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

                    var $target = $(e.target);

                    if ($target.parent().hasClass('disabled')) {
                        return false;
                    }
                });

                $(".next-step").click(function (e) {


                    var $active = $('.wizard .nav-tabs li.active');
                    $active.next().removeClass('disabled');
                    nextTab($active);

                })

                document.getElementById('test').addEventListener('click', function() {

                    var $active = $('.wizard .nav-tabs li.active');
                    $active.next().removeClass('disabled');
                    nextTab($active);
                }, false);

                document.getElementById('test').addEventListener('click', function() {
                    var $active = $('.wizard .nav-tabs li.active');
                    $active.next().removeClass('disabled');
                    nextTab($active);

                }, false);

                $(".prev-step").click(function (e) {

                    var $active = $('.wizard .nav-tabs li.active');
                    prevTab($active);

                });
        });

        function nextTab(elem) {
            $(elem).next().find('a[data-toggle="tab"]').click();
        }
        function prevTab(elem) {
            $(elem).prev().find('a[data-toggle="tab"]').click();
        }

        //popup details end

        /*$scope.askDetails=function()

                  {

                   console.log("yes button");
                    $('#askModal').modal('hide');
                    $('#askModel1').modal('show');
                  }*/


          $scope.withOutAskDetails=function()
          {
           console.log("no button");
           console.log("asset creation" +JSON.stringify($scope.land));
            /*if($scope.ownedMode==true)
            {
              $scope.deptData = [];
              $scope.deptData.push(JSON.parse($scope.land.department));
              $scope.land.department=null;
              $scope.land.department = $scope.deptData[0].id;

            }*/

           var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
           var land=$scope.land;
           /*land['createdPerson']=loginPersonDetails.name;
           land['employeeId']=loginPersonDetails.employeeId;
           land['assetType']=$scope.assetType;
           land.file = $scope.docList;
           land['duty']=$scope.duty;*/
           if(assetLandCreationForm) {
               assetLandCreationForm = false;
               console.log('Land Input: :' + JSON.stringify(land));
               $http({
                   method: 'POST',
                   url: 'api/AssetForLands',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"},
                   data: land
               }).success(function (response) {
                   console.log('Land Response: :' + JSON.stringify(response));
                   $scope.docList = [];
                   $scope.getEmployees();
                   $scope.createLand = false;
                   $scope.showLand = true;

                   $scope.getLandDetails();
                   console.log("create asset details: " + JSON.stringify($scope.land));
                   $("#addEmployeeSuccess").modal("show");
                   setTimeout(function () {
                       $('#addEmployeeSuccess').modal('hide')
                   }, 3000);

                   $rootScope.landData.push(response);
                   console.log("$rootScope.landData*************" + JSON.stringify($rootScope.landData));


               }).error(function (response) {
                   console.log('Error Response1 :' + JSON.stringify(response));

               });
           }
           assetLandCreationForm = true;

       };

       $scope.charter={};
       function doSomethingElse() {
       $('#tab3').addClass('active');
       $('#complete').addClass('active');
       $('#tab2').removeClass('active');
       $('#tab1').removeClass('active');
       $('#step1').removeClass('active');
       $('#step2').removeClass('active');
       $('.nav.nav-tabs li').removeClass("active");
       document.getElementById('tab2').style.pointerEvents = 'none';
       document.getElementById('tab1').style.pointerEvents = 'none';
       }

       $scope.errorMessage = false;
       $scope.errorMessageData = '';

       $scope.createProjectPlan=function()
           {
               if($scope.charter.projectCharter != '' && $scope.charter.projectCharter != undefined && $scope.charter.projectCharter != null &&  $scope.charter.projectCharter.length>0) {
                   if($scope.charter.projectULB != '' && $scope.charter.projectULB != undefined && $scope.charter.projectULB != null) {
                       if($scope.charter.projectPlan != '' && $scope.charter.projectPlan != undefined && $scope.charter.projectPlan != null) {
                           if($scope.charter.departmentInfo != '' && $scope.charter.departmentInfo != undefined && $scope.charter.departmentInfo != null &&  $scope.charter.departmentInfo.length>0) {
                               if($scope.charter.financialYear != '' && $scope.charter.financialYear != undefined && $scope.charter.financialYear != null) {
                                   doSomethingElse();
                               } else {
                                   $scope.errorMessage = true;
                                   $scope.errorMessageData = 'Please Select Financial Year';
                                   $timeout(function(){ $scope.errorMessage=false; }, 3000);
                               }
                           } else {
                               $scope.errorMessage = true;
                               $scope.errorMessageData = 'Please Select A Department';
                               $timeout(function(){ $scope.errorMessage=false; }, 3000);
                           }
                       } else {
                           $scope.errorMessage = true;
                           $scope.errorMessageData = 'Please Enter Project Plan';
                           $timeout(function(){ $scope.errorMessage=false; }, 3000);
                       }
                   } else {
                       $scope.errorMessage = true;
                       $scope.errorMessageData = 'Please Select Project ULB';
                       $timeout(function(){ $scope.errorMessage=false; }, 3000);
                   }

               } else {
                   $scope.errorMessage = true;
                   $scope.errorMessageData = 'Please Select The Project Charter';
                   $timeout(function(){ $scope.errorMessage=false; }, 3000);
                 }
           }

            $scope.createProjectForAnAsset=function()
              {
                  console.log("land creation");

                  var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                  var land=$scope.land;
                  land['projectDetails']=$scope.charter;
                    if(assetLandCreationForm) {
                    assetLandCreationForm = false;
                    $http({
                        method: 'POST',
                        url: 'api/AssetForLands',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"},
                        data: land
                    }).success(function (response) {
                       $scope.landPostSuccess=response;
                        //createLandButton();
                        console.log('Land Response: :' + JSON.stringify(response));
                         $("#submitClosureMessage").modal("hide");
                        //$('#askModel1').modal('hide');
                        $scope.docList = [];
                        $scope.getEmployees();
                        $scope.createLand = false;
                        $scope.showLand = true;
                        $rootScope.landData.push(response);
                        console.log("$rootScope.landData*************" + JSON.stringify($rootScope.landData));

                        $scope.getLandDetails();
                        console.log("create asset details: " + JSON.stringify($scope.land));
                        $("#addEmployeeSuccess").modal("show");
                        setTimeout(function () {
                            $('#addEmployeeSuccess').modal('hide')
                        }, 3000);
                    }).error(function (response) {
                        console.log('Error Response1 :' + JSON.stringify(response));
                    });
                    }
                    assetLandCreationForm = true;
              };

            $scope.createError = false;
            $scope.msgError = '';

            var validateDetails=function()
               {
               console.log("enterrrrrrrr")

                if($scope.land.assetName!=undefined && $scope.land.assetName!="" && $scope.land.assetName!=null) {
                if($scope.land.assetDescription!=undefined && $scope.land.assetDescription!="" && $scope.land.assetDescription!=null){
                if($scope.land.accountNumber!=undefined && $scope.land.accountNumber!="" && $scope.land.accountNumber!=null){
                if($scope.land.locality!=undefined && $scope.land.locality!="" && $scope.land.locality!=null){

                if($scope.land.assetRenewableAvailability!=undefined && $scope.land.assetRenewableAvailability!="" && $scope.land.assetRenewableAvailability!=null){
                if($scope.land.assetRenewableAvailability=="Yes"){
                if($scope.land.assetUpgradeDate!=undefined && $scope.land.assetUpgradeDate!="" && $scope.land.assetUpgradeDate!=null){

                } else{
                       $scope.createError = true;
                       $scope.msgError = 'Please Select Upgradable Date';
                        return false;
                      }
                }
                                                    //console.log("$scope.assetMode"+$scope.assetMode)
                if($scope.land.assetMode!=undefined && $scope.land.assetMode!="" && $scope.land.assetMode!=null) {

                if($scope.land.assetMode=="Owned"){
                                      if($scope.land.transferDate!=undefined && $scope.land.transferDate!="" && $scope.land.transferDate!=null){

                                      if($scope.land.department!=undefined && $scope.land.department!="" && $scope.land.department!=null){

                                      if($scope.land.regdNo!=undefined && $scope.land.regdNo!="" && $scope.land.regdNo!=null){

                                      }
                                      else{
                                      $scope.createError = true;
                                      $scope.msgError = 'Please Enter Registration No';
                                      return false;
                                      }
                                  }
                                      else{
                                      $scope.createError = true;
                                      $scope.msgError = 'Please Select Department';
                                      return false;
                                      }
                                  }
                                      else{
                                      $scope.createError = true;
                                      $scope.msgError = 'Please Select Transfer Date';
                                      return false;
                                      }
                                  }
                if($scope.land.assetMode=="Managed"){
                                  if($scope.land.phoneNumber!=undefined && $scope.land.phoneNumber!="" && $scope.land.phoneNumber!=null){

                                  if($scope.land.leasedDate!=undefined && $scope.land.leasedDate!="" && $scope.land.leasedDate!=null){

                                  if($scope.land.termsInMonths!=undefined && $scope.land.termsInMonths!="" && $scope.land.termsInMonths!=null){
                                  }
                                        else{
                                        $scope.createError = true;
                                        $scope.msgError = 'Please Enter Terns in Months';
                                        return false;
                                        }
                                     }
                                        else{
                                        $scope.createError = true;
                                        $scope.msgError = 'Please Select Leased Date';
                                        return false;
                                        }
                                     }
                                else{
                                      $scope.createError = true;
                                      $scope.msgError = 'Please Enter Mobile Number';
                                      return false;
                                     }
                                }
                    //                                                        return false;
                if($scope.land.contactPersonDepartment!=undefined && $scope.land.contactPersonDepartment!="" && $scope.land.contactPersonDepartment!=null ) {
                if($scope.land.contactPerson!=undefined && $scope.land.contactPerson!="" && $scope.land.contactPerson!=null ) {
                if($scope.land.village!=undefined && $scope.land.village!="" && $scope.land.village!=null ) {
                if($scope.land.status!=undefined && $scope.land.status!="" && $scope.land.status!=null ) {
                if($scope.land.status=="commissioned"){
                   if($scope.land.commissionDate!=undefined && $scope.land.commissionDate!="" && $scope.land.commissionDate!=null){
                      }
                        else{
                           $scope.createError = true;
                           $scope.msgError = 'Please Select Commissioned Date';
                           return false;
                          }
                }
                if($scope.land.status=="decommissioned"){
                   if($scope.land.decommissionDate!=undefined && $scope.land.decommissionDate!="" && $scope.land.decommissionDate!=null){
                    }
                      else{
                         $scope.createError = true;
                         $scope.msgError = 'Please Select Decommissioned Date';
                         return false;
                        }
                }
                if($scope.land.status=="disposal"){
                   if($scope.land.disposalMethod!=undefined && $scope.land.disposalMethod!="" && $scope.land.disposalMethod!=null){
                   if($scope.land.deCommissionStatus!=undefined && $scope.land.deCommissionStatus!="" && $scope.land.deCommissionStatus!=null){
                   if($scope.land.decommissionDate!=undefined && $scope.land.decommissionDate!="" && $scope.land.decommissionDate!=null){
                   }
                   else{
                   $scope.createError = true;
                   $scope.msgError = 'Please Select Decommission Date';
                   return false;
                   }
                }
                  else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Decommission Status';
                     return false;
                    }
                }
                  else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Enter Disposal Method';
                     return false;
                    }
                }
                if($scope.land.dutyType!=undefined && $scope.land.dutyType!="" && $scope.land.dutyType!=null ) {
                if($scope.land.manufacturingSNo!=undefined && $scope.land.manufacturingSNo!="" && $scope.land.manufacturingSNo!=null){
                if($scope.land.manufacturingCode!=undefined && $scope.land.manufacturingCode!="" && $scope.land.manufacturingCode!=null){
                if($scope.land.manufacturingName!=undefined && $scope.land.manufacturingName!="" && $scope.land.manufacturingName!=null){
                if($scope.land.supplierNo!=undefined && $scope.land.supplierNo!="" && $scope.land.supplierNo!=null){
                if($scope.land.supplierCode!=undefined && $scope.land.supplierCode!="" && $scope.land.supplierCode!=null){
                if($scope.land.supplierName!=undefined && $scope.land.supplierName!="" && $scope.land.supplierName!=null){
                if($scope.land.purchaseDate!=undefined && $scope.land.purchaseDate!="" && $scope.land.purchaseDate!=null){
                if($scope.land.warrentyPeriod!=undefined && $scope.land.warrentyPeriod!="" && $scope.land.warrentyPeriod!=null){
                if($scope.land.currentServiceLevel!=undefined && $scope.land.currentServiceLevel!="" && $scope.land.currentServiceLevel!=null){
                if($scope.land.expiryDate!=undefined && $scope.land.expiryDate!="" && $scope.land.expiryDate!=null){
                if($scope.land.maintenanceContactDepartment!=undefined && $scope.land.maintenanceContactDepartment!="" && $scope.land.maintenanceContactDepartment!=null){
                if($scope.land.maintenanceContactName!=undefined && $scope.land.maintenanceContactName!="" && $scope.land.maintenanceContactName!=null){
                if($scope.land.originalLife!=undefined && $scope.land.originalLife!="" && $scope.land.originalLife!=null){
                if($scope.land.expectedDateOfDisposal!=undefined && $scope.land.expectedDateOfDisposal!="" && $scope.land.expectedDateOfDisposal!=null){
                if($scope.land.originalRemainingLife!=undefined && $scope.land.originalRemainingLife!="" && $scope.land.originalRemainingLife!=null){
                if($scope.land.revisedRemainingLife!=undefined && $scope.land.revisedRemainingLife!="" && $scope.land.revisedRemainingLife!=null){
                if($scope.land.replacementDueDate!=undefined && $scope.land.replacementDueDate!="" && $scope.land.replacementDueDate!=null){
                if($scope.land.assetLastReviewedDate!=undefined && $scope.land.assetLastReviewedDate!="" && $scope.land.assetLastReviewedDate!=null){
                if($scope.land.impairmentLife!=undefined && $scope.land.impairmentLife!="" && $scope.land.impairmentLife!=null){
                if($scope.land.costCenter!=undefined && $scope.land.costCenter!="" && $scope.land.costCenter!=null){
                if($scope.land.historicCostAvailability!=undefined && $scope.land.historicCostAvailability!="" && $scope.land.historicCostAvailability!=null ) {

                if($scope.land.historicCostAvailability=="Yes"){
                   if($scope.land.historicCost!=undefined && $scope.land.historicCost!="" && $scope.land.historicCost!=null){
                   }
                   else{
                   $scope.createError = true;
                   $scope.msgError = 'Please Enter Historic Cost';
                   return false;
                   }
                }
                if($scope.land.historicCostAvailability=="No"){
                   if($scope.land.initialValuationCost!=undefined && $scope.land.initialValuationCost!="" && $scope.land.initialValuationCost!=null){
                   }
                   else{
                   $scope.createError = true;
                   $scope.msgError = 'Please Enter Initial Valuation Cost';
                   return false;
                   }
                }

                if($scope.land.amortizationMethod!=undefined && $scope.land.amortizationMethod!="" && $scope.land.amortizationMethod!=null ) {
                if($scope.land.amortizationRate!=undefined && $scope.land.amortizationRate!="" && $scope.land.amortizationRate!=null ) {
                if($scope.land.amortizationAmount!=undefined && $scope.land.amortizationAmount!="" && $scope.land.amortizationAmount!=null ) {
                if($scope.land.bookValue!=undefined && $scope.land.bookValue!="" && $scope.land.bookValue!=null ) {
                if($scope.land.returnValue!=undefined && $scope.land.returnValue!="" && $scope.land.returnValue!=null ) {
                if($scope.land.scrapValue!=undefined && $scope.land.scrapValue!="" && $scope.land.scrapValue!=null ) {
                if($scope.land.controlAccount!=undefined && $scope.land.controlAccount!="" && $scope.land.controlAccount!=null ) {
                if($scope.land.adjustmentAccount!=undefined && $scope.land.adjustmentAccount!="" && $scope.land.adjustmentAccount!=null ) {
                if($scope.land.shrinkageAccount!=undefined && $scope.land.shrinkageAccount!="" && $scope.land.shrinkageAccount!=null ) {
                if($scope.land.priceVariance!=undefined && $scope.land.priceVariance!="" && $scope.land.priceVariance!=null ) {
                if($scope.land.currencyVariance!=undefined && $scope.land.currencyVariance!="" && $scope.land.currencyVariance!=null ) {
                if($scope.land.purchasePriceVariance!=undefined && $scope.land.purchasePriceVariance!="" && $scope.land.purchasePriceVariance!=null ) {
                if($scope.land.receiptVariance!=undefined && $scope.land.receiptVariance!="" && $scope.land.receiptVariance!=null ) {
                if($scope.land.criticalityAsset!=undefined && $scope.land.criticalityAsset!="" && $scope.land.criticalityAsset!=null ) {
                if($scope.land.sinceDate!=undefined && $scope.land.sinceDate!="" && $scope.land.sinceDate!=null ) {
                if($scope.land.acquisitionStatus!=undefined && $scope.land.acquisitionStatus!="" && $scope.land.acquisitionStatus!=null ) {
                if($scope.land.ownerShipTransferStatus!=undefined && $scope.land.ownerShipTransferStatus!="" && $scope.land.ownerShipTransferStatus!=null ) {
                if($scope.land.requestTransferName!=undefined && $scope.land.requestTransferName!="" && $scope.land.requestTransferName!=null ) {
                if($scope.land.requiredDate!=undefined && $scope.land.requiredDate!="" && $scope.land.requiredDate!=null ) {
                if($scope.land.legalFormalityStatus!=undefined && $scope.land.legalFormalityStatus!="" && $scope.land.legalFormalityStatus!=null ) {
                if($scope.land.assetSparesName!=undefined && $scope.land.assetSparesName!="" && $scope.land.assetSparesName!=null ) {
                if($scope.land.assetSparesComment!=undefined && $scope.land.assetSparesComment!="" && $scope.land.assetSparesComment!=null ) {
                if($scope.land.acquisitionComments!=undefined && $scope.land.acquisitionComments!="" && $scope.land.acquisitionComments!=null ) {
                if($scope.documentSelectUpload!=undefined && $scope.documentSelectUpload!="" && $scope.documentSelectUpload!=null ) {
                if($scope.docList!=undefined && $scope.docList!="" && $scope.docList!=null ){
                console.log("doc list is:" +JSON.stringify($scope.docList))
                return true;
                }
                     else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Attach The Files...';
                     return false;
                    }
                }
                    else{
                    $scope.createError = true;
                    $scope.msgError = 'Please Select Document Type...';
                    return false;
                    }

                }
                   else{
                   $scope.createError = true;
                   $scope.msgError = 'Please Provide A Status/issues During Acquisition Comment...';
                   return false;
                   }
               }
                  else{
                  $scope.createError = true;
                  $scope.msgError = 'Please Give A Comment For Asset Spares/Consumables ...';
                  return false;
                  }
               }
                  else{
                  $scope.createError = true;
                  $scope.msgError = 'Please Select Asset Spares or Consumables ...';
                  return false;
                  }
               }
                  else{
                  $scope.createError = true;
                  $scope.msgError = 'Please Select Legal Formality Status ...';
                  return false;
                  }
               }
                  else{
                  $scope.createError = true;
                  $scope.msgError = 'Please Select Required Date ...';
                  return false;
                  }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Request Transfer Name ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Select Ownership Transfer Status ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Select Acquisition Notice Submission ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Select Since Date ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Select Criticality Of An Asset ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Receipt Variance ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Purchase Price Variance ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Currency Variance ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Invoice Price Variance ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Shrinkage Account ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Adjustment Account ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Control Account ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Scrap Value ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Return Value ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Book Value Of An Asset ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Amortization Amount ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Amortization Rate ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Amortization Method ...';
                 return false;
                 }
               }
                 else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Select Historic Cost Availability ...';
                 return false;
                 }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Cost Center';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Enter The Details For Evidence Of Impairment';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Asset Last Reviewed Date';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Replacement Due Date';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Revised Remaining Life';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Original Remaining Life';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Expected Date Of Disposal';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Enter Original Life';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Maintenance Contact Name';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Maintenance Contact Department';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Expiry Date';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Current Service Level';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Enter Warrenty Period';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Select Installation Date';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Enter Supplier Name';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Enter Supplier Code';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Enter Supplier Number';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Enter Manufacturing Name';
                     return false;
                    }
               }
                    else{
                     $scope.createError = true;
                     $scope.msgError = 'Please Enter Manufacturing Code';
                     return false;
                    }
                }
                else{
                 $scope.createError = true;
                 $scope.msgError = 'Please Enter Manufacturing Serial Number';
                 return false;
                }
              }
                else{
                $scope.createError = true;
                $scope.msgError = 'Please Select Duty Type...';
                return false;
                }
              }
                else{
                $scope.createError = true;
                $scope.msgError = 'Please Select Present Status/Usage...';
                return false;
                }
              }
                   else{
                   $scope.createError = true;
                   $scope.msgError = 'Please Enter Village...';
                   return false;
                   }
                }
                   else{
                   $scope.createError = true;
                   $scope.msgError = 'Please Select Owner Contact Person Name';
                   return false;
                   }
                }
                   else{
                   $scope.createError = true;
                   $scope.msgError = 'Please Select Owner Contact Department';
                   return false;
                   }
                }
                   else {
                   $scope.createError = true;
                   $scope.msgError = 'Please Select Asset is Owned/Managed';
                   return false;
                   }
               }
                   else{
                   $scope.createError = true;
                   $scope.msgError = 'Please Select Asset Renewable Availability';
                   return false;
                   }
                }
                else{
                $scope.createError = true;
                $scope.msgError = 'Please Enter Location';
                return false;
                }
           }
              else{
              $scope.createError = true;
              $scope.msgError = 'Please Select Account Number';
              return false;
              }
           }
              else{
              $scope.createError = true;
              $scope.msgError = 'Please Enter Asset Description';
              return false;
              }
           }
              else{
              //alert("Please Select Asset Name")
              $scope.createError = true;
              $scope.msgError = "Please Select Asset Name";
              //$scope.msgError = "Please Select Asset Name -- Group 1";
              return false;
              }
           }

            $scope.createLandButton = function () {
                console.log("asset creation");
               var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
               var land=$scope.land;
               land['createdPerson']=loginPersonDetails.name;
               land['employeeId']=loginPersonDetails.employeeId;
               land['assetType']=$scope.assetType;
               land.file = $scope.docList;
               land['duty']=$scope.duty;
               //$("#askModal").modal("show");
               //var dataIs=JSON.parse($scope.land.dutyType);
               //land['dutyType']=dataIs.name;
               var validate=validateDetails();
               console.log("validate"+validate);
               if(validate){
               var dataIs=JSON.parse($scope.land.dutyType);
               $scope.land['dutyType']=dataIs.name;
                if($scope.ownedMode==true)
                {
                  $scope.deptData = [];
                  $scope.deptData.push(JSON.parse($scope.land.department));
                  $scope.land.department=null;
                  $scope.land.department = $scope.deptData[0].id;
                }

               $("#submitClosureMessage").modal("show");

               }
               return true;
               console.log("@@@@@@@@@@@@@@@@@@users response@@@@@@@@@@@@@@"+JSON.stringify($scope.land));
               };

            //***********select*************


            $(function () {
                $("#scroll2").hide();
                $("#scroll").click(function () {
                    $("#scroll2").show();
                    $("#scroll").hide();
                    $('html,body').animate({
                            scrollTop: $("#scroll2").offset().top
                        },
                        'slow');
                });
                $("#cancel").click(function () {
                    $("#scroll2").hide()
                    $("#scroll").show();
                    ;
                });
            });

            //******************* File Upload For Create Asset *****************************************


            $scope.editLandDetails={};
            $scope.editLandData=function (landDetails) {
                $scope.loadingImage=false;
                $scope.editLandDetails=landDetails;
                $scope.editLand=true;
                $scope.showLand = false;
                $scope.editFileUpload=$scope.editLandDetails.file;
            }
            var addAssetTypeSubmit = true;
            $scope.editLandButton=function () {
                $scope.loadingImage=false;

                $scope.empUpdationError = false;
                $scope.updateEmpError = '';

                $scope.editFileUpload = [];
                if ($scope.editLandDetails['file'].length == 0) {
                    $scope.editLandDetails['file'] = [];
                }
                var editLandDetails= $scope.editLandDetails;
                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                editLandDetails['lastEditPerson']=loginPersonDetails.name;
                editLandDetails['file'].push($scope.editFileUploads);
                if(addAssetTypeSubmit)
                {
                addAssetTypeSubmit = false;
                $http({
                    "method": "PUT",
                    "url": 'api/AssetForLands/'+$scope.editLandDetails.id,
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                    "data": editLandDetails
                }).success(function (response, data) {
                    $scope.createLand = false;
                    $scope.editLand=false;
                    $scope.showLand = true;
                    $scope.editUploadStatus=false;
                    $scope.docList = [];
                    $scope.getLandDetails();

                    console.log("update land details: "+JSON.stringify($scope.editLandDetails) );
                    $("#editEmployeeSuccess").modal("show");
                    setTimeout(function(){$('#editEmployeeSuccess').modal('hide')}, 3000);
                }).error(function (response, data) {
                    console.log("failure");

                    if(response.error.details.messages.email) {
                        $scope.updateEmpError=response.error.details.messages.email[0];
                        $scope.updateEmpError = 'Email already Exist';
                        $scope.empUpdationError = true;
                    }
                    if(response.error.details.messages.employeeId) {
                        $scope.updateEmpError=response.error.details.messages.employeeId[0];
                        $scope.updateEmpError = 'EmployeeId already Exist';
                        $scope.empUpdationError = true;
                    }
                    $timeout(function(){
                        $scope.empUpdationError=false;
                    }, 3000);
                })

                }
                addAssetTypeSubmit = true;
            }
            //$scope.getLandDetails();

   });

 app.controller('viewAssetOthersController', function($http, $scope, $window, $location, $rootScope, $routeParams) {
      console.log("viewAssetOthersController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetOthers").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetOthers .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

     //For Escaping

     $(document).on('keyup',function(evt) {
         if (evt.keyCode == 27) {
            $window.location.reload();
         }
     });

     // For Cancel or Save

       (function () {

             var beforePrint = function () {
                 //alert('Functionality to run before printing.');
             };

             var afterPrint = function () {
                 console.log('Functionality to run after printing');
                 $window.location.reload();

             };

             if (window.matchMedia) {
                 var mediaQueryList = window.matchMedia('print');

                 mediaQueryList.addListener(function (mql) {
                     //alert($(mediaQueryList).html());
                     if (mql.matches) {
                         beforePrint();
                     } else {
                         afterPrint();
                     }
                 });
             }

             window.onbeforeprint = beforePrint;
             window.onafterprint = afterPrint;

         }());


      $scope.loadingImage=true;
      $scope.pack=$routeParams.packId;
      //$scope.uploadURL='api/Uploads/dhanbadDb/download/';
      $scope.uploadURL=uploadFileURL;
      $http({
          "method": "GET",
          "url": 'api/AssetForLands/'+$scope.pack,
          //"url": 'api/AssetForLands?filter=%7B%22where%22%3A%7B%20%22assetType%22%3A%20%7B%22neq%22%3A%22Land%22%7D%7D%7D'+$scope.pack,
          "headers": {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {
          $scope.loadingImage=false;
          console.log('Users Response :' + JSON.stringify(response));
          $scope.scheme = response;
      }).error(function (response) {
          console.log('Error Response :' + JSON.stringify(response));
      });
      $scope.clickToBack = function(){
          $location.url("/createAssetOthers");
      }

     /* $scope.printTable=function()
            {
             var divToPrint=document.getElementById("viewInfo");
             newWin= window.open("");
             newWin.document.write(divToPrint.outerHTML);
             newWin.print();
             newWin.close();
            }*/

            $scope.generatePDF = function() {

                     var element = document.getElementById("elementId");
                     element.parentNode.removeChild(element);
                     var printContents = document.getElementById('formConfirmation').innerHTML;
                     var originalContents = document.body.innerHTML;

                     document.body.innerHTML = printContents;

                     window.print();

                     document.body.innerHTML = originalContents;
                     /* kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
                        kendo.drawing.pdf.saveAs(group, "Converted PDF.docx");});*/

                    }

  });

 app.controller('costTransferFundsController', function($http, $scope, $window, $location, $rootScope, Excel, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder) {
      console.log("costTransferFundsController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.costTransferFunds").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.costTransferFunds .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

      $scope.dtOptions = { paging: false, searching: false };

      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.fundDetailsList!=null &&  $scope.fundDetailsList.length>0){
              $("<tr>" +
                  "<th>Date</th>" +
                  "<th>From Cost Centre</th>" +
                  "<th>To Cost Centre</th>" +
                  "<th>Created Person</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.fundDetailsList.length;i++){
                  var fundDetails=$scope.fundDetailsList[i];
                  $("<tr>" +
                      "<td>"+fundDetails.fromDate+"</td>" +
                      "<td>"+fundDetails.fromCostCenter+"</td>" +
                      "<td>"+fundDetails.toCostCenter+"</td>" +
                      "<td>"+fundDetails.createdPerson+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'costTransferFunds');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }


      $scope.getLandDetails = function () {
          $http({
              "method": "GET",
              "url": 'api/AssetForLands',
              "headers": {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('create Asset Get Response :' + JSON.stringify(response));
              $rootScope.landData = response;

          }).error(function (response, data) {
              console.log("failure");
          });
      };
      $scope.getLandDetails();

      $scope.costCenterData=[];

      $scope.getCostCenterData=function () {

          $http({
              method: 'GET',
              url: 'api/CostCenters?filter={"where":{"status":"Active"}}',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('Cost Center  Response :' + JSON.stringify(response));
             $scope.costCenterList = response;
             $scope.loadingImage=false;
          }).error(function (response) {
              if(response.error.details.messages.name) {
                  $scope.errorMessageData=response.error.details.messages.name[0];
                  $scope.errorMessage=true;
              }

          });
      }
      $scope.getCostCenterData();

      $scope.reset = function() {
          $scope.found = {};
      }
      $scope.foundDetails={};

      $scope.getFundsDetails=function () {
          $http({
              method: 'GET',
              url: 'api/LandTransferFunds/?filter={"where":{"assetNumber":"' + $scope.selectProjectId + '"}}',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('Field visit report :' + JSON.stringify(response));
              $scope.fundDetailsList=response;

              $scope.loadingImage=false;
              //   $scope.selectProjectDetails = true;
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });
      }

            $scope.getFundsDetails();

            $scope.addFundTransfer=function () {

                $scope.CaseCreationError = false;
                $scope.CaseError = '';
                var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                $scope.foundDetails['createdPerson']=loginPersonDetails.name;
                $scope.foundDetails['assetNumber']=$scope.selectProjectId;
                $scope.foundDetails['fromCostCenter']=$scope.selectProjectDetails.costCenter;

                console.log("json data" +JSON.stringify($scope.foundDetails));

            if($scope.foundDetails.fromDate!=undefined){

                if($scope.foundDetails.toCostCenter!=undefined){

                $http({
                    method: 'POST',
                    url: 'api/LandTransferFunds',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data:$scope.foundDetails
                }).success(function (response) {
                    console.log('Users Response :' + JSON.stringify(response));
                    //$("#createTransferFund").hide();
                    $('#createTransferFund').modal('hide');
                    $scope.selectareas($scope.selectProjectId);
                    $scope.getCostCenterData();
                    $scope.getFundsDetails();
                    //$window.location.reload();

                    //  //alert('successfully created charter');
                    //$scope.getFundsDetails();
                    $("#scroll2").hide();
                    $("#scroll").show();
                    // $scope.getTaskDetails();
                    //     $("#createCharter").modal("hide");
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
//                    $scope.CaseCreationError = true;
//                    $scope.CaseError = 'Please Select Cost Center Name';

                });
                }
                else {
                   $scope.CaseCreationError = true;
                   $scope.CaseError = 'Please Select Cost Center Name';

                 }
                }
                else{
                 $scope.CaseCreationError = true;
                 $scope.CaseError = 'Please Select Date';
                }
            }

      $scope.getLandDetails();

      $scope.castS = function(s){
      for(var i=0;$scope.costCenterList.length;i++){
      if($scope.costCenterList[i].name==s){
         $scope.costCenterList.splice(i,1)
      break;
      }
      }
      }

      $scope.selectareas = function(assetNumber){
          $scope.selectProjectId = assetNumber;

          $http({
              method: 'GET',
              url: 'api/AssetForLands/?filter={"where":{"assetNumber":"' + $scope.selectProjectId + '"}}',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('Field visit report :' + JSON.stringify(response));
              $scope.selectProjectDetails=response[0];
              $scope.getCostCenterData();
              $scope.getFundsDetails();
              $scope.showAction = true;
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });

          //$scope.getFundsDetails();
      }
      $(function () {
          $("#scroll2").hide();
          $("#scroll").click(function() {
              $("#scroll2").show();
              $("#scroll").hide();
              $('html,body').animate({
                      scrollTop: $("#scroll2").offset().top},
                  'slow');
          });
          $("#cancel").click(function() {
              $("#scroll2").hide()
              $("#scroll").show();;
          });
      })

      $scope.editHeaderScheme = function() {
          $("#editHeaderSchemeDetails").modal("show");
      }

  });

 app.controller('demandProjectionController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

         console.log("demandProjectionController");
      $(document).ready(function () {
          $('html,body').scrollTop(0);
          $(".sidebar-menu li ul > li").removeClass('active1');
          $('[data-toggle="tooltip"]').tooltip();
          $(".sidebar-menu #landAssetLi").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
          $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.assetDemandProjection").addClass("active1").siblings('.active1').removeClass('active1');
          $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.assetDemandProjection .collapse").addClass("in");
          $(".sidebar-menu li .collapse").removeClass("in");
      });


         $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
             .withOption("sPaginationType", "bootstrap")
             .withOption("retrieve", true)
             .withOption('stateSave', true)
             .withPaginationType('simple_numbers')
             .withOption('order', [0, 'ASC']);

         $scope.exportToExcel=function(tabelId){
             if( $scope.logBookList!=null &&  $scope.logBookList.length>0){
                 $("<tr>" +
                     "<th>Asset Type</th>" +
                     "<th>Asset Name</th>" +
                     "<th>Quantity</th>" +
                     "<th>Expected By Date</th>" +
                     "<th>Estimation Cost</th>" +
                     "<th>Procurement Method</th>" +
                     "<th>Approval Status</th>" +
                     "<th>Department</th>" +
                     "<th>Financial Year</th>" +
                     "</tr>").appendTo("table"+tabelId);
                 for(var i=0;i<$scope.logBookList.length;i++){
                     var logBook=$scope.logBookList[i];
                     $("<tr>" +
                         "<td>"+logBook.assetType+"</td>" +
                         "<td>"+logBook.assetName+"</td>" +
                         "<td>"+logBook.quantity+"</td>" +
                         "<td>"+logBook.expectedDate+"</td>" +
                         "<td>"+logBook.estimationCost+"</td>" +
                         "<td>"+logBook.procurementMethod+"</td>" +
                         "<td>"+logBook.status+"</td>" +
                         "<td>"+logBook.department+"</td>" +
                         "<td>"+logBook.financialYear+"</td>" +
                         "</tr>").appendTo("table"+tabelId);
                 }
                 var exportHref=Excel.tableToExcel(tabelId,'demandProjection');
                 $timeout(function(){location.href=exportHref;},100);
             }else{
                 $("#emptyDataTable").modal("show");
                 setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
             }
         }

         $scope.addButtonShow=false;
         console.log("show/hide status" +$scope.addButtonShow)



         $http({
             method: 'GET',
             url :'api/EmployeeDepartments?filter={"where":{"status":"Active"}}',
             headers: {"Content-Type": "application/json", "Accept": "application/json"}

         }).success(function (response) {
             //console.log('Users Response :' + JSON.stringify(response));
             $scope.departMen=response;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });

         $http({
          method: 'GET',
          //url: 'api/AssetTypes?filter={"where":{"status":"Active"}}',
          url: 'api/AssetTypes',
          headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
                  //console.log('Users Response :' + JSON.stringify(response));
          $scope.assetTypeList = response;
          }).error(function (response) {
                  //console.log('Error Response :' + JSON.stringify(response));
          });


          $http({
           method: 'GET',
           url: 'api/AssetStatuses?filter={"where":{"status":"Active"}}',
           headers: {"Content-Type": "application/json", "Accept": "application/json"}
           }).success(function (response) {
           //console.log('Users Response :' + JSON.stringify(response));
           $scope.assetStatusList = response;
           $scope.loadingImage=false;
           }).error(function (response) {
           if(response.error.details.messages.name) {
               $scope.errorMessageData=response.error.details.messages.name[0];
               $scope.errorMessage=true;
           }
           });


         $scope.showCharter = function() {
             $scope.createCharterArea = true;
             $scope.editCharterArea = false;
         }
         $scope.editCharterButton = function(charter) {
             $("#editCharter").modal("show");
             $scope.editCharterArea = true;
             $scope.createCharterArea = false;
             $scope.editCharterData=angular.copy(charter);
         }

         var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));

          $scope.getEmployeeDepartment = function(department) {
             //alert("reer" + JSON.stringify(department));

                    $scope.departmentName=department;

 //                      $http({
 //                     method: 'GET',
 //                     url :'api/DemandProjections/getDetails?employeeId=%7B%22department%22%3A%20%22'+$scope.departmentName+'%22%2C%20%22financialYear%22%3A%20%22'+$scope.finYear+'%22%2C%22employeeId%22%3A%22'+loginPersonDetails.employeeId+'%22%7D',
 //                          headers: {"Content-Type": "application/json", "Accept": "application/json"}
 //                     }).success(function (response) {
 //                     console.log('Users Response :' + JSON.stringify(response));
 //                     $scope.logBookList=response;
 //                     }).error(function (response) {
 //                     console.log('Error Response :' + JSON.stringify(response));
 //                 });


                }


          $scope.financialYear = ["2016-2017","2017-2018","2018-2019","2019-2020",
           "2020-2021","2021-2022","2022-2023","2023-2024","2024-2025","2025-2026"];

          $scope.getFinancialYear = function (financialYear)
          {
           $scope.addButtonShow=true;
              var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
              $scope.finYear=financialYear;
              console.log("11111111111111111 "+$scope.finYear);
              var details={
                  'department':$scope.departmentName,
                  'financialYear':$scope.finYear,
                  'employeeId':loginPersonDetails.employeeId
              }
              $scope.test = details;
             console.log(JSON.stringify($scope.test))
              console.log('details are '+JSON.stringify(details));
           //   DemandProjections/getDetails?employeeId=%7B%22department%22%3A%22Education%22%2C%22financialYear%22%3A%222016-2017%22%2C%22employeeId%22%3A%22SuperAdmin00001%22%7D
                $http({
                method: 'GET',
                    url :'api/DemandProjections/getDetails?employeeId=%7B%22department%22%3A%20%22'+$scope.departmentName+'%22%2C%20%22financialYear%22%3A%20%22'+$scope.finYear+'%22%2C%22employeeId%22%3A%22'+loginPersonDetails.employeeId+'%22%7D',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                console.log('Users Response :' + JSON.stringify(response));
                $scope.logBookList=response;
                }).error(function (response) {
                console.log('Error Response :' + JSON.stringify(response));
            });
          }

          $scope.removeData=function()
          {
          $scope.charter={};
          $scope.clearField="";
          }
         $scope.createDemandProjection=function () {
             var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
             var charterDetails=$scope.charter;
             var aasetTypeName=$scope.charter.assetType.name;
             charterDetails['assetType']=aasetTypeName;
             console.log('login person details'+JSON.stringify(loginPersonDetails.department));
             charterDetails['employeeName']=loginPersonDetails.firstName;
             charterDetails['createdPersonId']=loginPersonDetails.employeeId;
             charterDetails['department']=$scope.test.department;
             charterDetails['financialYear']=$scope.test.financialYear;

             $scope.clearField=$scope.charter.assetName;
              console.log("----------------------------"+JSON.stringify($scope.clearField));
              console.log(JSON.stringify($scope.charter));
                console.log(JSON.stringify(charterDetails))
             $http({
                 method: 'POST',
                 url :'api/DemandProjections',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                 "data":charterDetails
             }).success(function (response) {
                 console.log('Log Response:'+JSON.stringify(response))
                 //$scope.charter = {};
                  $scope.getEmployeeDepartment($scope.departmentName);
                 //$scope.getLogBookDetails();
                 $("#createCharter").modal("hide");
             }).error(function (response) {
                 console.log('Error Response :' + JSON.stringify(response));
             });
         }

         $scope.editDemandProjection=function () {
             var editCharterDetails= $scope.editCharterData;
             var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
             editCharterDetails['lastEditPerson']=loginPersonDetails.name;
             $http({
                 method: 'PUT',
                 url :'api/DemandProjections/'+$scope.editCharterData.id,
                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                 "data":editCharterDetails
             }).success(function (response) {
                 //$scope.getLogBookDetails();
                 $scope.getEmployeeDepartment($scope.departmentName);
                 //$scope.selectedEmployee();
                 $("#editCharter").modal("hide");
             }).error(function (response) {
                 console.log('Error Response :' + JSON.stringify(response));
             });
         }


      $scope.comments;
      $scope.approvalModal=function(fieldViisitData){
          $scope.fieldData=angular.copy(fieldViisitData);
          $scope.comments='';
          $("#approavlReport").modal("show");
      }
      $scope.rejectModal=function(fieldViisitData){
          $scope.fieldData=angular.copy(fieldViisitData);
          $("#rejectReport").modal("show");
          $scope.comments='';
      }

      $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
      var employeeId=$scope.userInfo.employeeId;
      var employeeName=$scope.userInfo.name;
      var approvalDoubleClick=false;
      $scope.approvalData=function(){
          var updateDetails = {
              "acceptLevel":$scope.fieldData.acceptLevel,
              "acceptStatus": "Yes",
              "requestStatus": "Approval",
              "comment":  $scope.comments.comment,
              "requestId":$scope.fieldData.id,
              "employeeId":employeeId,
              "employeeName":employeeName
          };

          console.log("ready to update:" +JSON.stringify(updateDetails))
          if(!approvalDoubleClick){
              approvalDoubleClick=true;
              $http({
                  "method": "POST",
                  "url": 'api/DemandProjections/updateDetails',
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": updateDetails
              }).success(function (response, data) {
                  approvalDoubleClick=false;
                  $scope.getEmployeeDepartment($scope.departmentName);
                  $("#approavlReport").modal("hide");
              }).error(function (response, data) {
                  approvalDoubleClick=false;
                  console.log("failure");
              })
          }
      }
      var rejectedDoubleClick=false;
      $scope.rejectData=function(){

          var updateDetails = {
              "acceptLevel":$scope.fieldData.acceptLevel,
              "acceptStatus": "No",
              "requestStatus": "Rejected",
              "comment":  $scope.comments.comment,
              "requestId":$scope.fieldData.id,
              "employeeId":employeeId,
              "employeeName":employeeName
          };
           console.log("reject:" +JSON.stringify(updateDetails))
          if(!rejectedDoubleClick){
              rejectedDoubleClick=true;
              $http({
                  "method": "POST",
                  "url": 'api/DemandProjections/updateDetails',
                  "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": updateDetails
              }).success(function (response, data) {
                  $scope.getEmployeeDepartment($scope.departmentName);
                  $("#rejectReport").modal("hide");
                  rejectedDoubleClick=false;

              }).error(function (response, data) {
                  console.log(data);
                  console.log(response);
                  rejectedDoubleClick=false;
              })

          }

      }

      $scope.editProjectUploads = function(document) {
          console.log("editing the data" +JSON.stringify(document));
          //$("#editProjectDeliverables").modal("show");
          $("#editCharter").modal("show");
          $scope.editCharterData = angular.copy(document);
      }
      var editUploadDoubleClick=false;

      $scope.editProjectUploadsDetails=function () {

              var editProjectUploadDetails = $scope.editCharterData;
              var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
              editProjectUploadDetails['lastEditPerson']=loginPersonDetails.name;
              editProjectUploadDetails['lastEditPersonId']=loginPersonDetails.employeeId;

              if(!editUploadDoubleClick){
                  editUploadDoubleClick=true;
                  $http({
                      method: 'POST',
                      url: 'api/DemandProjections/updateContent'  ,
                      headers: {"Content-Type": "application/json", "Accept": "application/json"},
                      "data": editProjectUploadDetails
                  }).success(function (response) {
                      $scope.getEmployeeDepartment($scope.departmentName);
                      $("#editCharter").modal("hide");
                      $("#editPlanLayoutSuccess").modal("show");
                      editUploadDoubleClick=false;
                      setTimeout(function(){$('#editPlanLayoutSuccess').modal('hide')}, 3000);
                  }).error(function (response) {
                      editUploadDoubleClick=false;
                      console.log('Error Response :' + JSON.stringify(response));
                  });
              }


      }

      $scope.getLogBookDetails=function () {
             $http({
                 method: 'GET',
                 url :'api/DemandProjections?filter={"where":{"department":"'+$scope.departmentName+'","financialYear":"'+$scope.finYear+'"}}',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 console.log('Users Response :' + JSON.stringify(response));
                 $scope.logBookList=response;
             }).error(function (response) {
                 console.log('Error Response :' + JSON.stringify(response));
             });

         }
 $scope.removeData();

     });

 app.controller('createAssetObjectivesController', function($scope, $rootScope, $window, $location, $http, Excel, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder, anchorSmoothScroll) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetObjectivesAndKPIs").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetObjectivesAndKPIs .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");


         $("#scroll").click(function() {

             document.getElementById('welcomeDiv').style.display = "block";
             $('html, body').animate({
                 scrollTop: $("#scroll2").offset().top
             }, 2000);
         });

     });

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

        $scope.exportToExcel=function(tabelId){
            //alert("TTTT :"+tabelId);
            if( $scope.assetTypeList1!=null &&  $scope.assetTypeList1.length>0){
                $("<tr>" +
                    "<th>Asset Type</th>" +
                    "<th>Asset ID</th>" +
                    "<th>Objectives</th>" +
                    "<th>KPI's</th>" +
                    "<th>Improvement Plan</th>" +
                    "</tr>").appendTo("table"+tabelId);
                for(var i=0;i<$scope.assetTypeList1.length;i++){
                    var typeList=$scope.assetTypeList1[i];
                    console.log("VVVV :"+JSON.stringify($scope.assetTypeList1[0]));
                    console.log("DATA :"+JSON.stringify(typeList.kpi));

                   var spanDetails="";
                   var impPlan="";
                   for(var j=0;j<typeList.kpi.length;j++){
                       console.log(JSON.stringify(typeList.kpi[j]))
                       spanDetails=spanDetails+"<span>"+typeList.kpi[j].kpino+",</span>";
                       impPlan=impPlan+"<span>"+typeList.kpi[j].improvementPlan+",</span>";
                   }
                    $("<tr>" +
                        "<td>"+typeList.assetType+"</td>" +
                        "<td>"+typeList.assetId+"</td>" +
                        "<td>"+typeList.objectives+"</td>" +
                        "<td>"+spanDetails+"</td>"+
                        "<td>"+impPlan+"</td>"+
                        // "<span > "+loopData.kpino+",</span>" +
                        /*"<td>"+typeList.kpis.improvementPlan+"</td>" +*/
                        "</tr>").appendTo("table"+tabelId);
                }
                var exportHref=Excel.tableToExcel(tabelId,'assetObjectives');
                $timeout(function(){location.href=exportHref;},100);
            }else{
                $("#emptyDataTable").modal("show");
                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
            }
        }

        $scope.assetbutton1=false;

             $scope.assetbutton = function(){
                 $scope.assetbutton1=true;
             }
             $scope.objectives=false;
             /* $scope.addObject = function(){
              $scope.objectives=true;

              }*/
             $scope.objectivesAndKpi={};
             $scope.timeList= [];
             var timeDetails={
                 "name":'name'

             };

             $scope.timeList.push(timeDetails);
             $scope.rowsLength=1;
             $scope.addIme = function(){

                 console.log('lenght is'+    $scope.timeList.length);
                 console.log('monday value'+$scope.timeList[0].monday);

                 var lengthOfRows=$scope.timeList.length+1;

                 var mondayName='monday'+lengthOfRows;
                 var tuesdayName='tuesday'+lengthOfRows;

                 var projectRows = {
                     monday:'name'
                 };
                 $scope.timeList.push(projectRows);
             };


             $scope.objectives1=false;
             /*$scope.addObject1 = function(){
              $scope.objectives1=true;
              }*/

             /*-------for kpi's-----*/

             $scope.objectives=false;
             $scope.objectivesAndKpi={};
             $scope.timeList1= [];
             var timeDetails125={
                 "name":'name',
                 "improvementPlan":''

             };

             $scope.timeList1.push(timeDetails);
             $scope.rowsLength=1;
             $scope.addIme1 = function(){

                 console.log('lenght is'+    $scope.timeList1.length);
                 console.log('monday value'+$scope.timeList1[0].monday);

                 var lengthOfRows=$scope.timeList1.length+1;

                 var mondayName='monday'+lengthOfRows;
                 var tuesdayName='tuesday'+lengthOfRows;

                 var projectRows = {
                     monday:'name',
                     improvementPlan:''
                 };
                 $scope.timeList1.push(projectRows);
             };


             $scope.objectives1=false;
             /*$scope.addObject1 = function(){
              $scope.objectives1=true;
              }*/
             $scope.assetType1={"id":""}
             $scope.kpilistS = [];
             $scope.getAssetId = function(name) {

             $scope.getAssetName=name;

                 $scope.assetTypeList1 = [];
                 $scope.objectives = [];
                 $scope.kpilistS = [];
                 if($scope.assetTypeList1){
                     $scope.update=true;
                 }
                 else{
                     $scope.update=false;
                 }
                 $("#welcomeDiv").hide();
                    $window.localStorage.setItem('assetType',name);
                 $http({
                     method: 'GET',
                     url: 'api/AssetForLands?filter=%7B%22where%22%3A%7B%20%22assetName%22%3A%22'+name+'%22%7D%7D',
                     headers: {"Content-Type": "application/json", "Accept": "application/json"}
                 }).success(function (response) {
                     $scope.assetList = response;
                     /*$scope.objectives = [];
                     $scope.kpilistS = [];*/
                     console.log('response details'+JSON.stringify(response));
                 }).error(function (response) {
                     //console.log('Error Response :' + JSON.stringify(response));
                 });


             }

              $scope.addButtonShow=false;
              $scope.hideButton123=false;
              $scope.getAsset = function(name) {
              $scope.timeDetails1='';
              $scope.timeDetails='';

              $scope.getAssetIdName=name;
                  $scope.addButtonShow=true;
                  $scope.assetTypeList1 = [];
                  $scope.objectives = [];
                  $scope.kpilistS = [];
                  if($scope.assetTypeList1){
                      $scope.update=false;
                  }
                  else{
                      $scope.update=true;
                  }
              $window.localStorage.setItem('asset',name);
                        $http({
                           method: 'GET',
                           url: 'api/AssetObjectives?filter={"where":{"assetId":"'+name+'"}}',
                           headers: {"Content-Type": "application/json", "Accept": "application/json"}
                        }).success(function (response) {
                           $scope.assetTypeList1 = response;
                           console.log('Users Response :' + JSON.stringify($scope.assetTypeList1))
                           $scope.objectives=response[0].objectives;
                           $scope.updateAssetId=response[0].id;
                           $scope.kpilistS= response[0].kpi;
                            if($scope.assetTypeList1.length>0){
                                $scope.hideButton123=true;
                            }

                           console.log("$scope.assertTypelist"+JSON.stringify($scope.assertTypelistData));
                           //$scope.timeList=response;
                           //  $scope.assetObjectives=response.objectives;
                           console.log("$scope.assetObjectives"+JSON.stringify($scope.assetObjectives));
                           if($scope.assetTypeList){
                               $scope.update=true;
                           }
                           else{
                               $scope.update=false;
                           }
                       }).error(function (response) {
                           //console.log('Error Response :' + JSON.stringify(response));
                       });

              }

             $scope.removePerson1 = function(index){
                 console.log("252:"+index);
                 console.log("332:"+ JSON.stringify($scope.kpilistS));
                 $scope.kpilistS.splice(index, 1);
                 console.log("3y2:"+ JSON.stringify($scope.kpilistS));
             };

             $scope.objectives = [];
             $scope.objectiveObject={"objno":""}
             $scope.selectAssist=function(data){
                 if($scope.objectives.length!=0) {
                     for (i = 0; i < $scope.objectives.length; i++) {
                         if ($scope.objectives[i] == data) {
                             alert("This Objective Already Added, Please Select Another Objective.");
                             return false;
                         }
                         $scope.check123 = true;
                     }
                 }else{
                     $scope.check123 = true;
                 }

                 if($scope.check123){
                     console.log("data"+data);
                     $scope.objectives.push(data);
                     $scope.objectivesAndKpi["objectives"] = $scope.objectives;
                 }

             }

             $scope.kpiObject={"kpino":"","improvementPlan":""}
             $scope.kpiSelect=function(data){
                 $("#message").hide();
                 $scope.kpiObject.kpino=data;
                 console.log("$scope.kpiObject.kpi"+JSON.stringify($scope.kpiObject));

             }


             $scope.selectKPI=function(data){
                 if($scope.kpiObject.kpino !=''){
                    // alert("Please Choose KPI");
                     if($scope.kpilistS.length!=0) {
                         for (i = 0; i < $scope.kpilistS.length; i++) {
                             if ($scope.kpiObject.kpino == $scope.kpilistS[i].kpino) {
                                 $("#message").show();
                                 return false;
                             }
                             $scope.check = true;

                         }
                     }else{
                         $scope.check = true;
                     }
                     if($scope.check){
                         console.log("comment"+JSON.stringify($scope.kpiObject));
                         console.log("comment"+JSON.stringify($scope.kpilistS));
                         $scope.kpilistS.push($scope.kpiObject);
                         $scope.kpiObject={"kpino":"","improvementPlan":""}
                         $scope.objectivesAndKpi["kpi"] = $scope.kpilistS;
                         console.log("$scope.kpi"+JSON.stringify($scope.kpilistS));
                     }
                 }


             }

             $scope.removePerson = function(index){
                 //alert("331:"+index)
                 //alert("332:"+$scope.objectives);
                 $scope.objectives.splice(index, 1);
                 //alert("332:"+$scope.objectives);
             };

             $scope.updateAssi=function() {
                 //alert("324:"+JSON.stringify($scope.objectives));
                 $scope.objectivesAndKpi.objectives=$scope.objectives;
                 $scope.objectivesAndKpi.kpi=$scope.kpilistS;

                 $http({
                     method: 'PUT',
                     url: 'api/AssetObjectives/'+$scope.updateAssetId,
                     headers: {"Content-Type": "application/json", "Accept": "application/json"},
                     data:$scope.objectivesAndKpi
                 }).success(function (response) {
                     console.log('Users Response :' + JSON.stringify(response));
                     console.log("$scope.assetObjectives"+JSON.stringify($scope.assetObjectives));
                     $("#editAssetObjective").modal("show");
                     setTimeout(function(){$('#editAssetObjective').modal('hide')}, 3000);
                     //$window.location.reload();
                 }).error(function (response) {
                     //console.log('Error Response :' + JSON.stringify(response));
                 });
             }

             $http({
                 method: 'GET',
                 //url: 'api/AssetTypes?filter={"where":{"status":"Active"}}',
                 url: 'api/AssetTypes',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 //console.log('Users Response :' + JSON.stringify(response));
                 $scope.assetTypeList = response;
             }).error(function (response) {
                 //console.log('Error Response :' + JSON.stringify(response));
             });

             //console.log("$scope.assetType1" + $scope.assetType1)


             $http({
                 method: 'GET',
                 url: 'api/MaintenanceObjectives?filter={"where":{"status":"Active"}}',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 //console.log('Users Response :' + JSON.stringify(response));
                 //$scope.assetObjectives = response;
                 $scope.assertTypelistData=response;
                 console.log("$scope.assestName"+$scope.assestName)

             }).error(function (response) {

             });

             $scope.selectList=function(name) {

             }

             $http({
                 method: 'GET',
                 url: 'api/AssetKPIs?filter={"where":{"status":"Active"}}',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 //console.log('Users Response :' + JSON.stringify(response));
                 $scope.assetKpi1 = response;
             }).error(function (response) {

             });
            $scope.errorMessage = false;
            $scope.errorMessageData = '';

            var addCreateObjectivesDoubleClick =true;
             $scope.addCreateObjectives = function(){
                 if(addCreateObjectivesDoubleClick){
                     addCreateObjectivesDoubleClick = false;

                     $scope.objectivesAndKpi.assetId=$window.localStorage.getItem('asset');
                     $scope.objectivesAndKpi.assetType=$window.localStorage.getItem('assetType');
                     if($scope.timeDetails != '' && $scope.timeDetails != undefined && $scope.timeDetails != null){
                     if($scope.timeDetails1 != '' && $scope.timeDetails1 != undefined && $scope.timeDetails1 != null){
                     if($scope.kpilistS[0] != '' && $scope.kpilistS[0] != undefined && $scope.kpilistS[0] != null){
                     $http({
                         method: 'POST',
                         url: 'api/AssetObjectives',
                         headers: {"Content-Type": "application/json", "Accept": "application/json"},
                         data: $scope.objectivesAndKpi
                     }).success(function (response) {
                         //console.log('Users Response :' + JSON.stringify(response));
                         $window.localStorage.removeItem('asset');
                         $window.localStorage.removeItem('assetType');
                         $("#addAssetObjective").modal("show");
                         setTimeout(function(){$('#addAssetObjective').modal('hide')}, 3000);
                         //$window.location.reload();
                         //$scope.getAssetId($scope.getAssetName);
                         $scope.getAsset($scope.getAssetIdName);
                     }).error(function (response) {

                     });
                     }else{
                         $scope.errorMessage = true;
                         $scope.errorMessageData = 'Please Give A Comment And Click The Add Button';
                         $timeout(function(){ $scope.errorMessage=false; }, 3000);
                         }
                     }else{
                       $scope.errorMessage = true;
                       $scope.errorMessageData = 'Please Select To Add KPI';
                       $timeout(function(){ $scope.errorMessage=false; }, 3000);
                       }
                     }else{
                     $scope.errorMessage = true;
                     $scope.errorMessageData = 'Please Select To Add Objectives';
                     $timeout(function(){ $scope.errorMessage=false; }, 3000);
                     }
                 }
                 addCreateObjectivesDoubleClick = true;

             }

    });

app.controller('createAssetSparesController', function($scope, $rootScope, $window, $location, $http, Excel, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder) {

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #landAssetLi").addClass("active");
        $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
        $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
        $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetSparesAndConsumables").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetSparesAndConsumables .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");

        $("#scroll").click(function() {

            document.getElementById('welcomeDiv').style.display = "block";
            $('html, body').animate({
                scrollTop: $("#scroll2").offset().top
            }, 2000);
        });

    });

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    $scope.exportToExcel=function(tabelId){
        //alert("SSSS :"+tabelId);
        if( $scope.assetTypeList1!=null &&  $scope.assetTypeList1.length>0){
            $("<tr>" +
                "<th>Asset Type</th>" +
                "<th>Asset ID</th>" +
                "<th>Spares/Consumables</th>" +
                "<th>Comments</th>" +
                "</tr>").appendTo("table"+tabelId);
            for(var i=0;i<$scope.assetTypeList1.length;i++){
                var typeList=$scope.assetTypeList1[i];
                console.log("spares :"+JSON.stringify($scope.assetTypeList1[0]));
                console.log("spares DATA :"+JSON.stringify(typeList.kpi));

                var spareDetails = "";
                var commentDetails = "";
                for(i=0; i<typeList.kpi.length; i++){
                    console.log(JSON.stringify(typeList.kpi[i]))
                    spareDetails=spareDetails+"<span>"+typeList.kpi[i].kpino+",</span>";
                    commentDetails=commentDetails+"<span>"+typeList.kpi[i].comments+",</span>";
                }

                $("<tr>" +
                    "<td>"+typeList.assetType+"</td>" +
                    "<td>"+typeList.assetId+"</td>" +
                    "<td>"+spareDetails+"</td>" +
                    "<td>"+commentDetails+"</td>" +
                    "</tr>").appendTo("table"+tabelId);
            }
            var exportHref=Excel.tableToExcel(tabelId,'assetSpares');
            $timeout(function(){location.href=exportHref;},100);
        }else{
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }

    $scope.assetbutton1=false;

    $scope.assetbutton = function(){
        $scope.assetbutton1=true;
    }
    $scope.objectives=false;
    /* $scope.addObject = function(){
     $scope.objectives=true;

     }*/
    $scope.objectivesAndKpi={};
    $scope.timeList= [];
    var timeDetails={
        "name":'name'

    };

    $scope.timeList.push(timeDetails);
    $scope.rowsLength=1;
    $scope.addIme = function(){

        console.log('lenght is'+    $scope.timeList.length);
        console.log('monday value'+$scope.timeList[0].monday);

        var lengthOfRows=$scope.timeList.length+1;

        var mondayName='monday'+lengthOfRows;
        var tuesdayName='tuesday'+lengthOfRows;

        var projectRows = {
            monday:'name'
        };
        $scope.timeList.push(projectRows);
    };


    $scope.objectives1=false;
    /*$scope.addObject1 = function(){
     $scope.objectives1=true;
     }*/

    /*-------for kpi's-----*/

    $scope.objectives=false;
    $scope.objectivesAndKpi={};
    $scope.timeList1= [];
    var timeDetails125={
        "name":'name',
        "improvementPlan":''

    };

    $scope.timeList1.push(timeDetails);
    $scope.rowsLength=1;
    $scope.addIme1 = function(){

        console.log('lenght is'+    $scope.timeList1.length);
        console.log('monday value'+$scope.timeList1[0].monday);

        var lengthOfRows=$scope.timeList1.length+1;

        var mondayName='monday'+lengthOfRows;
        var tuesdayName='tuesday'+lengthOfRows;

        var projectRows = {
            monday:'name',
            improvementPlan:''
        };
        $scope.timeList1.push(projectRows);
    };


    $scope.objectives1=false;
    /*$scope.addObject1 = function(){
     $scope.objectives1=true;
     }*/
    $scope.assetType1={"id":""}
    $scope.kpilistS = [];
    $scope.getAssetId = function(name) {
        $scope.assetTypeList1 = [];
        $scope.objectives = [];
        $scope.kpilistS = [];
        if($scope.assetTypeList1){
            $scope.update=true;
        }
        else{
            $scope.update=false;
        }
        $("#welcomeDiv").hide();
        $window.localStorage.setItem('assetType',name);
        $http({
            method: 'GET',
            url: 'api/AssetForLands?filter=%7B%22where%22%3A%7B%20%22assetName%22%3A%22'+name+'%22%7D%7D',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.assetList = response;
            /*$scope.objectives = [];
             $scope.kpilistS = [];*/
            console.log('response details'+JSON.stringify(response));
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.addButtonShow=false;
    $scope.hideButton123=false;
    $scope.getAsset = function(name) {
    $scope.getAssetIdName=name;
        $scope.addButtonShow=true;
        $scope.assetTypeList1 = [];
        $scope.objectives = [];
        $scope.kpilistS = [];
        if($scope.assetTypeList1){
            $scope.update=false;
        }
        else{
            $scope.update=true;
        }
        $window.localStorage.setItem('asset',name);
        $http({
            method: 'GET',
            url: 'api/CreateAssetSpares?filter={"where":{"assetId":"'+name+'"}}',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            $scope.assetTypeList1 = response;
            console.log('Users Response :' + JSON.stringify($scope.assetTypeList1))
            $scope.objectives=response[0].objectives;
            $scope.updateAssetId=response[0].id;
            $scope.kpilistS= response[0].kpi;
            if($scope.assetTypeList1.length>0){
                $scope.hideButton123=true;
            }

            console.log("$scope.assertTypelist"+JSON.stringify($scope.assertTypelistData));
            //$scope.timeList=response;
            //  $scope.assetObjectives=response.objectives;
            console.log("$scope.assetObjectives"+JSON.stringify($scope.assetObjectives));
            if($scope.assetTypeList){
                $scope.update=true;
            }
            else{
                $scope.update=false;
            }
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });

    }

    $scope.removePerson1 = function(index){
        console.log("252:"+index);
        console.log("332:"+ JSON.stringify($scope.kpilistS));
        $scope.kpilistS.splice(index, 1);
        console.log("3y2:"+ JSON.stringify($scope.kpilistS));
    };

    $scope.objectives = [];
    $scope.objectiveObject={"objno":""}
    $scope.selectAssist=function(data){
        if($scope.objectives.length!=0) {
            for (i = 0; i < $scope.objectives.length; i++) {
                if ($scope.objectives[i] == data) {
                    alert("This Objective Already Added, Please Select Another Objective.");
                    return false;
                }
                $scope.check123 = true;
            }
        }else{
            $scope.check123 = true;
        }

        if($scope.check123){
            console.log("data"+data);
            $scope.objectives.push(data);
            $scope.objectivesAndKpi["objectives"] = $scope.objectives;
        }

    }


    $scope.kpiObject={"kpino":"","comments":""}
    $scope.kpiSelect=function(data){
        $("#message").hide();
        $scope.kpiObject.kpino=data;
        console.log("$scope.kpiObject.kpi"+JSON.stringify($scope.kpiObject));
    }

    $scope.selectKPI=function(data){
        if($scope.kpiObject.kpino !=''){
            // alert("Please Choose KPI");
            if($scope.kpilistS.length!=0) {
                for (i = 0; i < $scope.kpilistS.length; i++) {
                    if ($scope.kpiObject.kpino == $scope.kpilistS[i].kpino) {
                        $("#message").show();
                        return false;
                    }
                    $scope.check = true;

                }
            }else{
                $scope.check = true;
            }
            if($scope.check){
                console.log("comment"+JSON.stringify($scope.kpiObject));
                console.log("comment"+JSON.stringify($scope.kpilistS));
                $scope.kpilistS.push($scope.kpiObject);
                $scope.kpiObject={"kpino":"","improvementPlan":""}
                $scope.objectivesAndKpi["kpi"] = $scope.kpilistS;
                console.log("$scope.kpi"+JSON.stringify($scope.kpilistS));
            }
        }

    }

    $scope.removePerson = function(index){
        //alert("331:"+index)
        //alert("332:"+$scope.objectives);
        $scope.objectives.splice(index, 1);
        //alert("332:"+$scope.objectives);
    };

    $scope.updateAssi=function() {
        //alert("324:"+JSON.stringify($scope.objectives));
        $scope.objectivesAndKpi.objectives=$scope.objectives;
        $scope.objectivesAndKpi.kpi=$scope.kpilistS;

        $http({
            method: 'PUT',
            url: 'api/CreateAssetSpares/'+$scope.updateAssetId,
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            data:$scope.objectivesAndKpi
        }).success(function (response) {
            console.log('Users Response :' + JSON.stringify(response));
            console.log("$scope.assetObjectives"+JSON.stringify($scope.assetObjectives));
            $("#editAssetSpares").modal("show");
            setTimeout(function(){$('#editAssetSpares').modal('hide')}, 3000);
            //$window.location.reload();
        }).error(function (response) {
            //console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $http({
        method: 'GET',
        url: 'api/AssetTypes',
        //url: 'api/AssetTypes?filter={"where":{"status":"Active"}}',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        //console.log('Users Response :' + JSON.stringify(response));
        $scope.assetTypeList = response;
    }).error(function (response) {
        //console.log('Error Response :' + JSON.stringify(response));
    });

    //console.log("$scope.assetType1" + $scope.assetType1)

    $http({
        method: 'GET',
        url: 'api/AssetSpares?filter={"where":{"status":"Active"}}',
        headers: {"Content-Type": "application/json", "Accept": "application/json"}
    }).success(function (response) {
        //console.log('Users Response :' + JSON.stringify(response));
        $scope.assetKpi1 = response;
    }).error(function (response) {

    });

    var addCreateObjectivesDoubleClick =true;
    $scope.errorMessage = false;
    $scope.errorMessageData = '';

    $scope.addCreateObjectives = function(){
        if(addCreateObjectivesDoubleClick){
            addCreateObjectivesDoubleClick = false;
            $scope.objectivesAndKpi.assetId=$window.localStorage.getItem('asset');
            $scope.objectivesAndKpi.assetType=$window.localStorage.getItem('assetType');
            if($scope.timeDetails1 != '' && $scope.timeDetails1 != undefined && $scope.timeDetails1 != null){
            console.log("details 1" +JSON.stringify($scope.timeDetails1));
            if($scope.kpiObject.length>0){
            $http({
                            method: 'POST',
                            url: 'api/CreateAssetSpares',
                            headers: {"Content-Type": "application/json", "Accept": "application/json"},
                            data: $scope.objectivesAndKpi
                        }).success(function (response) {
                            //console.log('Users Response :' + JSON.stringify(response));
                            $scope.getAsset($scope.getAssetIdName);
                            $window.localStorage.removeItem('asset');
                            $window.localStorage.removeItem('assetType');
                            $("#addAssetSpares").modal("show");
                            setTimeout(function(){$('#addAssetSpares').modal('hide')}, 3000);
                            //$window.location.reload();
                        }).error(function (response) {

                        });
            }else{
            console.log("detailssss 1" +JSON.stringify($scope.kpiObject.comments));
            console.log("detailssss Array" +JSON.stringify($scope.kpilistS[0]));
            if($scope.kpilistS[0] != '' && $scope.kpilistS[0] != undefined && $scope.kpilistS[0] != null){
            console.log("details 1" +JSON.stringify($scope.kpiObject.comments));
                       $http({
                           method: 'POST',
                           url: 'api/CreateAssetSpares',
                           headers: {"Content-Type": "application/json", "Accept": "application/json"},
                           data: $scope.objectivesAndKpi
                       }).success(function (response) {
                           //console.log('Users Response :' + JSON.stringify(response));
                           $scope.getAsset($scope.getAssetIdName);
                           $window.localStorage.removeItem('asset');
                           $window.localStorage.removeItem('assetType');
                           $("#addAssetSpares").modal("show");
                           setTimeout(function(){$('#addAssetSpares').modal('hide')}, 3000);
                           //$window.location.reload();
                       }).error(function (response) {

                       });
                       }else{
                       $scope.errorMessage = true;
                       $scope.errorMessageData = 'Please Give A Comment And Click The Add Button';
                       $timeout(function(){ $scope.errorMessage=false; }, 3000);
                         }
            }

            }else{
            $scope.errorMessage = true;
            $scope.errorMessageData = 'Please Select To Add Spares/Consumables';
            $timeout(function(){ $scope.errorMessage=false; }, 3000);
            }
        }
        addCreateObjectivesDoubleClick = true;

    }

});

 app.controller('createAssetFailureController', function($scope, $rootScope, $window, $location, $http, Excel, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder) {
     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetFailureAndMaintenance").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetFailureAndMaintenance .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

        $scope.exportToExcel=function(tabelId){
            if( $scope.getAssetTypesData!=null &&  $scope.getAssetTypesData.length>0){
                $("<tr>" +
                    "<th>Failure Classes</th>" +
                    "<th>Date Occurance</th>" +
                    "<th>Problems</th>" +
                    "<th>Causes</th>" +
                    "<th>Remidies</th>" +
                    "</tr>").appendTo("table"+tabelId);
                for(var i=0;i<$scope.getAssetTypesData.length;i++){
                    var getAsset=$scope.getAssetTypesData[i];
                    $("<tr>" +
                        "<td>"+getAsset.failureClasses.name+"</td>" +
                        "<td>"+getAsset.expectedDate+"</td>" +
                        "<td>"+getAsset.problems+"</td>" +
                        "<td>"+getAsset.causes+"</td>" +
                        "<td>"+getAsset.remidies+"</td>" +
                        "</tr>").appendTo("table"+tabelId);
                }
                var exportHref=Excel.tableToExcel(tabelId,'assetSpares');
                $timeout(function(){location.href=exportHref;},100);
            }else{
                $("#emptyDataTable").modal("show");
                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
            }
        }


             $http({
                 method: 'GET',
                 url: 'api/FailureClasses?filter={"where":{"status":"Active"}}',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 //console.log('Users Response :' + JSON.stringify(response));
                 $scope.failureClassList = response;
             }).error(function (response) {
                 //console.log('Error Response :' + JSON.stringify(response));
             });



             $http({
                 method: 'GET',
                 url: 'api/AssetForLands',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 //console.log('Users Response :' + JSON.stringify(response));
                 $scope.assetTypeList = response;
             }).error(function (response) {
                 //console.log('Error Response :' + JSON.stringify(response));
             });

             $scope.getAssetTypes=function(id){
                 $scope.assetId=id;
                 $scope.showAction=true;
                 $scope.selectProjectDetails=true;

                 $http({
                     method: 'GET',
                     url: 'api/CreateAssetFailureClasses?filter={"where":{"assetId":"'+$scope.assetId+'"}}',
                     headers: {"Content-Type": "application/json", "Accept": "application/json"}
                 }).success(function (response) {
                     $scope.getAssetTypesData = response;
                     $("#createProjectMilestone").modal("hide");
                     // alert("227:"+JSON.stringify($scope.getAssetTypesData));
                     console.log('227:' + JSON.stringify(response));
                 }).error(function (response) {
                     console.log('Error Response :' + JSON.stringify(response));
                 });

             }

             $scope.failureResetData = function(){
                 $scope.getAssetFailureData.failureClasses = '';
                 $scope.getAssetFailureData = {};
                 $scope.riskError=false;
             }


              $scope.riskError = false;
              $scope.error = '';
              var assetRiskDataDoubleClick = true;
              $scope.getAssetFailureData = {};
              $scope.assetFailureData = function() {
              $scope.doubleRisk = true;
              $scope.getAssetFailureData.assetId = $scope.assetId;

                 if($scope.getAssetTypesData.length>0){
                    for(i=0; i<$scope.getAssetTypesData.length; i++) {
                    console.log("++++++++++++" +JSON.stringify($scope.getAssetFailureData));
                    if($scope.getAssetFailureData.failureClasses!=undefined){
                      console.log("++++++++++++" +JSON.stringify($scope.getAssetFailureData.expectedDate));
                           if($scope.getAssetFailureData.expectedDate!=undefined) {

                                    if ($scope.getAssetFailureData.failureClasses.name == $scope.getAssetTypesData[i].failureClasses.name) {
                                        $scope.riskError = true;
                                        $scope.error = "This Failure Class is  Already Existed. So,Please Select Another Failure Class";

                                        $scope.doubleRisk = false;
                                        break;
                                    }
                                    $scope.riskName = true;
                                    }else{
                                      console.log("date")
                                      $scope.riskError=true;
                                      $scope.error="Please Select Date";
                                    }
                                    }
                                    else{
                                      console.log("fail class")
                                      $scope.riskError=true;
                                      $scope.error="Please Select Failure Class";
                                    }

                                }
                 }else{
                 $scope.riskName = true;
                 }



                 if($scope.riskName){
                 console.log(assetRiskDataDoubleClick);
                    if($scope.doubleRisk){
                    console.log(assetRiskDataDoubleClick);

                             console.log("json data risks" +JSON.stringify($scope.getAssetFailureData));

                             if($scope.getAssetFailureData.failureClasses!=undefined){
                             console.log(assetRiskDataDoubleClick);

                             if($scope.getAssetFailureData.expectedDate!=undefined)
                               {
                                 console.log("date" +JSON.stringify($scope.getAssetFailureData.expectedDate))
                                 console.log(assetRiskDataDoubleClick);

                                 if(assetRiskDataDoubleClick){
                                 assetRiskDataDoubleClick = false;
                                 console.log(assetRiskDataDoubleClick);
                                 $http({
                                 method: 'POST',
                                 url: 'api/CreateAssetFailureClasses',
                                 //url: 'api/AssetCauses',
                                 headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                 data:$scope.getAssetFailureData
                                 }).success(function (response) {
                                 assetRiskDataDoubleClick = false;
                                 //$window.location.reload();
                                 $("#createProjectMilestone").modal("hide");
                                 $("#addFailureSuccess").modal("show");
                                 setTimeout(function(){$('#addFailureSuccess').modal('hide')}, 3000);
                                 $scope.getAssetTypes($scope.assetId);
                                 console.log('Users Response :' + JSON.stringify(response));
                                 }).error(function (response) {
                                 console.log('Error Response :' + JSON.stringify(response));
                                 });
                                 }
                                 console.log(assetRiskDataDoubleClick);
                                 assetRiskDataDoubleClick = true;

 							 } else
 							 {
 							   $scope.riskError=true;
 							   $scope.error="Please Select Date";
 							 }
 							 } else
                                 {
                                   $scope.riskError=true;
                                   $scope.error="Please Select Failure Class";
                                 }
                                 console.log(assetRiskDataDoubleClick);
                    }

                 }

              }


             $scope.failureDetails = function(failureData) {
                 $("#editProjectMilestone").modal("show");
                 $scope.editFailureData = angular.copy(failureData);
             }

             $scope.editFailureData = {};
             $scope.editFailureDetails = function(){
                 // alert("262")
                 $http({
                     method: 'PUT',
                     url: 'api/CreateAssetFailureClasses?filter={"where":{"assetId":"'+$scope.assetId+'"}}',
                     headers: {"Content-Type": "application/json", "Accept": "application/json"},
                     data:$scope.editFailureData
                 }).success(function (response) {
                     $("#editProjectMilestone").modal("hide");
                     $("#editFailureSuccess").modal("show");
                     setTimeout(function () {$('#editFailureSuccess').modal('hide')}, 3000);
                     $scope.getAssetTypes($scope.assetId);
                     console.log('227:' + JSON.stringify(response));
                 }).error(function (response) {
                     console.log('Error Response :' + JSON.stringify(response));
                 });
             }

    });

 app.controller('createAssetRisksController', function($scope, $rootScope, $window, $location, $http, Excel, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder) {

      $(document).ready(function () {
          $('html,body').scrollTop(0);
          $(".sidebar-menu li ul > li").removeClass('active1');
          $('[data-toggle="tooltip"]').tooltip();
          $(".sidebar-menu #landAssetLi").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
          $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetRisks").addClass("active1").siblings('.active1').removeClass('active1');
          $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetRisks .collapse").addClass("in");
          $(".sidebar-menu li .collapse").removeClass("in");
      });

         $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
             .withOption("sPaginationType", "bootstrap")
             .withOption("retrieve", true)
             .withOption('stateSave', true)
             .withPaginationType('simple_numbers')
             .withOption('order', [0, 'ASC']);

         $scope.exportToExcel=function(tabelId){
             if( $scope.getAssetTypesData!=null &&  $scope.getAssetTypesData.length>0){
                 $("<tr>" +
                     "<th>Risks</th>" +
                     "<th>Assessment</th>" +
                     "<th>Mitigation</th>" +
                     "<th>Plans</th>" +
                     "</tr>").appendTo("table"+tabelId);
                 for(var i=0;i<$scope.getAssetTypesData.length;i++){
                     var getAssetType=$scope.getAssetTypesData[i];
                     $("<tr>" +
                         "<td>"+getAssetType.risks.name+"</td>" +
                         "<td>"+getAssetType.assessment+"</td>" +
                         "<td>"+getAssetType.mitigation+"</td>" +
                         "<td>"+getAssetType.plans+"</td>" +
                         "</tr>").appendTo("table"+tabelId);
                 }
                 var exportHref=Excel.tableToExcel(tabelId,'assetRisks');
                 $timeout(function(){location.href=exportHref;},100);
             }else{
                 $("#emptyDataTable").modal("show");
                 setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
             }
         }

         $(function () {
         $("select[id=dept]").on("invalid", function () {
             this.setCustomValidity("Please Select Department.");
         })});

        $http({
                method: 'GET',
                url: 'api/AssetRisks?filter={"where":{"status":"Active"}}',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                //console.log('Users Response :' + JSON.stringify(response));
                $scope.failureClassList = response;
            }).error(function (response) {
                //console.log('Error Response :' + JSON.stringify(response));
            });

            $http({
                method: 'GET',
                url: 'api/AssetForLands',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                //console.log('Users Response :' + JSON.stringify(response));
                $scope.assetTypeList = response;
            }).error(function (response) {
                //console.log('Error Response :' + JSON.stringify(response));
            });

            $http({
                  method: 'GET',
                  url: 'api/AssetDepartments?filter={"where":{"status":"Active"}}',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"}
                  }).success(function (response) {
                      //console.log('Users Response :' + JSON.stringify(response));
                      $scope.departmentList = response;
                  }).error(function (response) {

                  });


            $scope.getAssetTypes=function(id){
                //alert("203"+id);
                $scope.assetId=id;
                $scope.showAction=true;
                $scope.selectProjectDetails=true;

                $http({
                    method: 'GET',
                    url: 'api/CreateAssetRisks?filter={"where":{"assetId":"'+$scope.assetId+'"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.getAssetTypesData = response;
                    $("#createProjectMilestone").modal("hide");
                    console.log('227:' + JSON.stringify(response));
                }).error(function (response) {
                    console.log('Error Response :' + JSON.stringify(response));
                    if(response.error.details.messages.name) {
                        $scope.errorMessageData=response.error.details.messages.name[0];
                        $scope.errorMessage=true;
                    }
                });

            }

            $scope.riskResetData = function () {
                $scope.getAssetFailureData.risks = '';
                $scope.getAssetFailureData = {};
                $scope.error = '';
            }

            $scope.riskError = false;
            $scope.error = '';

            var assetRiskDataDoubleClick = true;
            $scope.getAssetFailureData = {
            "risks":{
            "name":""
            }};
            console.log("value" +JSON.stringify($scope.getAssetFailureData.risks.name));
            $scope.assetRiskData = function(){
            //console.log("value" +JSON.stringify($scope.getAssetFailureData.risks.name));
                $scope.doubleRisk = true;
                $scope.getAssetFailureData.assetId = $scope.assetId;

                 if($scope.getAssetTypesData.length>0){
                    for(i=0; i<$scope.getAssetTypesData.length; i++) {

                      if($scope.getAssetFailureData.risks!=undefined){



                                    console.log("value" +JSON.stringify($scope.getAssetFailureData.risks));
                                    if ($scope.getAssetFailureData.risks.name == $scope.getAssetTypesData[i].risks.name) {
                                        $scope.riskError = true;
                                        $scope.error = "This Risk Already Added. Please Select Another Risk";

                                        $scope.doubleRisk = false;
                                        break;
                                    }
                                  /*  }*/
                                    else
                                    {
                                       $scope.riskError = true;
                                       $scope.error = "Please Select Asset Risk ";

                                    }
                                    $scope.riskName = true;
                                     /*  }*/
                                }

                                else
                                {
                                  $scope.riskError = true;
                                  $scope.error = "Please Select Asset Risk ";

                                }

                                }



                 }else{
                 $scope.riskName = true;
                 }

                 if($scope.riskName){
                    if($scope.doubleRisk){
                        if(assetRiskDataDoubleClick){
                            assetRiskDataDoubleClick = false;
                            console.log("asset risk" +JSON.stringify($scope.getAssetFailureData.risks))
                            if($scope.getAssetFailureData.risks!=undefined){
                            $http({
                                method: 'POST',
                                url: 'api/CreateAssetRisks',
                                headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                data: $scope.getAssetFailureData
                            }).success(function (response) {
                                console.log('Users Response :' + JSON.stringify(response));
                                $scope.getAssetTypes($scope.assetId);
                                $scope.getAssetFailureData = '';
                                $("#addAssetRisks").modal("show");
                                setTimeout(function () {
                                    $('#addAssetRisks').modal('hide')
                                }, 3000);
                            }).error(function (response) {
                                console.log('Error Response :' + JSON.stringify(response));
                            });
                        }
                        else{
                         $scope.riskError = true;
                         $scope.error = "Please Select Asset Risk";
                        }
                        }
                         assetRiskDataDoubleClick = true;
                    }

                 }

            }


            $scope.failureDetails = function(failureData) {
                $("#editProjectMilestone").modal("show");
                $scope.tseklist=failureData.risks.name;
                console.log("popup data of risk name" +JSON.stringify($scope.tseklist));
                $scope.editFailureData = angular.copy(failureData);
                $scope.error='';
            }

            $scope.editFailureData = {};
            $scope.editRiskDetails = function(){
            $scope.riskError=false;
            $scope.error='';

            console.log(".................................. "+JSON.stringify($scope.editFailureData.risks.name))
            $scope.editFailureData['riskName']=$scope.editFailureData.risks.id;
                $http({
                    method: 'PUT',
                    url: 'api/CreateAssetRisks?filter={"where":{"assetId":"'+$scope.assetId+'"}}',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"},
                    data:$scope.editFailureData
                }).success(function (response) {
                    $("#editProjectMilestone").modal("hide");
                    //alert('22777777777777777777777777:' + JSON.stringify(response));
                    $scope.getAssetTypes($scope.assetId);
                    $("#editAssetRisks").modal("show");
                    setTimeout(function(){$('#editAssetRisks').modal('hide')}, 3000);
                }).error(function (response) {
                    //alert('Error Responseaaaaaaaaaa :' + JSON.stringify(response));
                    console.log('Error Responseaaaaaaaaaa :' + JSON.stringify(response));
                });

            }



    });

 app.controller('createAssetCausesController', function($scope, $rootScope, $window, $location, $http, Excel, Upload, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #landAssetPro.active #landProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetCauses").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #landAssetPro.active #landProSetUp li.createAssetCauses .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });
        $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
            .withOption("sPaginationType", "bootstrap")
            .withOption("retrieve", true)
            .withOption('stateSave', true)
            .withPaginationType('simple_numbers')
            .withOption('order', [0, 'ASC']);

        $scope.exportToExcel=function(tabelId){
            if( $scope.getAssetTypesData!=null &&  $scope.getAssetTypesData.length>0){
                $("<tr>" +
                    "<th>Asset Description</th>" +
                    "<th>Asset Cause</th>" +
                    "<th>Additional Details</th>" +
                    "</tr>").appendTo("table"+tabelId);
                for(var i=0;i<$scope.getAssetTypesData.length;i++){
                    var getAssetType=$scope.getAssetTypesData[i];
                    $("<tr>" +
                        "<td>"+getAssetType.description+"</td>" +
                        "<td>"+getAssetType.assetCause+"</td>" +
                        "<td>"+getAssetType.extraDetails+"</td>" +
                        "</tr>").appendTo("table"+tabelId);
                }
                var exportHref=Excel.tableToExcel(tabelId,'assetRisks');
                $timeout(function(){location.href=exportHref;},100);
            }else{
                $("#emptyDataTable").modal("show");
                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
            }
        }


             $http({
                 method: 'GET',
                 url: 'api/AssetForLands',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 //console.log('Users Response :' + JSON.stringify(response));
                 $scope.assetTypeList = response;
             }).error(function (response) {
                 //console.log('Error Response :' + JSON.stringify(response));
             });

             $scope.getAssetTypes=function(id){
                 //alert("203"+id);
                 $scope.assetId=id;
                 $scope.showAction=true;
                 $scope.selectProjectDetails=true;

                 $http({
                     method: 'GET',
                     url: 'api/AssetCauses?filter={"where":{"assetId":"'+$scope.assetId+'"}}',
                     headers: {"Content-Type": "application/json", "Accept": "application/json"}
                 }).success(function (response) {
                     $scope.getAssetTypesData = response;
                     $("#createProjectMilestone").modal("hide");
                     // alert("227:"+JSON.stringify($scope.getAssetTypesData));
                     console.log('227:' + JSON.stringify(response));
                 }).error(function (response) {
                     console.log('Error Response :' + JSON.stringify(response));
                 });

             }

             $scope.causeResetData = function () {
                 $scope.getAssetFailureData = {};
                 $scope.riskError = false;
             }
             $scope.riskError = false;
             $scope.error = '';
             var assetRiskDataDoubleClick = true;
             $scope.getAssetFailureData = {};
             $scope.assetRiskData = function(){
                 $scope.doubleRisk = true;
                 $scope.getAssetFailureData.assetId = $scope.assetId;
                  console.log("222:"+JSON.stringify($scope.getAssetFailureData));
                  if($scope.getAssetTypesData.length>0){
                  for(var i=0;i<$scope.getAssetTypesData.length;i++)
                  {

                  if($scope.getAssetFailureData.assetCause==$scope.getAssetTypesData[i].assetCause)
                  {
                    //alert("asset cause is existed")
                    $scope.riskError = true;
                    $scope.error = "Asset Cause is  Already Existed. Please Select Another Asset Cause";
                    $scope.doubleRisk = false;
                    break;
                  }
                   $scope.riskName = true;
                  }
                  }else{
                      $scope.riskName = true;
                  }
                   if($scope.riskName){
                         if($scope.doubleRisk){
                           if(assetRiskDataDoubleClick){
                             assetRiskDataDoubleClick = false;
                                $http({
                                     method: 'POST',
                                     //url: 'api/CreateAssetFailureClasses',
                                     url: 'api/AssetCauses',
                                     headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                     data:$scope.getAssetFailureData
                                 }).success(function (response) {
                                     assetRiskDataDoubleClick = false;
                                     //$window.location.reload();
                                     $scope.getAssetTypes($scope.assetId);
                                     console.log('Users Response :' + JSON.stringify(response));
                                 }).error(function (response) {
                                     console.log('Error Response :' + JSON.stringify(response));
                                 });
                                 }
                                 assetRiskDataDoubleClick = true;
                             }
                         }
             }


             $scope.failureDetails = function(failureData) {
                 $("#editProjectMilestone").modal("show");
                 $scope.editFailureData = angular.copy(failureData);
                 $scope.riskError=false;
             }
             var assetRiskDataDoubleClick = true;
             $scope.editFailureData = {};
             $scope.editRiskDetails = function(){

                      if(assetRiskDataDoubleClick){
                      assetRiskDataDoubleClick = false;
                     $http({
                         method: 'PUT',
                         /*url: 'api/CreateAssetFailureClasses',*/
                         url: 'api/AssetCauses?filter={"where":{"assetId":"'+$scope.assetId+'"}}',
                         headers: {"Content-Type": "application/json", "Accept": "application/json"},
                         data:$scope.editFailureData
                     }).success(function (response) {
                         $("#editProjectMilestone").modal("hide");
                         assetRiskDataDoubleClick = false;
                         $scope.getAssetTypes($scope.assetId);
                         console.log('227:' + JSON.stringify(response));
                     }).error(function (response) {
                         console.log('Error Response :' + JSON.stringify(response));
                     });
               }
               assetRiskDataDoubleClick = true;

       }
    });

 app.controller('purchageAgendaRequestController', function($scope, $rootScope, $window, $location, $http) {

     console.log("purchageAgendaRequestController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #vehicalPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #vehicalPro.active #vehicleProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.purchaseRequests").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.purchaseRequests .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

      $scope.generatePDF = function() {
              var element = document.getElementById("elementId");
              element.parentNode.removeChild(element);
              var printContents = document.getElementById('formConfirmation').innerHTML;
              var originalContents = document.body.innerHTML;

              document.body.innerHTML = printContents;

              window.print();

              document.body.innerHTML = originalContents;
              /* kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
                 kendo.drawing.pdf.saveAs(group, "Converted PDF.docx");});*/

             }

     $scope.loadingImage=true;
     $scope.uploadURL='api/Uploads/dhanbadDb/download/';
     $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
     $scope.viewRequestDiv=true;
     $scope.viewRequestDivBack=true;
     $scope.requestDivBack=false;
     $scope.requestDiv=false;
     $scope.editRequestDivBack=false;
     $scope.editRequestDiv=false;
     var employeeId=$scope.userInfo.employeeId;
     var employeeName=$scope.userInfo.name;
     $scope.getRequest=function () {

         $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
         var employeeId=$scope.userInfo.employeeId;
         var employeeName=$scope.userInfo.name;
         //alert("employee details:"  +$scope.userInfo.employeeId+ " " +$scope.userInfo.name);
         $http({
             method: 'GET',
             url: 'api/VehiclePurchaseRequests/getDetails?employeeId='+employeeId ,
             headers: {"Content-Type": "application/json", "Accept": "application/json"}
         }).success(function (response) {
             console.log('getFieldVisitReport :' + JSON.stringify(response));
             $scope.requestLists = response;
             $scope.loadingImage=false;
         }).error(function (response) {
             console.log('Error Response :' + JSON.stringify(response));
         });

     }
     $scope.getRequest();
     $scope.changeStatus=function (id) {
         if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
             for(var i=0;i<$scope.requestLists.length;i++){
                 if(id==$scope.requestLists[i].id){
                     $scope.editRequestDetails=angular.copy($scope.requestLists[i]) ;
                     $scope.viewRequestDiv=false;
                     $scope.viewRequestDivBack=false;
                     $scope.editRequestDivBack=true;
                     $scope.editRequestDiv=true;
                     $scope.requestDivBack=false;
                     $scope.requestDiv=false;
                 }
             }
         }
     }
     $scope.viewStatus=function (id) {
         if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
             for(var i=0;i<$scope.requestLists.length;i++){
                 if(id==$scope.requestLists[i].id){
                     $scope.viewRequestDetails=angular.copy($scope.requestLists[i]) ;
                     $scope.viewRequestDiv=false;
                     $scope.viewRequestDivBack=false;
                     $scope.editRequestDivBack=false;
                     $scope.editRequestDiv=false;
                     $scope.requestDivBack=true;
                     $scope.requestDiv=true;
                 }
             }
         }
     }
     $scope.clickToBack = function(){
         $window.location.reload();
     }

     $scope.acceptStatus = function (request) {

         var updateDetails = {
             'email':request.email,
             "acceptLevel":request.acceptLevel,
             "acceptStatus": "Yes",
             "requestStatus": "Approval",
             "comment": request.comment,
             "requestId":request.id,
             "employeeId":employeeId,
             "employeeName":employeeName
         };

         $scope.approvalScheme = true;

         $http({
             "method": "POST",
             "url": 'api/VehiclePurchaseRequests/updateDetails',
             "headers": {"Content-Type": "application/json", "Accept": "application/json"},
             "data": updateDetails
         }).success(function (response, data) {
             //console.log("filter Schemes "+ JSON.stringify(response));
             $scope.requestLists = response;


             $window.localStorage.setItem('requestFlag',true);
             $window.location.reload();
             //$window.localStorage.getItem('requestTab');

         }).error(function (response, data) {
             console.log("failure");
         })
     }

     $scope.rejectStatus = function (request) {

         var updateDetails = {
             'email':request.email,
             "acceptLevel":request.acceptLevel,
             "acceptStatus": "No",
             "requestStatus": "Rejected",
             "comment": request.comment,
             "requestId":request.id,
             "employeeId":employeeId,
             "employeeName":employeeName
         };
         $http({
             "method": "POST",
             "url": 'api/VehiclePurchaseRequests/updateDetails',
             "headers": {"Content-Type": "application/json", "Accept": "application/json"},
             "data": updateDetails
         }).success(function (response, data) {
             //console.log("filter Schemes "+ JSON.stringify(response));
             $scope.requestLists = response;


             $window.localStorage.setItem('requestFlag',true);
             $window.location.reload();
             //$window.localStorage.getItem('requestTab');

         }).error(function (response, data) {
             console.log("failure");
         })


     }

     $scope.exportToExcel=function(tableId){ // ex: '#my-table'
         var exportHref=Excel.tableToExcel(tableId,'sheet name');
         $timeout(function(){location.href=exportHref;},100); // trigger download
     }
     $scope.viewRequest = function() {
         $scope.loadingImage=false;
         location.href = '#/viewRequest';
     }
     $scope.sort = function(keyname){
         $scope.sortKey = keyname;   //set the sortKey to the param passed
         $scope.reverse = !$scope.reverse; //if true make it false and vice versa
     }


 });
  app.controller('purchageAgendaRequestReportController', function($scope, $rootScope, $window, $location, $http) {

      console.log("purchageAgendaRequestReportController");

      $(document).ready(function () {
          $('html,body').scrollTop(0);
          $(".sidebar-menu li ul > li").removeClass('active1');
          $('[data-toggle="tooltip"]').tooltip();
          $(".sidebar-menu #landAssetLi").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #vehicalPro").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #vehicalPro.active #vehicleProSetUp").addClass("menu-open");
          $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.purchaseReports").addClass("active1").siblings('.active1').removeClass('active1');
          $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.purchaseReports .collapse").addClass("in");
          $(".sidebar-menu li .collapse").removeClass("in");
      });

      $scope.generatePDF = function() {
                    var element = document.getElementById("elementId");
                    element.parentNode.removeChild(element);
                    var printContents = document.getElementById('formConfirmation').innerHTML;
                    var originalContents = document.body.innerHTML;

                    document.body.innerHTML = printContents;

                    window.print();

                    document.body.innerHTML = originalContents;
                    /* kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
                       kendo.drawing.pdf.saveAs(group, "Converted PDF.docx");});*/

                   }

      $scope.loadingImage=true;
      $scope.uploadURL='api/Uploads/dhanbadDb/download/';
      $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
      $scope.viewRequestDiv=true;
      $scope.viewRequestDivBack=true;
      $scope.requestDivBack=false;
      $scope.requestDiv=false;
      $scope.editRequestDivBack=false;
      $scope.editRequestDiv=false;
      var employeeId=$scope.userInfo.employeeId;
      var employeeName=$scope.userInfo.name;
      $scope.getRequest=function () {

          $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
          var employeeId=$scope.userInfo.employeeId;
          var employeeName=$scope.userInfo.name;
          $http({
              method: 'GET',
              url: 'api/VehiclePurchaseRequests?filter=%7B%22where%22%3A%7B%22or%22%3A%5B%7B%22finalStatus%22%3A%20%22Approved%22%7D%2C%7B%22finalStatus%22%3A%20%22Rejected%22%7D%5D%7D%7D' ,
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('getFieldVisitReport :' + JSON.stringify(response));
              $scope.requestLists = response;
              $scope.loadingImage=false;
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });

      }
      $scope.getRequest();

      $scope.viewStatus=function (id) {
          if($scope.requestLists!=undefined && $scope.requestLists!=null && $scope.requestLists.length>0){
              for(var i=0;i<$scope.requestLists.length;i++){
                  if(id==$scope.requestLists[i].id){
                      $scope.viewRequestDetails=angular.copy($scope.requestLists[i]) ;
                      $scope.viewRequestDiv=false;
                      $scope.viewRequestDivBack=false;
                      $scope.editRequestDivBack=false;
                      $scope.editRequestDiv=false;
                      $scope.requestDivBack=true;
                      $scope.requestDiv=true;
                  }
              }
          }
      }
      $scope.clickToBack = function(){
      $window.location.reload();
      }

      $scope.exportToExcel=function(tableId){ // ex: '#my-table'
          var exportHref=Excel.tableToExcel(tableId,'sheet name');
          $timeout(function(){location.href=exportHref;},100); // trigger download
      }

      $scope.sort = function(keyname){
          $scope.sortKey = keyname;   //set the sortKey to the param passed
          $scope.reverse = !$scope.reverse; //if true make it false and vice versa
      }
  });

 app.controller('purchageAgendaController', function(Excel, $scope, $rootScope, $window, $location, $http,Upload,$timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #vehicalPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #vehicalPro.active #vehicleProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.purchaseAgenda").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.purchaseAgenda .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });

      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.charterList!=null &&  $scope.charterList.length>0){
              $("<tr>" +
                  "<th>Purchage Agenda ID</th>" +
                  "<th>Meeting Date</th>" +
                  "<th>Department Name</th>" +
                  "<th>Prepared By</th>" +
                  "<th>Reviewed By</th>" +
                  "<th>BackGround</th>" +
                  "<th>Discussion</th>" +
                  "<th>Fical Impact</th>" +
                  "<th>Recommandation</th>" +
                  "<th>Visibility</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.charterList.length;i++){
                  var charter=$scope.charterList[i];
                  $("<tr>" +
                      "<td>"+charter.purchaseAgendaNumber+"</td>" +
                      "<td>"+charter.meetingDate+"</td>" +
                      "<td>"+charter.departmentName+"</td>" +
                      "<td>"+charter.preparedBy+"</td>" +
                      "<td>"+charter.reviewedBy+"</td>" +
                      "<td>"+charter.backGround+"</td>" +
                      "<td>"+charter.discussion+"</td>" +
                      "<td>"+charter.ficalImpact+"</td>" +
                      "<td>"+charter.recommandation+"</td>" +
                      "<td>"+charter.visibility+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'purchageAgenda');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $http({
          method: 'GET',
          url: 'api/AssetDepartments?filter={"where":{"status":"Active"}}',
          headers: {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {
          //console.log('Users Response :' + JSON.stringify(response));
          $scope.departmentList = response;
      }).error(function (response) {

      });


      $scope.showCharter = function() {
          $scope.createCharterArea = true;
          $scope.editCharterArea = false;
      }
      $scope.editCharterButton = function(charter) {
          $("#editCharter").modal("show");
          $scope.editCharterArea = true;
          $scope.createCharterArea = false;
          $scope.editCharterData=angular.copy(charter);
      }

      //resolution copy upload start


      var filedetails = [];
      var fileIdsArray = [];
      var fileUploadStatus = true;
      $scope.files = [];
      $scope.uploadURL = 'api/Uploads/dhanbadDb/download/';
      $scope.uploadFiles = function (files) {

          $scope.disable = true;
          $scope.errorMssg = true;
          var fileCount = 0;
          $scope.fileUploadLists = true;
          fileIdsArray = [];
          fileUploadStatus = false;
          $scope.files = [];
          angular.forEach(files, function (file) {
              fileCount++;
              file.upload = Upload.upload({
                  url: 'api/Uploads/dhanbadDb/upload',
                  data: {file: file}
              });

              file.upload.then(function (response) {
                  console.log(JSON.stringify(response.data.metadata.mimetype));
                  if(response.data.metadata.mimetype != 'image/jpeg' && response.data.metadata.mimetype != 'application/pdf'){
                      $http({
                          method: 'DELETE',
                          url: 'api/Uploads/dhanbadDb/files/'+response.data._id,
                          headers: {"Content-Type": "application/json", "Accept": "application/json"}
                      }).success(function (response) {

                      });
                      alert('Please Upload JPEG or PDF files only');
                  }else{
                      $timeout(function () {
                          var fileDetails = {
                              'id': response.data._id,
                              'name': response.data.filename
                          };
                          fileIdsArray.push(fileDetails);
                          filedetails.push(response.data);
                          $scope.files.push(fileDetails);
                          fileUploadStatus = true;
                          //console.log('details are' + JSON.stringify(fileIdsArray));
                          file.result = response.data;
                      });
                  }

                  if(fileCount == files.length){
                      $scope.fileUploadLists = true;
                      $scope.disable = false;
                      $scope.errorMssg = false;
                  }
              }, function (response) {
                  if (response.status > 0)
                      $scope.errorMsg = response.status + ': ' + response.data;
              }, function (evt) {
                  file.progress = Math.min(100, parseInt(100.0 *
                      evt.loaded / evt.total));
              });
          });


      };

      $scope.deleteFile = function(index, fileId){

          $http({
              method: 'DELETE',
              url: 'api/Uploads/dhanbadDb/files/'+fileId,
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              $scope.files.splice(index, 1);
          });

      };

      $scope.actClick=function(){
          $('#createCharter').modal('show');
          $scope.charter = {};
          $scope.files = [];
          //$scope.actCreationError = false;
          //$scope.createActError = '';
      }

      //resolution copy upload end

      $scope.createAssignDriver=function () {

          if (fileUploadStatus) {
              var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
              var charterDetails = $scope.charter;
              var departmentName=$scope.charter.departmentNamee.name;
              charterDetails['departmentName']=departmentName;

              console.log('login person details' + loginPersonDetails.name);
              charterDetails['createdPerson'] = loginPersonDetails.name;
              charterDetails['employeeId']=loginPersonDetails.employeeId;
              charterDetails.files = $scope.files;
              //charterDetails.file = $scope.docList;
              console.log('HARI Response :' + JSON.stringify(charterDetails));
              $http({
                  method: 'POST',
                  url: 'api/PurchageAgendas',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"},
                  "data": charterDetails
              }).success(function (response) {
                  console.log('Users Response :' + JSON.stringify(response));
                  //  //alert('successfully created charter');
                  //$scope.charter = {};
                  $scope.getPurchageAgendaDetails();
                  $("#createCharter").modal("hide");
              }).error(function (response) {
                  console.log('Error Response :' + JSON.stringify(response));
              });
          }

      }

      $scope.editAssignDriver=function () {
          var editCharterDetails= $scope.editCharterData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editCharterDetails['lastEditPerson']=loginPersonDetails.name;
          var departmentName=$scope.editCharterData.departmentNamee.name;
          editCharterDetails['departmentName']=departmentName;
          $http({
              method: 'PUT',
              url :'api/PurchageAgendas/'+$scope.editCharterData.id,
              headers: {"Content-Type": "application/json", "Accept": "application/json"},
              "data":editCharterDetails
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              ////alert('successfully edit charter');
              $scope.getPurchageAgendaDetails();
              $("#editCharter").modal("hide");
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });
      }

      $scope.getPurchageAgendaDetails=function () {
          $http({
              method: 'GET',
              url :'api/PurchageAgendas',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('Users Response :' + JSON.stringify(response));
              $scope.charterList=response;
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });

      }
      $scope.getPurchageAgendaDetails();

  });

 app.controller('assignDriverController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

      $(document).ready(function () {
          $('html,body').scrollTop(0);
          $(".sidebar-menu li ul > li").removeClass('active1');
          $('[data-toggle="tooltip"]').tooltip();
          $(".sidebar-menu #landAssetLi").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #vehicalPro").addClass("active");
          $(".sidebar-menu #landAssetLi.treeview #vehicalPro.active #vehicleProSetUp").addClass("menu-open");
          $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.assignDriver").addClass("active1").siblings('.active1').removeClass('active1');
          $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.assignDriver .collapse").addClass("in");
          $(".sidebar-menu li .collapse").removeClass("in");
      });

       $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
           .withOption("sPaginationType", "bootstrap")
           .withOption("retrieve", true)
           .withOption('stateSave', true)
           .withPaginationType('simple_numbers')
           .withOption('order', [0, 'ASC']);

       $scope.exportToExcel=function(tabelId){
           if( $scope.getAssetTypesData!=null &&  $scope.getAssetTypesData.length>0){
               $("<tr>" +
                   "<th>Department</th>" +
                   "<th>Designation</th>" +
                   "<th>Driver Name</th>" +
                   "<th>Effective Date</th>" +
                   "<th>Instructions/Comments</th>" +
                   "</tr>").appendTo("table"+tabelId);
               for(var i=0;i<$scope.getAssetTypesData.length;i++){
                   var charter=$scope.getAssetTypesData[i];
                   $("<tr>" +
                       "<td>"+charter.department.name+"</td>" +
                       "<td>"+charter.designation+"</td>" +
                       "<td>"+charter.driverName+"</td>" +
                       "<td>"+charter.effectiveDate+"</td>" +
                       "<td>"+charter.comments+"</td>" +
                       "</tr>").appendTo("table"+tabelId);
               }
               var exportHref=Excel.tableToExcel(tabelId,'assignDriver');
               $timeout(function(){location.href=exportHref;},100);
           }else{
               $("#emptyDataTable").modal("show");
               setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
           }
       }
             var count=0;
       $scope.deptWiseEmp=function(x){
       //console.log("------------------------------------------------------------ ",x)
       $http({
                 method: 'GET',
                 url: 'api/Employees?filter={"where":{"department":"'+x+'"}}',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 //console.log('Users Response :' + JSON.stringify(response));
                 $scope.employeeList = response;
                 count=0;
                 console.log("employee data" +JSON.stringify($scope.employeeList))
             }).error(function (response) {
                 //console.log('Error Response :' + JSON.stringify(response));
             });

       }



       $http({
           method: 'GET',
           url: 'api/EmployeeDepartments?filter={"where":{"status":"Active"}}',
           headers: {"Content-Type": "application/json", "Accept": "application/json"}
       }).success(function (response) {
           console.log('Users Response :' + JSON.stringify(response));
           $scope.departmentList = response;
       }).error(function (response) {
           //console.log('Error Response :' + JSON.stringify(response));
       });

       $http({
           method: 'GET',
          // url: 'api/AssetForLands',
           url: 'api/AssetForLands?filter={"where":{"assetType":"Movable"}}',
           headers: {"Content-Type": "application/json", "Accept": "application/json"}
       }).success(function (response) {
           //console.log('Users Response :' + JSON.stringify(response));
           $scope.assetTypeList = response;
       }).error(function (response) {
           //console.log('Error Response :' + JSON.stringify(response));
       });

       $scope.getAssignDrivers=function(id){
           $scope.assetId=id;
           $scope.showAction=true;
           $scope.selectProjectDetails=true;

           $http({
               method: 'GET',
               url: 'api/AssignDrivers?filter={"where":{"assetId":"'+$scope.assetId+'"}}',
               headers: {"Content-Type": "application/json", "Accept": "application/json"}
           }).success(function (response) {
               $scope.getAssetTypesData = response;
               $("#createProjectMilestone").modal("hide");
               console.log('227:' + JSON.stringify(response));
           }).error(function (response) {
               console.log('Error Response :' + JSON.stringify(response));
           });

       }
         $scope.error='';
       $scope.errorMsg=false;

           $scope.driverChange = function(x){
             console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxx: ",x);
 //            debugger;

             if($scope.getAssetTypesData){
             for(var i=0;i<$scope.getAssetTypesData.length;i++){
             if(x.employeeId==$scope.getAssetTypesData[i].employeeId){

             count=count+1;}
             }
             }
             if(count>0){
                       $scope.error="Selected Driver Already Assigned to other Asset";
                       $scope.errorMsg=true;
                       $timeout(function(){ $scope.errorMsg=false; }, 3000);
             }else{
                   $scope.error='';
                   $scope.errorMsg=false;
             }
              console.log(JSON.stringify(count));
             }

        $scope.removeData=function()
                {
                $scope.getAssetFailureData = {};
                $scope.getAssetFailureData.department="";
                $scope.getAssetFailureData.designation="";
                $scope.error='';
                }


       $scope.getAssetFailureData = {};


       var assignDriverSubmit = true;
       $scope.addAssignDriverDetails = function(){
           $scope.getAssetFailureData.assetId = $scope.assetId;
           console.log("dept" +JSON.stringify($scope.getAssetFailureData.department));
            if($scope.getAssetFailureData.department != ""){
                 if($scope.getAssetFailureData.driverName != undefined){
                     if($scope.getAssetFailureData.effectiveDate != undefined){
                     if(assignDriverSubmit) {
                     assignDriverSubmit = false;
 //                    debugger;
                     $scope.getAssetFailureData['departmentName']= $scope.getAssetFailureData.department.name;
                     $scope.getAssetFailureData['employeeId']= $scope.getAssetFailureData.driverName.employeeId;
                      console.log("$scope.getAssetFailureData:================== "+JSON.stringify($scope.getAssetFailureData));
                      if(count==0){
                       $http({
                      method: 'POST',
                      url: 'api/AssignDrivers',
                      headers: {"Content-Type": "application/json", "Accept": "application/json"},
                      data:$scope.getAssetFailureData
                  }).success(function (response) {
                      console.log('Users Response :' + JSON.stringify(response));
                      $scope.getAssignDrivers($scope.assetId);
                      $scope.getAssetFailureData = '';
                       assignDriverSubmit = true;
                  }).error(function (response) {
                      console.log('Error Response :' + JSON.stringify(response));
                  });
                      }else{
                             $scope.error="Selected Driver Already Assigned to other Asset";
                            $scope.errorMsg=true;
                            $timeout(function(){ $scope.errorMsg=false; }, 3000);
                      }

                     }
                     assignDriverSubmit = true;
                     }else{
                         //alert("Please Select Effective Date");
                         $scope.error="Please Select Effective Date";
                         $scope.errorMsg=true;
                         $timeout(function(){ $scope.errorMsg=false; }, 3000);
                     }

                 }else{
                     //alert("Please Select Driver Name");
                     $scope.error="Please Select Driver Name";
                     $scope.errorMsg=true;
                     $timeout(function(){ $scope.errorMsg=false; }, 3000);
                 }

            }else{
                //alert("Please Select Department");
                $scope.error="Please Select Department";
                $scope.errorMsg=true;
                $timeout(function(){ $scope.errorMsg=false; }, 3000);
            }
       }

       $scope.failureDetails = function(failureData) {
           $("#editProjectMilestone").modal("show");
           $scope.editFailureData = angular.copy(failureData);
       }

       $scope.editFailureData = {};
       $scope.editAssignDriverDetails = function(){
           $http({
               method: 'PUT',
               /*url: 'api/CreateAssetFailureClasses',*/
               url: 'api/AssignDrivers?filter={"where":{"assetId":"'+$scope.assetId+'"}}',
               headers: {"Content-Type": "application/json", "Accept": "application/json"},
               data:$scope.editFailureData
           }).success(function (response) {
               $("#editProjectMilestone").modal("hide");
               $scope.getAssignDrivers($scope.assetId);
               //$window.location.reload();
               // alert("227:"+JSON.stringify($scope.getAssetTypesData));
               console.log('227:' + JSON.stringify(response));
           }).error(function (response) {
               console.log('Error Response :' + JSON.stringify(response));
           });
       }

       // $scope.getAssignDrivers();
   });

 app.controller('logBookController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #vehicalPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #vehicalPro.active #vehicleProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.logBook").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.logBook .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.logBookList!=null &&  $scope.logBookList.length>0){
              $("<tr>" +
                  "<th>Date</th>" +
                  "<th>From Place</th>" +
                  "<th>To Place</th>" +
                  "<th>Distance</th>" +
                  "<th>Accomplished By</th>" +
                  "<th>Purpose</th>" +
                  "<th>Comments</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.logBookList.length;i++){
                  var logBook=$scope.logBookList[i];
                  $("<tr>" +
                      "<td>"+logBook.date+"</td>" +
                      "<td>"+logBook.fromPlace+"</td>" +
                      "<td>"+logBook.toPlace+"</td>" +
                      "<td>"+logBook.distance+"</td>" +
                      "<td>"+logBook.accomplishedBy+"</td>" +
                      "<td>"+logBook.purpose+"</td>" +
                      "<td>"+logBook.comments+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'logBook');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.addButtonShow=false;


      $http({
          method: 'GET',
          url :'api/EmployeeDepartments?filter={"where":{"status":"Active"}}',
          headers: {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {
          //console.log('Users Response :' + JSON.stringify(response));
          $scope.employeeListType=response;
      }).error(function (response) {
          console.log('Error Response :' + JSON.stringify(response));
      });


      $scope.getEmployeeDepartment = function(department) {
          $scope.departmentName=department;
          $scope.employeeDesignation = '';

          console.log("jndkj"+department)
          $http({
              method: 'GET',
              url :'api/Employees?filter={"where":{"department":"'+$scope.departmentName+'"}}',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log("response"+JSON.stringify(response))
              $scope.getDetailsEmp=response;
              console.log("4817:"+JSON.stringify($scope.getDetailsEmp));
              $window.localStorage.setItem('employee',JSON.stringify($scope.getDetailsEmp));
              $window.localStorage.setItem('departmentName',$scope.departmentName);
              console.log("315:"+JSON.stringify(response));
              $scope.getLogBookDetails();
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });
      }

      $scope.showCharter = function() {
          $scope.createCharterArea = true;
          $scope.editCharterArea = false;
      }
      $scope.editCharterButton = function(charter) {
          $("#editCharter").modal("show");
          $scope.editCharterArea = true;
          $scope.createCharterArea = false;
          $scope.editCharterData=angular.copy(charter);
      }

      $scope.selectedEmployee = function(employee){
      $scope.addButtonShow=true;
          //alert("hhhhh :"+JSON.stringify(employee));
          //var selectedList = JSON.parse($scope.getDetails.employee);
          var selectedList = employee;
          $scope.employeeDesignation = selectedList.designation;
          $scope.filterByEmployee = selectedList.firstName;
          $scope.filterByEmployeeId = selectedList.id;
          $scope.filterByDepartment = selectedList.department;
          $http({
              method: 'GET',
              url :'api/LogBooks?filter={"where":{"employeeName":"'+$scope.filterByEmployee+'","departmentName":"'+$scope.filterByDepartment+'"}}',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('Users Response :' + JSON.stringify(response));
              $scope.logBookList=response;
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });


      }

      $scope.removeData=function()
      {

        $scope.charter={};
        $scope.logbookError=false;

      }

      $scope.logbookMsg='';
      $scope.logbookError=false;

      $scope.createLogBook=function () {

          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          //alert("sgg"+JSON.stringify(loginPersonDetails));
          var charterDetails=$scope.charter;
          console.log('login person details'+loginPersonDetails.department);
         // charterDetails['employeeName']=loginPersonDetails.firstName;
          charterDetails['employeeName']= $scope.filterByEmployee;
          //charterDetails['designation']=loginPersonDetails.designation;
          charterDetails['designation']=$scope.employeeDesignation;
        //  charterDetails['employeeId']=loginPersonDetails.employeeId;
          charterDetails['employeeId']=$scope.filterByEmployeeId;
          charterDetails['departmentName']=loginPersonDetails.department;
          charterDetails['departmentName']=$window.localStorage.getItem('departmentName');
          console.log('..................'+JSON.stringify(charterDetails));

          if($scope.charter.date!=undefined){
          $http({
              method: 'POST',
              url :'api/LogBooks',
              headers: {"Content-Type": "application/json", "Accept": "application/json"},
              "data":charterDetails
          }).success(function (response) {
              console.log('Log Response:'+JSON.stringify(response))
              $scope.charter = {};
              $scope.getLogBookDetails();
              $("#createCharter").modal("hide");
              $("#addLogBookSuccess").modal("show");
              setTimeout(function(){$('#addLogBookSuccess').modal('hide')}, 3000);
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });
          }
          else {
           $scope.logbookMsg="Please Select Date";
           $scope.logbookError=true;
          }

      }

      $scope.editLogBook=function () {
          var editCharterDetails= $scope.editCharterData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editCharterDetails['lastEditPerson']=loginPersonDetails.name;
          $http({
              method: 'PUT',
              url :'api/LogBooks/'+$scope.editCharterData.id,
              headers: {"Content-Type": "application/json", "Accept": "application/json"},
              "data":editCharterDetails
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.getLogBookDetails();
              $scope.selectedEmployee();
              $("#editCharter").modal("hide");
              $("#editFailureSuccess").modal("show");
              setTimeout(function(){$('#editFailureSuccess').modal('hide')}, 3000);
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });
      }

      $scope.getLogBookDetails=function () {
          $http({
              method: 'GET',
              url :'api/LogBooks?filter={"where":{"departmentName":"'+$scope.departmentName+'"}}',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              console.log('Users Response :' + JSON.stringify(response));
              $scope.logBookList=response;
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });

      }


  });

app.controller('submitBillsController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
 var datavalue;
     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".sidebar-menu li ul > li").removeClass('active1');
         $('[data-toggle="tooltip"]').tooltip();
         $(".sidebar-menu #landAssetLi").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #vehicalPro").addClass("active");
         $(".sidebar-menu #landAssetLi.treeview #vehicalPro.active #vehicleProSetUp").addClass("menu-open");
         $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.submitBills").addClass("active1").siblings('.active1').removeClass('active1');
         $(".sidebar-menu #landAssets #vehicalPro.active #vehicleProSetUp li.submitBills .collapse").addClass("in");
         $(".sidebar-menu li .collapse").removeClass("in");
     });


      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

      $scope.exportToExcel=function(tabelId){
          if( $scope.submitBillsList!=null &&  $scope.submitBillsList.length>0){
              $("<tr>" +
                  "<th>Bill Date</th>" +
                  "<th>Bill Number</th>" +
                  "<th>Amount</th>" +
                  "<th>Purpose</th>" +
                  "<th>Reason</th>" +
                  "</tr>").appendTo("table"+tabelId);
              for(var i=0;i<$scope.submitBillsList.length;i++){
                  var submitBills=$scope.submitBillsList[i];
                  $("<tr>" +
                      "<td>"+submitBills.billDate+"</td>" +
                      "<td>"+submitBills.billNumber+"</td>" +
                      "<td>"+submitBills.amount+"</td>" +
                      "<td>"+submitBills.purpose+"</td>" +
                      "<td>"+submitBills.reason+"</td>" +
                      "</tr>").appendTo("table"+tabelId);
              }
              var exportHref=Excel.tableToExcel(tabelId,'submitBills');
              $timeout(function(){location.href=exportHref;},100);
          }else{
              $("#emptyDataTable").modal("show");
              setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
          }
      }

      $scope.addButtonShow=false;
      $http({
          method: 'GET',
          url :'api/EmployeeDepartments?filter={"where":{"status":"Active"}}',
          headers: {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {
          //console.log('Users Response :' + JSON.stringify(response));
          $scope.departmentList=response;
      }).error(function (response) {
          console.log('Error Response :' + JSON.stringify(response));
      });

        $scope.getDetailsListTrue = false;
        $scope.employeeData;



       /* $scope.data = function(){
        alert(datavalue)
        $http({
                   method: 'GET',
                   url :'api/AssetForLands?filter={"where":{"id":"'+datavalue+'"}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
               }).success(function (response) {

                  //$scope.checkasset = response;
                  $scope.assetResp = response;
               }).error(function (response) {
                   console.log('Error Response :' + JSON.stringify(response));
               });


        }*/

      $scope.getEmployeeDepartment = function(department) {
      $scope.selectAsset = '';
      $scope.submitBillsListTrue=false;
      $scope.assetTur = false;
    $scope.getDetailsListTrue = false;

          $scope.departmentName=department;
          delete $scope.assetResp
          $http({
              method: 'GET',
              url :'api/Employees?filter={"where":{"department":"'+$scope.departmentName+'"}}',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              $scope.getDetailsList=response;
              $scope.getDetailsListTrue = ($scope.getDetailsList.length>0)?true:false;
            //  console.log("4817:"+JSON.stringify($scope.getDetailsList));
              $window.localStorage.setItem('employee',JSON.stringify($scope.getDetailsList));
              $window.localStorage.setItem('departmentName',$scope.departmentName);

             // $scope.getSubmitBillsDetails();
          }).error(function (response) {
             // console.log('Error Response :' + JSON.stringify(response));
          });



      }

      $scope.showCharter = function() {
          $scope.createCharterArea = true;
          $scope.editCharterArea = false;
      }
      $scope.editCharterButton = function(charter) {
          $("#editCharter").modal("show");
          $scope.editCharterArea = true;
          $scope.createCharterArea = false;
          $scope.editCharterData=angular.copy(charter);
      }
      //--------------------selected employeelist--------------------------------
            $scope.assetResp=[];
      $scope.selectedEmpList = function(employee){
      console.log("hgggggggggggg"+employee);
      $scope.selectAsset = '';
 /*$timeout(function(){
                          $scope.data();
                      }, 3000);*/
      //$scope.addButtonShow=true;

             var selectedDetails = employee;
             $scope.selectedEmployeeDetails = employee;
          $scope.empDesignation = selectedDetails.designation;
          $scope.filterByEmployee = selectedDetails.firstName;
          $scope.filterByDepartment = selectedDetails.department;
          $scope.filterByEmployeeId = selectedDetails.employeeId;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));

          var details={
              "employeeId":loginPersonDetails.employeeId,
              "searchPersonId":$scope.selectedEmployeeDetails.employeeId,
          }

          $http({
              method: 'GET',
              url :'api/SubmitBills/getDetails?employeeId=%7B%22employeeId%22%3A%20%22'+loginPersonDetails.employeeId+'%22%2C%20%22searchPersonId%22%3A%20%22'+$scope.selectedEmployeeDetails.employeeId+'%22%7D',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
           $scope.submitBillsList1=response;
          $scope.assetReDirect($scope.selectedEmployeeDetails);
            }).error(function (response) {
                   console.log('Error Response :' + JSON.stringify(response));
               });
               }
      $scope.submitBillsListTrue = false;
 $scope.selectedAsset =function(x){
 $scope.addButtonShow=true;
 console.log("=========================== "+JSON.stringify(x))
if(x){
console.log("selectAssetselectAssetselectAssetselectAssetselectAssetselectAsset::: :   ",$scope.selectAsset)
$scope.submitBillsList = $scope.submitBillsList1;
$scope.submitBillsListTrue =($scope.submitBillsList.length>0)?true:false;
}
}

$scope.assetReDirect = function(employee){
       $http({
              method: 'GET',
              url :'api/AssignDrivers?filter={"where":{"employeeId":"'+employee.employeeId+'"}}',

              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {

            $scope.assetIdCombo = response;
            for(var i=0;i<$scope.assetIdCombo.length;i++){
            $scope.assId = $scope.assetIdCombo[i].assetId;
    datavalue=$scope.assetIdCombo[i].assetId;
   console.log("value is"+datavalue);
             $http({
           method: 'GET',
           url :'api/AssetForLands?filter={"where":{"id":"'+$scope.assId+'"}}',
           headers: {"Content-Type": "application/json", "Accept": "application/json"}
       }).success(function (response) {

          //$scope.checkasset = response;
          $scope.assetResp = response;
       }).error(function (response) {
           console.log('Error Response :' + JSON.stringify(response));
       });
        }
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });




}


       $scope.removeData=function()
                     {
                    $scope.charter = {};
                    $scope.errorMsg=false;
                     }

      $scope.error='';
      $scope.errorMsg=false;


      $scope.createSubmitBills=function () {
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          var charterDetails=$scope.charter;
          //charterDetails['employeeName']=$scope.selectedEmployeeDetails.firstName;
          charterDetails['employeeName']= $scope.filterByEmployee;
          charterDetails['designation']= $scope.empDesignation;
          charterDetails['employeeId']= $scope.filterByEmployeeId;
          charterDetails['departmentName']= $scope.departmentName;
          //charterDetails['departmentName']=$window.localStorage.getItem('departmentName');
          charterDetails['createdPerson']=loginPersonDetails.name;
          charterDetails['createdPersonId']=loginPersonDetails.employeeId;
          //debugger;
          charterDetails['selectAsset']=$scope.selectAsset;
          if($scope.charter.billDate!=undefined)
          {
              $http({
              method: 'POST',
              url :'api/SubmitBills',
              headers: {"Content-Type": "application/json", "Accept": "application/json"},
              "data":charterDetails
          }).success(function (response) {
              console.log('Users Response========= :' + JSON.stringify(response));
              $scope.charter = {};
              $scope.selectedEmpList($scope.selectedEmployeeDetails);
              $("#createCharter").modal("hide");
              $("#addPlanLayoutSuccess").modal("show");
              setTimeout(function(){$('#addPlanLayoutSuccess').modal('hide')}, 3000);
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });
          }
          else {
          $scope.error="Please Select Bill Date";
          $scope.errorMsg=true;

          }

      }
/*
      $scope.editSubmitBills=function () {
          var editCharterDetails= $scope.editCharterData;
          var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
          editCharterDetails['lastEditPerson']=loginPersonDetails.name;
          editCharterDetails['employeeName']=$scope.assetTypelist.employee;
          $http({
              method: 'PUT',
              url :'api/SubmitBills/'+$scope.editCharterData.id,
              headers: {"Content-Type": "application/json", "Accept": "application/json"},
              "data":editCharterDetails
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              ////alert('successfully edit charter');
              $scope.selectedEmpList($scope.selectedEmployeeDetails);
              $("#editCharter").modal("hide");
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });
      }*/
     $scope.comments;
     $scope.approvalModal=function(fieldViisitData){
         $scope.fieldData=angular.copy(fieldViisitData);
         $("#approavlReport").modal("show");
     }
     $scope.rejectModal=function(fieldViisitData){
         $scope.fieldData=angular.copy(fieldViisitData);
         $("#rejectReport").modal("show");
     }

     $scope.userInfo = JSON.parse($window.localStorage.getItem('userDetails'));
     var employeeId=$scope.userInfo.employeeId;
     var employeeName=$scope.userInfo.name;
     var approvalDoubleClick=false;
     $scope.approvalData=function(){
         var updateDetails = {
             "acceptLevel":$scope.fieldData.acceptLevel,
             "acceptStatus": "Yes",
             "requestStatus": "Approval",
             "comment":  $scope.comments.comment,
             "requestId":$scope.fieldData.id,
             "employeeId":employeeId,
             "employeeName":employeeName
         };
         if(!approvalDoubleClick){
             approvalDoubleClick=true;
             $http({
                 "method": "POST",
                 "url": 'api/SubmitBills/updateDetails',
                 "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                 "data": updateDetails
             }).success(function (response, data) {
                 approvalDoubleClick=false;
                 $scope.selectedEmpList($scope.selectedEmployeeDetails);
                 $("#approavlReport").modal("hide");
             }).error(function (response, data) {
                 approvalDoubleClick=false;
                 console.log("failure");
             })
         }
     }
     var rejectedDoubleClick=false;
     $scope.rejectData=function(){

         var updateDetails = {
             "acceptLevel":$scope.fieldData.acceptLevel,
             "acceptStatus": "No",
             "requestStatus": "Rejected",
             "comment":  $scope.comments.comment,
             "requestId":$scope.fieldData.id,
             "employeeId":employeeId,
             "employeeName":employeeName
         };
         if(!rejectedDoubleClick){
             rejectedDoubleClick=true;
             $http({
                 "method": "POST",
                 "url": 'api/SubmitBills/updateDetails',
                 "headers": {"Content-Type": "application/json", "Accept": "application/json"},
                 "data": updateDetails
             }).success(function (response, data) {
                 //$scope.getDocuments();
                 $("#rejectReport").modal("hide");
                 rejectedDoubleClick=false;
                 $scope.selectedEmpList($scope.selectedEmployeeDetails);
             }).error(function (response, data) {
                 console.log("failure");
                 rejectedDoubleClick=false;
             })

         }

     }
     $scope.editProjectUploads = function(document) {
         console.log("editing the data" +JSON.stringify(document));
         //$("#editProjectDeliverables").modal("show");
         $("#editCharter").modal("show");
         $scope.editCharterData = angular.copy(document);
     }

     $scope.editSubmitBills=function () {
             var editProjectUploadDetails = $scope.editCharterData;
             var loginPersonDetails = JSON.parse($window.localStorage.getItem('userDetails'));
             editProjectUploadDetails['lastEditPerson']=loginPersonDetails.name;
             editProjectUploadDetails['lastEditPersonId']=loginPersonDetails.employeeId;

         var editUploadDoubleClick=false;
             if(!editUploadDoubleClick){
                 editUploadDoubleClick=true;
                 $http({
                     method: 'POST',
                     url: 'api/SubmitBills/updateContent'  ,
                     headers: {"Content-Type": "application/json", "Accept": "application/json"},
                     "data": editProjectUploadDetails
                 }).success(function (response) {
                     $scope.selectedEmpList($scope.selectedEmployeeDetails);
                     $("#editCharter").modal("hide");
                     $("#editPlanLayoutSuccess").modal("show");
                     editUploadDoubleClick=false;
                     setTimeout(function(){$('#editPlanLayoutSuccess').modal('hide')}, 3000);
                 }).error(function (response) {
                     editUploadDoubleClick=false;
                     console.log('Error Response :' + JSON.stringify(response));
                 });
             }


     }
     /*    $scope.getSubmitBillsDetails=function () {
             $http({
                 method: 'GET',
                 url :'api/SubmitBills?filter={"where":{"departmentName":"'+$scope.departmentName+'"}}',
                 headers: {"Content-Type": "application/json", "Accept": "application/json"}
             }).success(function (response) {
                 //console.log('Users Response :' + JSON.stringify(response));
                 $scope.submitBillsList=response;
             }).error(function (response) {
                 console.log('Error Response :' + JSON.stringify(response));
             });

         }*/

  });

app.controller('assetDepartmentController', function($http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {
    console.log("assetDepartmentController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #configuration li.department").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #configLi.treeview").addClass("active");
        $(".sidebar-menu #configuration li.department .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

    $scope.loadingImage=true;
    /*$scope.vm = {};*/

    $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
        .withOption("sPaginationType", "bootstrap")
        .withOption("retrieve", true)
        .withOption('stateSave', true)
        .withPaginationType('simple_numbers')
        .withOption('order', [0, 'ASC']);

    /*$scope.dtColumns = [
        DTColumnBuilder.newColumn('name').withTitle('Name'),
        DTColumnBuilder.newColumn('status').withTitle('Status')
    ];*/

    $scope.editDepartment = function(department) {
        $("#editDepartment").modal("show");
        /*$scope.editCharterArea = true;
         $scope.createCharterArea = false;*/
        $scope.editDepartmentData=angular.copy(department);
    }

    $scope.getDepartments=function () {
        $http({
            method: 'GET',
            url: 'api/AssetDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"}
        }).success(function (response) {
            console.log('getDepartments.................... :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $scope.departmentList = response;
        }).error(function (response) {
            console.log('Error Response :' + JSON.stringify(response));
        });
    }

    $scope.reset = function () {
    $scope.department = angular.copy($scope.master);
      };

    $scope.addDepartmentModal = function() {
        $("#addDepartment").modal("show");
        $scope.department = {};
        $scope.errorMessageData = '';
        $scope.errorMessage=false;
    }
    $scope.addDepartment=function () {
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        var department=$scope.department;
        department['createdPerson']=loginPersonDetails.name;
        $http({
            method: 'POST',
            url: 'api/AssetDepartments',
            headers: {"Content-Type": "application/json", "Accept": "application/json"},
            "data":department
        }).success(function (response) {
            console.log('Users Response :' + JSON.stringify(response));
            $scope.loadingImage=false;
            $("#addDepartment").modal("hide");
            $("#addDeptSuccess").modal("show");
            setTimeout(function(){$('#addDeptSuccess').modal('hide')}, 3000);
            $scope.getDepartments();
        }).error(function (response) {
            if(response.error.details.messages.name) {
                $scope.errorMessageData=response.error.details.messages.name[0];
                $scope.errorMessage=true;
            }
            $timeout(function(){
                $scope.errorMessage=false;
            }, 3000);

        });
    }

    $scope.editDepartmentButton=function () {
        var editCharterDetails= $scope.editDepartmentData;
        var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
        editCharterDetails['lastEditPerson']=loginPersonDetails.name;
        $http({
            "method": "PUT",
            "url": 'api/AssetDepartments/'+$scope.editDepartmentData.id,
            "headers": {"Content-Type": "application/json", "Accept": "application/json"},
            "data": editCharterDetails
        }).success(function (response, data) {
            //console.log("filter Schemes "+ JSON.stringify(response));
            $scope.loadingImage=false;
            $("#editDepartment").modal("hide");
            $("#editDeptSuccess").modal("show");
            setTimeout(function(){$('#editDeptSuccess').modal('hide')}, 3000);
            $scope.getDepartments();
        }).error(function (response, data) {
            if(response.error.details.messages.name) {
                $scope.errorMessageUpdateData=response.error.details.messages.name[0];
                $scope.errorUpdateMessage=true;
            }
            $timeout(function(){
                $scope.errorUpdateMessage=false;
            }, 3000);
            console.log("failure");
        })
    }

    $scope.getDepartments();

    $scope.exportToExcel=function(tableId){
        if( $scope.departmentList!=null &&  $scope.departmentList.length>0){

            $("<tr>" +
                "<th>Department Name</th>" +
                "<th>Status</th>" +
                "</tr>").appendTo("table"+tableId);

            for(var i=0;i<$scope.departmentList.length;i++){
                var department=$scope.departmentList[i];

                $("<tr>" +
                    "<td>"+department.name+"</td>" +
                    "<td>"+department.status+"</td>" +
                    "</tr>").appendTo("table"+tableId);
            }
            var exportHref = Excel.tableToExcel(tableId, 'Department Lists');
            $timeout(function () {
                location.href = exportHref;
            }, 100);
        } else {
            $("#emptyDataTable").modal("show");
            setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
        }
    }
$scope.reset();
});


app.controller('createAssetServiceLevelForecastController', function($filter,$http, $scope, $window, $location, $rootScope, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

console.log("createAssetServiceLevelForecastController");

    $(document).ready(function () {
        $('html,body').scrollTop(0);
        $(".sidebar-menu li ul > li").removeClass('active1');
        $('[data-toggle="tooltip"]').tooltip();
        $(".sidebar-menu #configuration li.department").addClass("active1").siblings('.active1').removeClass('active1');
        $(".sidebar-menu #configLi.treeview").addClass("active");
        $(".sidebar-menu #configuration li.department .collapse").addClass("in");
        $(".sidebar-menu li .collapse").removeClass("in");
    });

          $scope.hideButton=false;

          $http({
              method: 'GET',
              url :'api/AssetDepartments?filter={"where":{"status":"Active"}}',
              headers: {"Content-Type": "application/json", "Accept": "application/json"}
          }).success(function (response) {
              //console.log('Users Response :' + JSON.stringify(response));
              $scope.employeeListType=response;
          }).error(function (response) {
              console.log('Error Response :' + JSON.stringify(response));
          });


          $scope.getEmployeeDepartment = function(id) {
              $scope.departmentName=id;
              console.log("jndkj"+id)
              $http({
                  method: 'GET',
                  url :'api/AssetForLands?filter={"where":{"department":"'+$scope.departmentName+'"}}',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"}
              }).success(function (response) {
                  //console.log("response"+JSON.stringify(response))
                  $scope.getDetailsEmp=response;
                  console.log("4817:"+JSON.stringify($scope.getDetailsEmp));
                 /* if($scope.getDetailsEmp=[])
                  {
                     alert("hello");
                     $scope.clearResponse=[];
                  }*/
                  $window.localStorage.setItem('assetType',JSON.stringify($scope.getDetailsEmp));
                  $window.localStorage.setItem('departmentName',$scope.departmentName);
                  console.log("315:"+JSON.stringify(response));

              }).error(function (response) {
                  console.log('Error Response :' + JSON.stringify(response));
              });
          }


          $scope.selectedEmployee = function(assetName){
          //console.log("json data:" +JSON.stringify(assetType));
              //alert("hhhhh :"+JSON.stringify(employee));
              //var selectedList = JSON.parse($scope.getDetails.employee);
              var selectedList = assetName;
              //$scope.employeeDesignation = selectedList.designation;
              $scope.filterByEmployeeId = selectedList.id;
              $scope.filterByAsset = selectedList.assetType;
              $scope.filterByDepartment = selectedList.department;
              //alert("1111111111111111111111111111"+$scope.filterByDepartment)
              //alert($scope.filterByDepartment)
              $http({
                  method: 'GET',
                  url :'api/AssetForLands?filter={"where":{"assetType":"'+$scope.filterByAsset+'","department":"'+$scope.filterByDepartment+'"}}',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"}
              }).success(function (response) {
                  console.log('Users Response============================== :' + JSON.stringify(response));
                  $scope.logBookList2=response;
                  $scope.replacementDueDate=response[0].replacementDueDate;
                  $scope.assetNumberValue=response[0].assetNumber;
                  $scope.currentServiceValue=response[0].currentServiceLevel;
                  $scope.warrantyValue=response[0].warrentyPeriod;
                  $scope.expiryValue=response[0].expiryDate;
                  console.log('replacement date Response============================== :' + JSON.stringify($scope.replacementDueDate));
                  //$scope.getLogBookDetails();
              }).error(function (response) {
                  console.log('Error Response :' + JSON.stringify(response));
              });


          }

           $scope.charter={
           date:$filter("date")(Date.now(), 'dd-MM-yyyy')};
           //console.log("date is" +$scope.charter.date);

           $scope.getDate=function()
           {
             $scope.hideButton=true;
             //console.log(assetName);
             console.log("date is" +$scope.charter.date)
             var splitDate=$scope.charter.date.split("-");
             console.log("date split" +splitDate[1])
             //var replacementDate=new Date($scope.replacementDueDate);
             var replaceSplitDate=$scope.replacementDueDate.split("-");
             console.log("replace date is" +$scope.replacementDueDate);
             console.log("replace date split" +replaceSplitDate[1]);
             $scope.filterByDate =replaceSplitDate[1];
             console.log("filter my month" +$scope.filterByDate);

             if(splitDate[1]==replaceSplitDate[1])
             {
                //$scope.filterByDate=replaceSplitDate;
                //console.log("match" + $scope.filterByDate)
                 $http({
                  method: 'GET',
                  url :'api/AssetForLands?filter={"where":{"assetType":"'+$scope.filterByAsset+'","department":"'+$scope.filterByDepartment+'","replacementDueDate":"'+$scope.replacementDueDate+'"}}',
                  headers: {"Content-Type": "application/json", "Accept": "application/json"}
                  }).success(function (response) {
                  console.log('Users Response============================== :' + JSON.stringify(response));
                  $scope.logBookList1=response;
                  $scope.getLogBookDetails();
                  }).error(function (response) {
                  console.log('Error Response :' + JSON.stringify(response));
              });
             }
             else
             {
               console.log("mismatch")
             }

           }

           $scope.errorMsg=false;
           $scope.msgError=''

           $scope.createServiceLevel=function()
           {
              var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));

                                   var charterDetails=$scope.charter;
                                   console.log('login person details'+loginPersonDetails.department);
                                   charterDetails['employeeName']= $scope.filterByEmployee;
                                   charterDetails['assetId']= $scope.filterByAsset;
                                   charterDetails['departmentName']=$scope.filterByDepartment;
                                   charterDetails['employeeId']=$scope.filterByEmployeeId;
                                   charterDetails['assetNumber']=$scope.assetNumberValue;
                                   charterDetails['currentServiceLevel']=$scope.currentServiceValue;
                                   charterDetails['replacementDueDate']=$scope.replacementDueDate;
                                   charterDetails['warrentyPeriod']=$scope.warrantyValue;
                                   charterDetails['expiryDate']=$scope.expiryValue;

                                   console.log('..................'+JSON.stringify(charterDetails));

                                    //alert("dept.........." +JSON.stringify($scope.departmentName))

                                    if($scope.departmentName!= undefined){
                                    //alert("dept" +JSON.stringify($scope.departmentName))
                                              if($scope.filterByAsset != undefined ){
                                               //alert("dept" +JSON.stringify($scope.departmentName))
                                                      if($scope.charter.nextDueDate != undefined){
                                                        //alert("dept" +JSON.stringify($scope.charter.nextDueDate))
                                   $http({
                                       method: 'POST',
                                       url :'api/CreateFutureServiceLevels',
                                       headers: {"Content-Type": "application/json", "Accept": "application/json"},
                                       "data":charterDetails
                                   }).success(function (response) {
                                       console.log('Log Response:'+JSON.stringify(response))
                                       $scope.filterByNextDate=response.nextDueDate;
                                       console.log("next date response" +JSON.stringify($scope.filterByNextDate));
                                       //$scope.charter = {};
                                       $scope.getLogBookDetails();
                                       $("#createCharter").modal("hide");
                                       $("#addLogBookSuccess").modal("show");
                                       setTimeout(function(){$('#addLogBookSuccess').modal('hide')}, 3000);
                                   }).error(function (response) {
                                       console.log('Error Response :' + JSON.stringify(response));
                                   });
                                   } else{
                                       $scope.errorMsg = true;
                                       $scope.msgError = 'Please Select Next Due Date';
                                       console.log("select date")
                                   }
                                   }
                                   else {
                                    $scope.errorMsg = true;
                                    $scope.msgError = 'Please Select An Asset';
                                    console.log("select asset")
                                   }
                                   } else {
                                   $scope.errorMsg = true;
                                   $scope.msgError = 'Please Select Department';
                                   //alert("select dept11111111111")
                                   }

           }

           $scope.editCharterButton = function(charter) {
                     $("#editCharter").modal("show");
                     $scope.editService = true;
                     $scope.createCharterArea = false;
                     $scope.editService=angular.copy(charter);
                 }



                 $scope.editServiceLevel=function () {
                     var editCharterDetails= $scope.editService;
                     var loginPersonDetails= JSON.parse($window.localStorage.getItem('userDetails'));
                     editCharterDetails['lastEditPerson']=loginPersonDetails.name;
                     $http({
                         method: 'PUT',
                         url :'api/CreateFutureServiceLevels/'+$scope.editService.id,
                         headers: {"Content-Type": "application/json", "Accept": "application/json"},
                         "data":editCharterDetails
                     }).success(function (response) {
                         console.log('Users Response :' + JSON.stringify(response));
                         $scope.clearResponse=response;
                         $scope.getLogBookDetails();
                        // $scope.selectedEmployee();
                         $("#editCharter").modal("hide");
                         $("#editFailureSuccess").modal("show");
                         setTimeout(function(){$('#editFailureSuccess').modal('hide')}, 3000);
                     }).error(function (response) {
                         console.log('Error Response :' + JSON.stringify(response));
                     });
                 }

           $scope.resetData=function()
           {
             delete $scope.charter.nextDueDate;
           }

           $scope.getLogBookDetails=function () {
                    $http({
                        method: 'GET',
                        //url :'api/CreateFutureServiceLevels?filter={"where":{"assetId":"'+$scope.filterByAsset+'","departmentName":"'+$scope.filterByDepartment+'","replacementDueDate":"'+$scope.replacementDueDate+'"}}',
                        url :'api/CreateFutureServiceLevels?filter={"where":{"employeeId":"'+$scope.filterByEmployeeId+'"}}',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"}
                    }).success(function (response) {
                        console.log('Users Response :' + JSON.stringify(response));
                        $scope.logBookList=response;
                    }).error(function (response) {
                        console.log('Error Response :' + JSON.stringify(response));
                    });

            }
});

app.controller('MISReportsController', function($scope, $rootScope, $window, $location, $http, Excel, $timeout, DTOptionsBuilder, DTColumnBuilder) {

     console.log("MISReportsController");

     $(document).ready(function () {
         $('html,body').scrollTop(0);
         $(".navbar-nav li.landAndAsset").addClass("active").siblings('.active').removeClass('active');
         $(".navbar-nav li.landAndAsset li.assetType").addClass("active1").siblings('.active1').removeClass('active1');
         $(".navbar-nav li.schemes .collapse").removeClass("in");
         $(".navbar-nav li.landAndAsset .collapse").addClass("in");
         $(".navbar-nav li.configuration .collapse").removeClass("in");
         $(".navbar-nav li.administration .collapse").removeClass("in");

         $(".navbar-nav > li.projectWorks").addClass("active").siblings('.active').removeClass('active');
         $(".navbar-nav li.projectWorks li.masterData li.departmentList").addClass("active1").siblings('.active1').removeClass('active1');
         $(".navbar-nav li.projectWorks li.masterData .nav-list").addClass("active2").siblings('.active2').removeClass('active2');
         $(".navbar-nav li.projectWorks > .collapse").addClass("in");
         $(".navbar-nav li.masterData .collapse").addClass("in");
     });

      $scope.dtOptions = DTOptionsBuilder.newOptions().withBootstrap()
          .withOption("sPaginationType", "bootstrap")
          .withOption("retrieve", true)
          .withOption('stateSave', true)
          .withPaginationType('simple_numbers')
          .withOption('order', [0, 'ASC']);

        $scope.errorMessage = false;
        $scope.errorMessageData = '';
        $scope.isDisabled = false;
        $scope.ImmovableAssetReports=function(){
            $scope.land="assetReport";
            var owned=0,managed=0;
            $http({
                "method": "GET",
                "url": 'api/AssetForLands/?filter={"where":{"assetType":"Immovable"}}',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                //console.log('get Assets For Lands...:' + JSON.stringify(response));
                angular.forEach(response,function(v,i){
                    if(v.assetMode=='Owned'){
                        owned++;
                    }else{
                        managed++;
                    }
                    if(i==response.length-1){
                        $scope.result=[{
                            "name": "owned",
                            "y": owned
                        },{
                            "name":"managed",
                            "y": managed
                        }
                        ]
                        $scope.displayAssetReports($scope.result);
                    }
                });
            }).error(function (response, data) {
                console.log("failure"+response);
            });
        }
        $scope.ImmovableAssetReports();
        $scope.MovableAssetReports=function(){
            //?filter=%7B%22where%22%3A%7B%22assetType%22%3A%22Immovable%22%7D%7D
            //get Immovalbe assets
            var o=0,m=0;
            $http({
                "method": "GET",
                "url": 'api/AssetForLands/?filter={"where":{"assetType":"Movable"}}',
                //"url":'api/AssetForLands',
                "headers": {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                //console.log('get Assets For Lands...:' + JSON.stringify(response));
                angular.forEach(response,function(v,i){
                    if(v.assetMode=='Owned'){
                        o++;
                    }else{
                        m++;
                    }
                    if(i==response.length-1){
                        $scope.result=[{
                            "name": "owned",
                            "y": o
                        },{
                            "name":"managed",
                            "y": m
                        }
                        ]
                        $scope.displayAssetReports($scope.result);
                    }
                });
            }).error(function (response, data) {
                console.log("failure"+response);
            });
        }

        $scope.AssetByDepartment= function(){
        $scope.loadingImage= true;
            $scope.land='deptWiseAssetReport';
            var owned=0,managed=0,others=0,departments=[];
                $http({
                    method: 'GET',
                    url: 'api/AssetForLands/getDeptWiseAssetReport',
                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                $scope.loadingImage= false;
                    $http({
                        method: 'GET',
                        url: 'api/AssetDepartments?filter={"where":{"status":"Active"}}',
                        headers: {"Content-Type": "application/json", "Accept": "application/json"}
                    }).success(function (res) {
                        for(i=0;i<res.length;i++)
                        {
                            departments.push(res[i].name);
                        }
                        console.log('departments array...'+JSON.stringify(departments));
                        $scope.DisplayAssetByDepartment(departments, response);
                    }).error(function (response) {
                    $scope.loadingImage= false;
                        console.log('getting departments array error');
                    });

                }).error(function (response) {
                $scope.loadingImage= false;
                        alert('error getting assetByDepartment..'+response);
                });
        }

        $scope.SupplierWiseAssets= function(){
            $scope.land = 'supppWiseAssetReport';
            $scope.showSupplier=false;
            $scope.showDetails=false;
            $scope.selectDept = '';

            $http({
                method: 'GET',
                url: 'api/AssetDepartments?filter={"where":{"status":"Active"}}',
                headers: {"Content-Type": "application/json", "Accept": "application/json"}
            }).success(function (response) {
                $scope.departmentList = response;
            }).error(function (response) {

            });


            $scope.selectDepartment = function(dep) {
                $http({
                    "method": "GET",
                    "url": 'api/AssetForLands/?filter={"where":{"and":[{"assetType":"Movable"},{"departmentValue":"' + dep + '"}]}}',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    $scope.assetInfo = response;
                    if(response.length>0){
                        $scope.showSupplier=true;
                    }else{
                        $scope.showSupplier=false;
                        $scope.showDetails=false;
                    }
                }).error(function (response, data) {
                    console.log("failure");
                });
            }
            $scope.selectSupplierName1 =function(val,y){
                $http({
                    "method": "GET",
                    "url": 'api/AssetForLands/?filter={"where":{"and":[{"supplierName":"' + val +'"},{"departmentValue":"' + y + '"}]}}',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response) {
                    console.log('Asset Get Response :' + JSON.stringify(response));
                    /*alert("supplier wise" JSON.stringify(response))*/
                    $scope.assetWithSupplier = response[0];
                    $scope.showDetails=true;
                    $scope.showAction = true;
                }).error(function (response, data) {
                    console.log("failure");
                });
            }


            $scope.exportToExcel=function(tableId){
            $("#schemeReportDetails").empty();
            $scope.isDisabled = true;

                  if( $scope.assetWithSupplier!=null){

                      $("<tr>" +
                          "<th>Asset Name</th>" +
                          "<th>Asset Number</th>" +
                          "<th>Supplier Name</th>" +
                          "<th>Manufacturing Name</th>" +
                          "<th>Warranty Period</th>" +
                          "<th>Expiry Date</th>" +
                          "<th>Maintenance Contact Name</th>" +
                          "<th>Maintenance Contact Department</th>" +
                          "</tr>").appendTo("table"+tableId);

                      /*for(var i=0;i<$scope.assetWithSupplier;i++){*/
                          var assetWithSupplier=$scope.assetWithSupplier;

                          console.log("asset with supplier info" +assetWithSupplier);

                          $("<tr>" +
                              "<td>"+assetWithSupplier.assetName+"</td>" +
                              "<td>"+assetWithSupplier.assetNumber+"</td>" +
                              "<td>"+assetWithSupplier.supplierName+"</td>" +
                              "<td>"+assetWithSupplier.manufacturingName+"</td>" +
                              "<td>"+assetWithSupplier.warrentyPeriod+"</td>" +
                              "<td>"+assetWithSupplier.expiryDate+"</td>" +
                              "<td>"+assetWithSupplier.maintenanceContactName+"</td>" +
                              "<td>"+assetWithSupplier.contactDeptValue+"</td>" +
                              "</tr>").appendTo("table"+tableId);
                      /*}*/
                      var exportHref = Excel.tableToExcel(tableId, 'List of Supplier wise Asset Details');
                      $timeout(function () {
                          location.href = exportHref;
                      }, 100);
                  } else {
                      $("#emptyDataTable").modal("show");
                      setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                  }
              }

        }

        $scope.displayAssetReports= function(result){
            Highcharts.chart('assetReport', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'Asset Report statistics to Represent owned and managed Assets'
                },
                subtitle: {
                    text: 'Please select type of asset to view other results'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Assets',
                    colorByPoint: true,
                    data: result
                }]
            });
        }

        $scope.DisplayAssetByDepartment= function(depts, result){

               Highcharts.chart('AsyDepartmenetWise', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Department Wise Asset Reports'
                },
                xAxis: {
                    categories: depts,
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Assets',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ' millions'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: result
                /*series: [{
                    name: 'Owned',
                    data: [107, 31, 635]
                }, {
                    name: 'managed',
                    data: [133, 156, 947]
                }, {
                    name: 'others',
                    data: [1052, 954, 4250]
                }]*/
            });

        }

        $scope.AssetDueForRenewal= function(){
           $scope.land='assetDueForRenewal';
           $scope.showDetailss =false;
           $scope.assetListResp =[];

           $scope.errorMessage = false;
           $scope.errorMessageData = '';

           $scope.selectFinYear = function(dep) {
                     $scope.showDetailss =false;
                     $http({
                          "method": "GET",
                          "url": 'api/AssetForLands/?filter={"where":{"assetRenewableAvailability":"Yes"}}',
                          "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                     }).success(function (response) {
                         $scope.assetRenewableAvailability = response;
                         for(var i=0;i<$scope.assetRenewableAvailability.length;i++){
                         if($scope.assetRenewableAvailability[i].assetUpgradeDate.split("-")[2]==dep){
                         $http({
                            method: 'GET',
                            url: 'api/AssetForLands/?filter={"where":{"assetUpgradeDate":{"like":"' + dep + '"}}}',
                            headers: {"Content-Type": "application/json", "Accept": "application/json"}
                            }).success(function (response) {

                            $scope.assetListResp = response;
                               $scope.showDetailss = ($scope.assetListResp.length>0)?true:false;
                               $scope.errorMessage =false;
                            }).error(function (response) {
                            });
                         }else {
                               $scope.errorMessage =true;
                               $scope.errorMessageData ="This Year Doesn't Have Any Disposals";

                              }
                        }
                     }).error(function (response, data) {

                     });
                     $scope.assetListClear();
                   }
              $scope.assetListClear =function(){
                  delete $scope.selectMonth;
                  $scope.showDetailsss=false;
              }
                 $scope.showDetailsss = false;
           $scope.selectFinMonth =function(val,y){
                    $scope.showDetailsss = false;
                      $http({
                           "method": "GET",
                           "url": 'api/AssetForLands/?filter={"where":{"assetUpgradeDate":{"like":"' + y + '-'+ val +'"}}}',
                           "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                      }).success(function (response) {
                          console.log('Asset Get Response :' + JSON.stringify(response));
                          $scope.filterAsset = response;
                          $scope.showDetailsss = ($scope.filterAsset.length>0)?true:false;
                          if(response.length>0){
                               $scope.errorMessage=false;
                               }else{
                               $scope.errorMessage =true;
                               $scope.errorMessageData ="This Month Doesn't Have Any Disposals";
                           }
                          }).error(function (response, data) {
                            console.log("failure");
                          });
                 }

           $scope.exportToExcel=function(tableId){
           $("#schemeReportDetails").empty();
           $scope.isDisabled = true;
               if( $scope.filterAsset!=null &&  $scope.filterAsset.length>0){

                   $("<tr>" +
                       "<th>Asset Name</th>" +
                       "<th>Asset Number</th>" +
                       "<th>Asset Description</th>" +
                       "<th>Asset Renewable Availablity Status</th>" +
                       "<th>Warranty Period</th>" +
                       "<th>Asset Upgrade Date</th>" +
                       "<th>Maintenance Contact Name</th>" +
                       "<th>Maintenance Contact Department</th>" +
                       "</tr>").appendTo("table"+tableId);

                   for(var i=0;i<$scope.filterAsset.length;i++){
                       var schemeWiseRequest=$scope.filterAsset[i];

                       $("<tr>" +
                           "<td>"+schemeWiseRequest.assetName+"</td>" +
                           "<td>"+schemeWiseRequest.assetNumber+"</td>" +
                           "<td>"+schemeWiseRequest.assetDescription+"</td>" +
                           "<td>"+schemeWiseRequest.assetRenewableAvailability+"</td>" +
                           "<td>"+schemeWiseRequest.warrentyPeriod+"</td>" +
                           "<td>"+schemeWiseRequest.assetUpgradeDate+"</td>" +
                           "<td>"+schemeWiseRequest.maintenanceContactName+"</td>" +
                           "<td>"+schemeWiseRequest.contactDeptValue+"</td>" +
                           "</tr>").appendTo("table"+tableId);
                   }
                   var exportHref = Excel.tableToExcel(tableId, 'List of Renewable Asset Details');
                   $timeout(function () {
                       location.href = exportHref;
                   }, 100);
               } else {
                   $("#emptyDataTable").modal("show");
                   setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
               }
           }

        }

        $scope.AssetDueForDisposal = function(){
            $scope.land='assetDueForDisposal';

            $scope.errorMessage = false;
            $scope.errorMessageData = '';

              $scope.showDetailss =false;
              $scope.assetListResp =[];
              $scope.selectFinYear = function(dep) {
              $scope.showDetailss =false;
                $http({
                             "method": "GET",
                             "url": 'api/AssetForLands?filter={"where":{"status":"disposal"}}',
                             "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                        }).success(function (response) {
                            $scope.assetDisposalResp = response;
                            console.log("resp" +JSON.stringify($scope.assetDisposalResp));
                            if($scope.assetDisposalResp.length>0){
                            for(var i=0;i<$scope.assetDisposalResp.length;i++){
                                if($scope.assetDisposalResp[i].decommissionDate.split("-")[2]==dep){
                                $http({
                                   method: 'GET',
                                   url: 'api/AssetForLands/?filter={"where":{"decommissionDate":{"like":"' + dep + '"}}}',
                                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
                                   }).success(function (response) {
                                   $scope.disposalDateByYear = response;
                                   console.log("resp1" +JSON.stringify($scope.disposalDateByYear));
                                      $scope.showDetailss = ($scope.disposalDateByYear.length>0)?true:false;
                                      $scope.errorMessage =false;
                                   }).error(function (response) {
                                   });
                                }else{
                                 $scope.errorMessage =true;
                                 $scope.errorMessageData ="This Year Doesn't Have Any Disposals";
                                }
                           }}else{
                             $scope.errorMessage =true;
                             $scope.errorMessageData ="This Year Doesn't Have Any Disposals";
                           }
                        }).error(function (response, data) {
                    });
                    $scope.assetListClear();
              }

              $scope.assetListClear =function(){
              delete $scope.selectMonth;
              $scope.showDetailsss=false;
              }
              $scope.showDetailsss = false;

              $scope.selectFinMonth =function(val,y){
              $scope.showDetailsss = false;

              $http({
                      "method": "GET",
                      "url": 'api/AssetForLands/?filter={"where":{"decommissionDate":{"like":"' + y + '-'+ val +'"}}}',
                      "headers": {"Content-Type": "application/json", "Accept": "application/json"}
              }).success(function (response) {
                 console.log('Asset Get Response :' + JSON.stringify(response));
                 $scope.filterAsset = response;
                 console.log("resp2" +JSON.stringify($scope.filterAsset));
                 if(response.length>0){

                 $scope.errorMessage=false;

                 }else{

                 $scope.errorMessage =true;
                 $scope.errorMessageData ="This Month Doesn't Have Any Disposals";

                 }
                 $scope.showDetailsss = ($scope.filterAsset.length>0)?true:false;
                 }).error(function (response, data) {
                 console.log("failure");
                 });
              }

            $scope.exportToExcel=function(tableId){
            $("#schemeReportDetails").empty();
            $scope.isDisabled = true;
                 if( $scope.filterAsset!=null &&  $scope.filterAsset.length>0){

                     $("<tr>" +
                         "<th>Asset Name</th>" +
                         "<th>Asset Number</th>" +
                         "<th>Asset Description</th>" +
                         "<th>Asset Renewable Availablity Status</th>" +
                         "<th>Asset Status</th>" +
                         "<th>Asset Upgrade Date</th>" +
                         "<th>Maintenance Contact Name</th>" +
                         "<th>Maintenance Contact Department</th>" +
                         "</tr>").appendTo("table"+tableId);

                     for(var i=0;i<$scope.filterAsset.length;i++){
                         var schemeWiseRequest=$scope.filterAsset[i];

                         $("<tr>" +
                             "<td>"+schemeWiseRequest.assetName+"</td>" +
                             "<td>"+schemeWiseRequest.assetNumber+"</td>" +
                             "<td>"+schemeWiseRequest.assetDescription+"</td>" +
                             "<td>"+schemeWiseRequest.assetRenewableAvailability+"</td>" +
                             "<td>"+schemeWiseRequest.status+"</td>" +
                             "<td>"+schemeWiseRequest.assetUpgradeDate+"</td>" +
                             "<td>"+schemeWiseRequest.maintenanceContactName+"</td>" +
                             "<td>"+schemeWiseRequest.contactDeptValue+"</td>" +
                             "</tr>").appendTo("table"+tableId);
                     }
                     var exportHref = Excel.tableToExcel(tableId, 'List of Disposal Asset Details');
                     $timeout(function () {
                         location.href = exportHref;
                     }, 100);
                 } else {
                     $("#emptyDataTable").modal("show");
                     setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                 }
            }
        }

        /*For Graph 03 start*/
        $scope.DisplayAssetByStatusAndYear=function(){
            var years=[],info=[];
           /*For Graph 03 start*/
                   var years=[],info=[];
                      var date = new Date();

                      console.log('current date..'+date);
                       var cYear= date.getFullYear();
                      for(var x=0;x<5;x++){
                           years.push(cYear);
                           cYear = cYear+1;
                      }

                       $scope.DisplayAssetByStatusAndYear=function(){
                       $scope.land="statusAndYearWiseReport";

                       $http({
                               "method": "GET",
                               "url": 'api/AssetForLands/getReportsOnComAndDeCom',
                               "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                           }).success(function (response) {
                               console.log('Status and Year Response :' + JSON.stringify(response));
                               $scope.assetWithSupplier = response;
                               $scope.showDetails=true;

                                Highcharts.chart('AssetByStatusAndYear', {
                                              chart: {
                                                  type: 'bar'
                                              },
                                              title: {
                                                  text: 'Commissioned And Decommissioned Assets By Year'
                                              },
                                              subtitle: {
                                                  text: null
                                              },
                                              xAxis: {
                                                  categories: years,
                                                  title: {
                                                      text: null
                                                  }
                                              },
                                              yAxis: {
                                                  min: 0,
                                                  title: {
                                                      text: 'Number Of Assets',
                                                      align: 'high'
                                                  },
                                                  labels: {
                                                      overflow: 'justify'
                                                  }
                                              },
                                              tooltip: {
                                                  valueSuffix: ' count'
                                              },
                                              plotOptions: {
                                                  bar: {
                                                      dataLabels: {
                                                          enabled: true
                                                      }
                                                  }
                                              },
                                              legend: {
                                                  layout: 'vertical',
                                                  align: 'right',
                                                  verticalAlign: 'top',
                                                  x: -40,
                                                  y: 80,
                                                  floating: true,
                                                  borderWidth: 1,
                                                  backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                                                  shadow: true
                                              },
                                              credits: {
                                                  enabled: false
                                              },
                                              series: $scope.assetWithSupplier
                                          });

                           }).error(function (response, data) {
                               console.log("failure");
                           });

                   }
                   /*For Graph 03 end*/
        }

        $scope.ExpiryAssets= function(){
            $scope.land='expiryAssets';
                $scope.exportToExcel=function(tableId){
                $("#schemeReportDetails").empty();
                $scope.isDisabled = true;
                      if( $scope.schemeWiseRequest!=null &&  $scope.schemeWiseRequest.length>0){

                          $("<tr>" +
                              "<th>Asset Name</th>" +
                              "<th>Asset Number</th>" +
                              "<th>Supplier Name</th>" +
                              "<th>Manufacturing Name</th>" +
                              "<th>Warranty Period</th>" +
                              "<th>Expiry Date</th>" +
                              "<th>Maintenance Contact Name</th>" +
                              "<th>Maintenance Contact Department</th>" +
                              "</tr>").appendTo("table"+tableId);

                          for(var i=0;i<$scope.schemeWiseRequest.length;i++){
                              var schemeWiseRequest=$scope.schemeWiseRequest[i];

                              $("<tr>" +
                                  "<td>"+schemeWiseRequest.assetName+"</td>" +
                                  "<td>"+schemeWiseRequest.assetNumber+"</td>" +
                                  "<td>"+schemeWiseRequest.supplierName+"</td>" +
                                  "<td>"+schemeWiseRequest.manufacturingName+"</td>" +
                                  "<td>"+schemeWiseRequest.warrentyPeriod+"</td>" +
                                  "<td>"+schemeWiseRequest.expiryDate+"</td>" +
                                  "<td>"+schemeWiseRequest.maintenanceContactName+"</td>" +
                                  "<td>"+schemeWiseRequest.contactDeptValue+"</td>" +
                                  "</tr>").appendTo("table"+tableId);
                          }
                          var exportHref = Excel.tableToExcel(tableId, 'List of Expiry Asset Details');
                          $timeout(function () {
                              location.href = exportHref;
                          }, 100);
                      } else {
                          $("#emptyDataTable").modal("show");
                          setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                      }
                  }

           $scope.selectDur=function(selectValue){
                  if(selectValue!=''){
                          var data={
                          'type':selectValue
                      }
                      $scope.expiryData(data);
                  }
              }

          $scope.expiryData=function(inputData){
                 $http({
                      method: 'POST',
                      url: 'api/AssetForLands/getExpiredReports',
                      headers: {"Content-Type": "application/json", "Accept": "application/json"},
                      "data": inputData
                  }).success(function (response) {
                      $scope.schemeWiseRequest = response;
                      $scope.selectProjectDetails = true;
                      $scope.showAction = true;
                  }).error(function (response) {
                  });
          }
        }

        $scope.citizenRequests = function() {
                    $scope.land='citizenRequests';

                    $scope.uploadFile=uploadFileURL;

                    var selectedDept;
                    var selectMinistry;
                     $scope.purchaseAgendaNumber= function (agendaId) {
                         selectedAgendaData=agendaId;
                     }
                     $scope.departmentName= function (deptId) {
                     $scope.isDisabled = false;
                         selectDeptData=deptId;
                         //$scope.deptWiseViewData=[];

                     }
                     $scope.purchageAgenda = function() {
                        $http({
                          method: 'GET',
                          url: 'api/AssetDepartments?filter={"where":{"status":"Active"}}',
                          headers: {"Content-Type": "application/json", "Accept": "application/json"}
                        }).success(function (response) {
                          console.log('Users Response :' + JSON.stringify(response));
                          $scope.departmentList = response;
                        }).error(function (response) {

                        });
                    }
                     $scope.purchageAgenda();

                     $scope.searchData = function() {

                            $scope.selectDept = selectDeptData;
    //                        alert($scope.selectDept)
                                $http({
                                    method: 'GET',
                                      url: 'api/VehiclePurchaseRequests?filter={"where":{"purchaseDepartment":"' + $scope.selectDept + '"}}',
            //                        url: 'api/PurchageAgendas?filter={"where":{"departmentName:"'+ $scope.selectDept +'}}',
            //                        url: 'api/PurchageAgendas/?filter={"where":{"and":[{"purchaseAgendaNumber":"' + $scope.sselectedAgenda + '"},{"departmentName":"' + $scope.selectedDept  + '"}]}}',
                                    headers: {"Content-Type": "application/json", "Accept": "application/json"}
                                }).success(function (response) {
                                console.log("response" + JSON.stringify(response))
                                            $scope.deptWiseViewData=response;
                                            $scope.loadingImage=false;
                                            $scope.selectProjectDetails=true;
                                            $scope.showAction=true;
                                   }).error(function (response) {
                                });

                        }

                     $scope.exportToExcel=function(tableId) {
                     $("#listOfSchemeDetails").empty();
                            $scope.isDisabled = true;
                            $scope.deptWiseViewDatatemp=[];
                            $scope.deptWiseViewDatatemp=$scope.deptWiseViewData;
                            if( $scope.deptWiseViewDatatemp!=null &&  $scope.deptWiseViewDatatemp.length>0){
                                $("<tr>" +
                                    "<th>purchaseAgendaNumber</th>" +
                                    "<th>requestId</th>" +
                                    "</tr>").appendTo("table"+tableId);

                                for(var i=0;i<$scope.deptWiseViewDatatemp.length;i++){
                                    var beneficiaries=$scope.deptWiseViewDatatemp[i];
                                    $("<tr>" +
                                        "<td>"+beneficiaries.purchaseAgendaNumber+"</td>" +
                                        "<td>"+beneficiaries.requestId+"</td>" +
                                        "</tr>").appendTo("table"+tableId);
                                }
                                var exportHref = Excel.tableToExcel(tableId, 'Citizen Request Details ');
                                $timeout(function () {
                                    location.href = exportHref;
                                }, 100);
                            } else {
                                $("#emptyDataTable").modal("show");
                                setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                            }
                        }
                }

        $scope.citizenRequestsAging = function() {
            $scope.land='citizenRequestsAging';
            $scope.uploadFile=uploadFileURL;
            $scope.schemesList = function (){
                $http({
                    "method": "GET",
                    "url": 'api/PurchageAgendas/getVehiclePendingRequests',
                    "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                }).success(function (response, data){
                    console.log('responce'+JSON.stringify(response));
                    $scope.schemeRanging = response;
                    $scope.loadingImage=false;
                    $scope.selectProjectDetails=true;
                    $scope.showAction=true;
                }).error(function (response, data) {
                    console.log("failure");
                })
            }
            $scope.schemesList();
        }

        $scope.purchaseAgendaRequests = function() {
            $scope.land='purchaseAgendaRequests';

            $scope.uploadFile=uploadFileURL;

            $scope.selectDur=function(selectValue){
                 if(selectValue!=''){
                         var data={
                         'duration':selectValue
                     }
     //                alert('ahi');
                     $window.localStorage.setItem('schemeData', JSON.stringify(data));
                     $scope.searchData(data);
                 }
             }

             $scope.getReportsCount= function () {
                if($scope.startDate != '' && $scope.startDate != undefined && $scope.startDate != null) {
                 if($scope.endDate != '' && $scope.endDate != undefined && $scope.endDate != null) {
                 var data={
                             'startDate':$scope.startDate,
                             'endDate':$scope.endDate
                         }
                         $window.localStorage.setItem('schemeData', JSON.stringify(data));
                         $scope.searchData(data);

                 } else {
                     $scope.errorUpdateMessage="Please Select End Date";
                      $scope.editDoubleClick=true;
                      $timeout(function(){
                          $scope.editDoubleClick=false;
                      }, 3000);
                }
                } else {
                  $scope.errorUpdateMessage="Please Select Start Date";
                   $scope.editDoubleClick=true;
                   $timeout(function(){
                       $scope.editDoubleClick=false;
                   }, 3000);
                }
             }

             $scope.searchData=function(inputData){
                $http({
                     method: 'POST',
                     url: 'api/PurchageAgendas/getAgendaWiseReport',
                     headers: {"Content-Type": "application/json", "Accept": "application/json"},
                     "data": inputData
                 }).success(function (response) {
                     console.log('responce'+JSON.stringify(response));
                     $scope.schemeWiseRequest = response;
                     $scope.selectProjectDetails = true;
                     $scope.showAction = true;
     //                $scope.loadingImage = true;
                 }).error(function (response) {
                 });
             }

             $scope.exportToExcel=function(tableId) {
             $("#schemeReportDetails").empty();
                          $scope.isDisabled = true;
                                 if( $scope.schemeWiseRequest!=null &&  $scope.schemeWiseRequest.length>0){

                                     $("<tr>" +
                                         "<th>purchaseAgendaNumber</th>" +
                                         "<th>New Count</th>" +
                                         "<th>Approved Count</th>" +
                                         "<th>Rejected Count</th>" +
                                         "<th>Total Count</th>" +
                                         "</tr>").appendTo("table"+tableId);

                                     for(var i=0;i<$scope.schemeWiseRequest.length;i++){
                                         var beneficiaries=$scope.schemeWiseRequest[i];
                                         $("<tr>" +
                                             "<td>"+beneficiaries.purchaseAgendaDetails.purchaseAgendaNumber+"</td>" +
                                             "<td>"+beneficiaries.falseCount+"</td>" +
                                             "<td>"+beneficiaries.approvedCount+"</td>" +
                                             "<td>"+beneficiaries.rejectCount+"</td>" +
                                             "<td>"+beneficiaries.totalCount+"</td>" +
                                           /*  "<td>"+beneficiaries.purchaseAgendaDetails.requestId+"</td>" +*/
                                             "</tr>").appendTo("table"+tableId);
                                     }
                                     var exportHref = Excel.tableToExcel(tableId, 'Citizen Purchase Agenda Request Details ');
                                     $timeout(function () {
                                         location.href = exportHref;
                                     }, 100);
                                 } else {
                                     $("#emptyDataTable").modal("show");
                                     setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                                 }
                          }
             }

        $scope.deptWiseDriverExpenses= function(){
                   $scope.land='deptWiseDriverDetails';
                   $scope.showDetailss =false;
                   $scope.errorMessage = false;
                   $scope.errorMessageData = '';
                   $scope.selectRenewalYear = '';
                   $http({
                         method: 'GET',
                         url: 'api/EmployeeDepartments?filter={"where":{"status":"Active"}}',
                         headers: {"Content-Type": "application/json", "Accept": "application/json"}
                   }).success(function (response) {

                         $scope.departmentList = response;
                   }).error(function (response) {

                   });

                   $scope.selectFinYear = function(dep) {

                   $http({
                   method: 'GET',
                   url: 'api/SubmitBills?filter={"where":{"billDate":{"like":"'+ dep +'"}}}',
                   headers: {"Content-Type": "application/json", "Accept": "application/json"}
                   }).success(function (response) {
                   $scope.assetListResp = response;
                   console.log(JSON.stringify(response));
                   $scope.showDetailss = ($scope.assetListResp.length>0)?true:false;
                   $scope.errorMessage =false;
                   if(response.length>0){
                      $scope.errorMessage=false;
                      }else{
                      $scope.errorMessage =true;
                      $scope.errorMessageData ="This Year Doesn't Have Any Expenses";
                      $scope.showDetailssss = false;
                   }
                   }).error(function (response) {
                   });

                     $scope.assetListClear();
                   }
                   $scope.assetListClear =function(){
                   delete $scope.selectMonth;
                   delete $scope.selectDepartment;
                   $scope.showDetailsss=false;
                   }
                   $scope.showDetailsss = false;
                   $scope.selectFinMonth =function(val,y){
                            $scope.showDetailsss = false;
                              $http({
                                   "method": "GET",
                                   "url": 'api/SubmitBills/?filter={"where":{"billDate":{"like":"' + y + '-'+ val +'"}}}',
                                   "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                              }).success(function (response) {
                                  console.log('Asset Get Response :' + JSON.stringify(response));
                                  $scope.filterAsset = response;
                                  $scope.showDetailsss = ($scope.filterAsset.length>0)?true:false;
                                     $scope.errorMessage =false;
                                     if(response.length>0){
                                        $scope.errorMessage=false;
                                        }else{
                                        $scope.errorMessage =true;
                                        $scope.errorMessageData ="This Month Doesn't Have Any Expenses";
                                        $scope.showDetailssss = false;
                                     }
                                  }).error(function (response, data) {
                                    console.log("failure");
                                  });
                                  $scope.clearData();
                         }
                         $scope.clearData = function(){

                         delete $scope.selectDepartment;

                         }
                   $scope.selectDepts = function (val,y,x){

                    $scope.showDetailssss = false;
                         $http({
                              "method": "GET",
                              "url": 'api/SubmitBills/?filter={"where":{"and":[{"billDate":{"like":"' + y + '-'+ val +'"},"departmentName":{"like":"'+ x +'"}}]}}',
                              "headers": {"Content-Type": "application/json", "Accept": "application/json"}
                         }).success(function (response) {
                             console.log('Asset Get Response :' + JSON.stringify(response));
                             $scope.filterAssets = response;
                             $scope.showDetailssss = ($scope.filterAssets.length>0)?true:false;
                              $scope.errorMessage =false;
                              if(response.length>0){
                                 $scope.errorMessage=false;
                                 }else{
                                 $scope.errorMessage =true;
                                 $scope.errorMessageData ="This Department Doesn't Have Any Expenses";
                              }
                             }).error(function (response, data) {
                               console.log("failure");
                             });
                   }
                   $scope.exportToExcel=function(tableId){
                   $("#schemeReportDetails").empty();
                   $scope.isDisabled = true;
                       if( $scope.filterAsset!=null &&  $scope.filterAsset.length>0){

                           $("<tr>" +
                               "<th>Asset ID</th>" +
                               "<th>Assign Driver Name</th>" +
                               "<th>Bill Number</th>" +
                               "<th>Bill Amount</th>" +
                               "<th>Bill Date</th>" +
                               "<th>Department Name</th>" +
                               "</tr>").appendTo("table"+tableId);

                           for(var i=0;i<$scope.filterAssets.length;i++){
                               var schemeWiseRequest=$scope.filterAssets[i];

                               $("<tr>" +
                                   "<td>"+schemeWiseRequest.selectAsset+"</td>" +
                                   "<td>"+schemeWiseRequest.employeeName+"</td>" +
                                   "<td>"+schemeWiseRequest.billNumber+"</td>" +
                                   "<td>"+schemeWiseRequest.amount+"</td>" +
                                   "<td>"+schemeWiseRequest.billDate+"</td>" +
                                   "<td>"+schemeWiseRequest.departmentName+"</td>" +
                                   "</tr>").appendTo("table"+tableId);
                           }
                           var exportHref = Excel.tableToExcel(tableId, 'Department Wise Driver Expenses');
                           $timeout(function () {
                               location.href = exportHref;
                           }, 100);
                       } else {
                           $("#emptyDataTable").modal("show");
                           setTimeout(function(){$('#emptyDataTable').modal('hide')}, 3000);
                       }
                   }

                }

    /* Vehicle Expenses Graph Report*/

       $scope.vehicleExpenses=function(){
       $scope.land="vehicleReport";

       $http({
          "method": "GET",
          "url": 'api/AssetForLands/getVehicleExpenses',
          "headers": {"Content-Type": "application/json", "Accept": "application/json"}
      }).success(function (response) {

          $scope.assetWithSupplier = response;
          console.log('vehicle info :' + JSON.stringify($scope.assetWithSupplier));
          $scope.showDetails=true;

       var chart = AmCharts.makeChart( "vehicleId", {
         "type": "serial",
         "theme": "light",
         "dataProvider": $scope.assetWithSupplier,
         "valueAxes": [ {
           "gridColor": "#FFFFFF",
           "gridAlpha": 0.2,
           "dashLength": 0,
           "title":'Counts'
         } ],
         "gridAboveGraphs": true,
         "startDuration": 1,
         "graphs": [ {
           "balloonText": "[[category]]: <b>[[value]]</b>",
           "fillAlphas": 0.8,
           "lineAlpha": 0.2,
           "type": "column",
           "valueField": "total"
         } ],
         "chartCursor": {
           "categoryBalloonEnabled": false,
           "cursorAlpha": 0,
           "zoomable": false
         },
         "categoryField": "assetNumber",
         "categoryAxis": {
           "gridPosition": "start",
           "gridAlpha": 0,
           "tickPosition": "start",
           "tickLength": 20,
           "title":'Assets'
         },
         "export": {
           "enabled": true
         }

       } );

       }).error(function (response, data) {
          console.log("failure");
      });
   }
   /*For vehicle end*/

 });

